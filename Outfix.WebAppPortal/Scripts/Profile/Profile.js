﻿$(function () {
    $('#saveAddress').on('submit', function (e) {
        e.preventDefault(); // prevent the form's normal submission
        $('#Customer_Address').val($('#FullAddress').val());
    });

    $("#ProfileSubmit").on('submit', function (e) {
        e.preventDefault(); // prevent the form's normal submission
        var objArr = [];
        var divQ = $(".questions :input");
        divQ.each(function (e) {
            var obj = new Object();
            var AnsType = this.getAttribute("data-content");

            if (this.type === "radio" && this.checked == true) {
                obj.QuestionID = this.parentNode.getAttribute("data-content");
                obj.AnswerId = this.id;

                objArr.push(obj);
            }
            else if (this.type !== "radio") {
                if (AnsType === "ddl") {
                    obj.QuestionID = this.id;
                    obj.AnswerId = this.value;
                }
                else if (AnsType == "txt" || AnsType == "date") {
                    obj.QuestionID = this.id;
                    obj.Answer = this.value;
                }
                objArr.push(obj);
            }
        })

        //console.log(objArr);
        var str = JSON.stringify(objArr);
        $.ajax({
            url: "/Profile/SaveSurveyForm",
            type: "POST",
            data: { str },

            success: function (data) {

                //code after success
                let l = "#ld_" + data.Id;
                $(l).hide();

                if (data.msg != "") {
                    alert(data.msg);
                }
                else {
                    let b = "#btnCart_" + data.Id;
                    $(b).val("Product Added");

                    $('#cartCount').html(data.count);
                    $('#cartCount').show();
                }

                if (data.count == 0) {
                    $('#cartCount').hide();
                }


            },
            error: function (er) {
                alert(er);
            }

        });

    });

});


var Profile = {

    inviteViaGmail: () => {
        var user = Profile.getCurrentUser();
        var subject = 'Join Outfix with Rs 300 free credits';
        var matter = 'Just wanted to share with you my experience with Outfix! It offers the best, most reliable and unique way to buy things you want!\n\n' +
            'I would like to invite you to try it out. Click on the link below to sign up and get Rs 300 in your wallet on Signup. \n\n' +
            'Regards,\n' + user.FirstName;


        matter = matter.replace(/\n\r?/g, '%0D%0A');

        var url =
            'http://mail.google.com/mail/?view=cm&fs=1' +
            '&to=' +
            '&su=' +
            subject +
            '&body=' +
            matter +
            '&ui=1';

        var w = 900;
        var h = 500;
        var left = screen.width / 2 - w / 2;
        var top = screen.height / 2 - h / 2;

        window.open(
            url,
            '_blank',
            'toolbar=0,location=0,menubar=0,width=' +
            w +
            ',height=' +
            h +
            ', top=' +
            top +
            ',left=' +
            left
        );
    },
    inviteViaEmail: () => {
        var user = Profile.getCurrentUser();

        var email = '';
        var subject =
            'Join Outfix with ' +
            'Rs 300 free credit';

        var matter = 'Just wanted to share with you my experience with Outfix! It offers the best, most reliable and unique way to buy things you want!\n\n' +
            'I would like to invite you to try it out. Click on the link below to sign up and get Rs 300 in your wallet on Signup. \n\n' +
            'Regards,\n' + user.FirstName;

        matter = matter.replace(/\n\r?/g, '%0D%0A');

        var url =
            'mailto:' + email + '?subject=' + subject + '&body=' + matter;

        window.open(url, '_blank');
    },
    inviteViaOutfix: () => {
        var emails = $('#UserEmails');
        var msg = $('#UserEmails');
        $.ajax({
            url: "/Home/SendInviteMails",
            type: "POST",
            data: { emails, msg },
            success: function (data) {
            },
            error: function (er) {
                alert(er);
            }
        });
    },
    postFB: () => {
        var msg =
            'Need a reliable way to purchase what you want? Check out the Outfix website. You can order anything from web. Get ' +
            'Rs 300 free credits:';
        var title =
            'Get Rs 300' +
            ' free credit on Outfix';
        var desc = 'Check this out';
        var link = 'https://soon.oufix.asia/signup/';

        FB.ui(
            {
                method: 'feed',
                link: 'https://developers.facebook.com/docs/',
                redirect_uri: 'https://localhost:44367/Profile/Refer',
                display: 'popup'
                //name: title,
                //link: link,
                //caption: desc,
                //description: "hi"
            },
            function (response) {
                debugger
                if (response && response.post_id) {
                    //code;
                }
            }
        );
    },
    messageFB: () => {
        var msg =
            'Need a reliable way to purchase what you want? Check out the Outfix website. You can order anything from web. Get ' +
            'Rs 300 free credits:';
        var title =
            'Get Rs 300' +
            ' free credit on Outfix';
        var desc = 'Check this out';

        FB.ui({
            //app_id: '439132446670385',
            method: 'send',
            name: title,
            link: 'https://developers.facebook.com/docs/',
            description: msg,
            message: msg,
        });
    },
    InviteViaTwitter: () => {
        var tweet =
            'Check out the Outfix Website. Order anything from web. Get ' +
            'Rs 300 free credit:' +
            ' #BeOutfix';

        var url =
            'http://twitter.com/share?text=' +
            encodeURIComponent(tweet) +
            '&url=/';
        var width = 575,
            height = 400,
            left = ($(window).width() - width) / 2,
            top = ($(window).height() - height) / 2,
            opts =
                'status=1' +
                ',width=' +
                width +
                ',height=' +
                height +
                ',top=' +
                top +
                ',left=' +
                left;
        window.open(url, 'twitter', opts);
        return false;
    },

    getCurrentUser: () => {
        var user;
        $.ajax({
            url: "/Home/GetCurrentCustomer",
            type: "GET",
            async: false,
            success: function (data) {
                user = data;
            },
            error: function (er) {
                alert(er);
            }

        });
        return user;
    }
}