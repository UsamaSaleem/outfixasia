﻿$(function () {

    $("#btnRegister").on('click', function (e) {
        e.preventDefault(); // prevent the form's normal submission
        $("#RegHeading").show();
        $("#divRegister").show();
        $("#btnLogin").show();

        $("#LoginHeading").hide();
        $("#divLogin").hide();
        $("#btnRegister").hide();

    });

    $("#btnLogin").on('click', function (e) {
        e.preventDefault(); // prevent the form's normal submission
        $("#LoginHeading").show();
        $("#divLogin").show();
        $("#btnRegister").show();

        $("#RegHeading").hide();
        $("#divRegister").hide();
        $("#btnLogin").hide();

    });
});


Navbar = {

    Men_appear: () => {

        $("#divSubmenu").show();
        $("#Men").show();
        $("#Women").hide();
        $("#Kids").hide();
    },
    Women_appear: () => {
        $("#divSubmenu").show();
        $("#Men").hide();
        $("#Women").show();
        $("#Kids").hide();
    },
    Kids_appear: () => {
        $("#divSubmenu").show();
        $("#Men").hide();
        $("#Women").hide();
        $("#Kids").show();
    },
    SubMenuDissapear: (menu) => {
        $("#divSubmenu").hide();
        $(menu).hide();
    },

    Men_mbl_appear: () => {
        var x = document.getElementById("Men_Mobile");
        var y = document.getElementById("Women_Mobile");
        var z = document.getElementById("Kids_Mobile");
        var w = document.getElementById("link_men");
        var s = document.getElementById("link_women");
        var t = document.getElementById("link_kids");

        var Men_Pointer = document.getElementById("Men_Pointer");
        var Women_Pointer = document.getElementById("Women_Pointer");
        var Kids_Pointer = document.getElementById("Kids_Pointer");

        if (x.style.display == "none" || x.style.display == "") {
            $("#Men_Mobile").show("fast", function () {
                x.style.display = "flex";
                y.style.display = "none";
                z.style.display = "none";
                w.style.color = "orange";
                s.style.color = "white";
                t.style.color = "white";
                Men_Pointer.style.display = "block";
                Women_Pointer.style.display = "none";
                Kids_Pointer.style.display = "none";
            });
        }
        else {
            $("#Men_Mobile").hide("fast", function () {
                x.style.display = "none";
                w.style.color = "white";
                Men_Pointer.style.display = "none";

            });
        }
    },
    Women_mbl_appear: () => {
        var x = document.getElementById("Women_Mobile");
        var y = document.getElementById("Men_Mobile");
        var z = document.getElementById("Kids_Mobile");
        var w = document.getElementById("link_women");
        var s = document.getElementById("link_kids");
        var t = document.getElementById("link_men");
        var Men_Pointer = document.getElementById("Men_Pointer");
        var Women_Pointer = document.getElementById("Women_Pointer");
        var Kids_Pointer = document.getElementById("Kids_Pointer");

        if (x.style.display == "none" || x.style.display == "") {
            $("#Women_Mobile").show("fast", function () {
                x.style.display = "flex";
                y.style.display = "none";
                z.style.display = "none";
                w.style.color = "orange";
                s.style.color = "white";
                t.style.color = "white";

                Men_Pointer.style.display = "none";
                Women_Pointer.style.display = "block";
                Kids_Pointer.style.display = "none";
            });
        }
        else {
            $("#Women_Mobile").hide("fast", function () {
                x.style.display = "none";
                w.style.color = "white";
                Women_Pointer.style.display = "none";
            });
        }
    },
    Kids_mbl_appear: () => {
        var x = document.getElementById("Kids_Mobile");
        var y = document.getElementById("Men_Mobile");
        var z = document.getElementById("Women_Mobile");
        var w = document.getElementById("link_kids");
        var s = document.getElementById("link_women");
        var t = document.getElementById("link_men");
        var Men_Pointer = document.getElementById("Men_Pointer");
        var Women_Pointer = document.getElementById("Women_Pointer");
        var Kids_Pointer = document.getElementById("Kids_Pointer");

        if (x.style.display == "none" || x.style.display == "") {
            $("#Kids_Mobile").show("fast", function () {
                y.style.display = "none";
                z.style.display = "none";
                x.style.display = "flex";
                w.style.color = "orange";
                s.style.color = "white";
                t.style.color = "white";
                Men_Pointer.style.display = "none";
                Women_Pointer.style.display = "none";
                Kids_Pointer.style.display = "block";
            });
        }
        else {
            $("#Kids_Mobile").hide("fast", function () {
                x.style.display = "none";
                w.style.color = "white";
                Kids_Pointer.style.display = "none";
            });
        }
    },

    Box_appear: () => {
        var x = document.getElementById("Box");
        var y = document.getElementById("Wallet");
        var z = document.getElementById("Profile_Div");

        var Box_Pointer = document.getElementById("Box_Pointer");
        var Wallet_Pointer = document.getElementById("Wallet_Pointer");
        var Profile_Pointer = document.getElementById("Profile_Pointer");

        if (x.style.display == "none" || x.style.display == "") {
            $("#Box").fadeIn("fast", function () {
                x.style.display = "block";
                Box_Pointer.style.display = "block";
            });
            if (y.style.display == "block") {
                $("#Wallet").fadeToggle();
            }
            if (z.style.display == "block") {
                $("#Profile_Div").fadeToggle();
            }
            if (Wallet_Pointer.style.display == "block") {
                Wallet_Pointer.style.display = "none";
            }
            if (Profile_Pointer.style.display == "block") {
                Profile_Pointer.style.display = "none";
            }
        }
        else {
            $("#Box").fadeOut("fast", function () {
                x.style.display = "none";
            });
        }
    },

    Wallet_appear: () => {
        var x = document.getElementById("Wallet");
        var y = document.getElementById("Box");
        var z = document.getElementById("Profile_Div");

        var Box_Pointer = document.getElementById("Box_Pointer");
        var Wallet_Pointer = document.getElementById("Wallet_Pointer");
        var Profile_Pointer = document.getElementById("Profile_Pointer");

        if (x.style.display == "none" || x.style.display == "") {
            $("#Wallet").fadeIn("fast", function () {
                x.style.display = "block";
                Wallet_Pointer.style.display = "block";
            });
            if (y.style.display == "block") {
                $("#Box").fadeToggle();
            }
            if (z.style.display == "block") {
                $("#Profile_Div").fadeToggle();
            }
            if (Box_Pointer.style.display == "block") {
                Box_Pointer.style.display = "none";
            }
            if (Profile_Pointer.style.display == "block") {
                Profile_Pointer.style.display = "none";
            }
        }
        else {
            $("#Wallet").fadeOut("fast", function () {
                x.style.display = "none";
            });
        }
    },

    Profile_appear: () => {
        var x = document.getElementById("Profile_Div");
        var y = document.getElementById("Box");
        var z = document.getElementById("Wallet");

        var Box_Pointer = document.getElementById("Box_Pointer");
        var Wallet_Pointer = document.getElementById("Wallet_Pointer");
        var Profile_Pointer = document.getElementById("Profile_Pointer");

        if (x.style.display == "none" || x.style.display == "") {
            y.style.display = "none";
            z.style.display = "none";
            $("#Profile_Div").fadeIn("fast", function () {
                x.style.display = "block";
                Profile_Pointer.style.display = "block";
            });
            if (y.style.display == "block") {
                $("#Box").fadeToggle();
            }
            if (z.style.display == "block") {
                $("#Wallet").fadeToggle();
            }
            if (Wallet_Pointer.style.display == "block") {
                Wallet_Pointer.style.display = "none";
            }
            if (Box_Pointer.style.display == "block") {
                Box_Pointer.style.display = "none";
            }
        }
        else {
            $("#Profile_Div").fadeOut("fast", function () {
                x.style.display = "none";
            });
        }
    },

}
