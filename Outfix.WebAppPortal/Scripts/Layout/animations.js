
$(document).ready(function () {

    var zoom_container_size = $('.zoom_container').height();
    var zoom_area_size = 100;
    var zoom_radius = zoom_area_size / 2;

    $('.thumbnail').mousemove(function (e) {
        // Show original picture    
        var $original = $('#picture_original');
        var $container = $original.parent();
        $container.removeClass('hidden');
        // Thumbnail
        var offset = $(this).offset();
        var tX = e.pageX - offset.left;
        var tY = e.pageY - offset.top;
        // We stay inside the limits of the zoomable area
        tX = Math.max(zoom_radius, Math.min($(this).width() - zoom_radius, tX));
        tY = Math.max(zoom_radius, Math.min($(this).height() - zoom_radius, tY));
        // Ratios
        var ratioX = ($original.width() - zoom_container_size) / ($(this).width() - zoom_area_size);
        var ratioY = ($original.height() - zoom_container_size) / ($(this).height() - zoom_area_size);
        // Margin to be set in the original    
        var moX = -Math.floor((tX - zoom_radius) * ratioX);
        var moY = -Math.floor((tY - zoom_radius) * ratioY);
        // Apply zoom efect
        $original.css('marginLeft', moX);
        $original.css('marginTop', moY);
        // Log values
        $('#ratios').html('Ratio X: <b>' + ratioX + '</b><br>Ratio Y: <b>' + ratioY + '</b>');
        $('#coordinates_thumbnail').html('tX: <b>' + tX + '</b><br>tY: <b>' + tY + '</b>');
        $('#coordinates_original').html('Margin left: <b>' + Math.round(moX) + '</b><br>Margin top: <b>' + moY + '</b>');
    });

    $('.thumbnail').mouseout(function (e) {
        var $original = $('#picture_original');
        var $container = $original.parent();
        $container.addClass('hidden');
    });

    $('body').click(function () {
        $("#Box").hide();
        $("#Wallet").hide();
        $("#Profile_Div").hide();
        $("#Men_Mobile").hide();
        $("#Women_Mobile").hide();
        $("#Kids_Mobile").hide();
        $("#Kids").hide();
        $("#Women").hide();
        $("#Men").hide();
        $("#Profile_Pointer").hide();
        $("#Box_Pointer").hide();
        $("#Wallet_Pointer").hide();
    });
})

function Show_FIT() {
    var x = document.getElementById("FIT_DIV");
    if (x.style.display === "none") {
        $("#STATS_DIV").fadeOut("slow", function () {
            x.style.display = "none";
        });
        $(".First").fadeOut("slow", function () {
            x.style.display = "none";
        });
        $("#FIT_DIV").fadeIn("slow", function () {
            x.style.display = "block";
        });
        $(".Second").fadeIn("slow", function () {
            x.style.display = "block";
        });
    }
}
function Show_STYLE() {
    var x = document.getElementById("STYLE_DIV");
    if (x.style.display === "none") {
        $("#FIT_DIV").fadeOut("slow", function () {
            x.style.display = "none";
        });
        $(".Second").fadeOut("slow", function () {
            x.style.display = "none";
        });
        $("#STYLE_DIV").fadeIn("slow", function () {
            x.style.display = "block";
        });
        $(".Third").fadeIn("slow", function () {
            x.style.display = "block";
        });
    }
}
function STYLE_to_FIT() {
    var x = document.getElementById("FIT_DIV");
    if (x.style.display === "none") {
        $("#STYLE_DIV").fadeOut("slow", function () {
            x.style.display = "none";
        });
        $(".Third").fadeOut("slow", function () {
            x.style.display = "none";
        });
        $("#FIT_DIV").fadeIn("slow", function () {
            x.style.display = "block";
        });
        $(".Second").fadeIn("slow", function () {
            x.style.display = "block";
        });
    }
}
function FIT_to_STATS() {
    var x = document.getElementById("STATS_DIV");
    if (x.style.display === "none") {
        $("#FIT_DIV").fadeOut("slow", function () {
            x.style.display = "none";
        });
        $(".Second").fadeOut("slow", function () {
            x.style.display = "none";
        });
        $("#STATS_DIV").fadeIn("slow", function () {
            x.style.display = "block";
        });
        $(".First").fadeIn("slow", function () {
            x.style.display = "block";
        });
    }
}

$(document).ready(function () {

    $('.color_selection_border.active').css("border", "2px solid green");
    $('.color_selection_border').click(function () {
        $('.color_selection_border:not(.color_selection_border_active)').css("border", "");
        $(this).css("border", "2px solid green");
        $(this).css("border-radius", "2px");
    });

    $('.vid').on('mousedown', function (event) {
        event.preventDefault();
    });

    $('#menu').on('mousedown', function (event) {
        event.preventDefault();
    });
})

function valueChanged() {
    if ($('.diff_ship').is(":checked")) {
        $("#Home_Address").prop("style").setProperty("display", "none");
        $("#Shipment_Address").prop("style").setProperty("display", "inline-block");
    }
    else {
        $("#Home_Address").prop("style").setProperty("display", "inline-block");
        $("#Shipment_Address").prop("style").setProperty("display", "none");
    }
    // $('.diff_ship').prop('checked', false);
}
function valueChanged1() {
    if ($('.same_ship').is(":checked")) {
        $(".diff_address").hide();
        $(".same_address").show();
    }
    else {
        $(".same_address").hide();
        $(".diff_address").show();
    }
    $('.same_ship').prop('checked', false);
}

$(document).scroll(function () {
    var y = $(this).scrollTop();
    if (y > 300) {
        $('.started').fadeIn();
    } else {
        $('.started').fadeOut();
    }
});

function myFunction() {
    var x = document.getElementById("mobile_options");
    if (x.style.display == "none" || x.style.display == "") {
        $("#mobile_options").fadeIn("slow", function () {
            x.style.display = "block";
        });
    }
    else {
        $("#mobile_options").fadeOut("slow", function () {
            x.style.display = "none";
        });
    }
}

function openCity(evt, cityName) {
    var i, tabcontent, tablinks;
    tabcontent = document.getElementsByClassName("tabcontent");
    for (i = 0; i < tabcontent.length; i++) {
        tabcontent[i].style.display = "none";
    }
    tablinks = document.getElementsByClassName("tablinks");
    for (i = 0; i < tablinks.length; i++) {
        tablinks[i].className = tablinks[i].className.replace(" active", "");
    }
    document.getElementById(cityName).style.display = "block";
    evt.currentTarget.className += " active";
}

if ($(window).width() > 640) {

    $(document).ready(function () {
        $(document).on('mouseover', '.item_border', function () {
            $(this).find(".start1").show();
        }).on('mouseout', '.item_border', function () {
            $(this).find(".start1").hide();
        });
    });


}

$(document).ready(function () {
    $(".diff_address").hide();

    $('.vid').on('mousedown', function (event) {
        event.preventDefault();
    });

    $('#menu').on('mousedown', function (event) {
        event.preventDefault();
    });
})

var Picture = {
    PDesImage: (img) => {
        var src = img.getAttribute('data-content');
        var po = $('#picture_original');
        po.attr('src', src);

        var pz = $('#pic_zoom');
        pz.attr('src', src);
    },
}



function Show_What() {
    var x = document.getElementById("What_Section");
    var y = document.getElementById("How_Section");
    var z = document.getElementById("Profile_Style_Section");
    var w = document.getElementById("Billing_Section");
    var t = document.getElementById("Scheduling_Section");
    y.style.display = "none";
    z.style.display = "none";
    w.style.display = "none";
    t.style.display = "none";
    if (x.style.display === "none") {
        $("#What_Section").fadeIn("slow", function () {
            x.style.display = "block";
        });
    }
    else {
        $("#What_Section").fadeOut("slow", function () {
            x.style.display = "none";
        });
    }
}

function Show_How() {
    var x = document.getElementById("What_Section");
    var y = document.getElementById("How_Section");
    var z = document.getElementById("Profile_Style_Section");
    var w = document.getElementById("Billing_Section");
    var t = document.getElementById("Scheduling_Section");
    x.style.display = "none";
    z.style.display = "none";
    w.style.display = "none";
    t.style.display = "none";
    if (y.style.display === "none") {
        $("#How_Section").fadeIn("slow", function () {
            y.style.display = "block";
        });
    }
    else {
        $("#How_Section").fadeOut("slow", function () {
            y.style.display = "none";
        });
    }
}

function Show_Style_Profile() {
    var x = document.getElementById("What_Section");
    var y = document.getElementById("How_Section");
    var z = document.getElementById("Profile_Style_Section");
    var w = document.getElementById("Billing_Section");
    var t = document.getElementById("Scheduling_Section");
    x.style.display = "none";
    y.style.display = "none";
    w.style.display = "none";
    t.style.display = "none";
    if (z.style.display === "none") {
        $("#Profile_Style_Section").fadeIn("slow", function () {
            z.style.display = "block";
        });
    }
    else {
        $("#Profile_Style_Section").fadeOut("slow", function () {
            z.style.display = "none";
        });
    }
}

function Show_Billing() {
    var x = document.getElementById("What_Section");
    var y = document.getElementById("How_Section");
    var z = document.getElementById("Profile_Style_Section");
    var w = document.getElementById("Billing_Section");
    var t = document.getElementById("Scheduling_Section");
    x.style.display = "none";
    y.style.display = "none";
    z.style.display = "none";
    t.style.display = "none";
    if (w.style.display === "none") {
        $("#Billing_Section").fadeIn("slow", function () {
            w.style.display = "block";
        });
    }
    else {
        $("#Billing_Section").fadeOut("slow", function () {
            w.style.display = "none";
        });
    }
}

function Show_Scheduling() {
    var x = document.getElementById("What_Section");
    var y = document.getElementById("How_Section");
    var z = document.getElementById("Profile_Style_Section");
    var w = document.getElementById("Billing_Section");
    var t = document.getElementById("Scheduling_Section");
    x.style.display = "none";
    y.style.display = "none";
    z.style.display = "none";
    w.style.display = "none";
    if (t.style.display === "none") {
        $("#Scheduling_Section").fadeIn("slow", function () {
            t.style.display = "block";
        });
    }
    else {
        $("#Scheduling_Section").fadeOut("slow", function () {
            t.style.display = "none";
        });
    }
}
