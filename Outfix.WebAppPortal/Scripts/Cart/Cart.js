﻿$(function () {

    $('#coupon_applied').on('submit', function (e) {
        e.preventDefault();
        var cpn = document.getElementsByName("coupon");
        Cart.checkCoupon(cpn[0].value);
    });

    //$(window).keydown(function (event) {
    //    if (event.keyCode == 13) {
    //        event.preventDefault();
    //        return false;
    //    }
    //});

    $('#coupon_info').on('click', function (e) {
        e.preventDefault();
        $(this).popover('show');
    });

});

var Cart = {
    btnClickAddToCart: (btn) => {
        var json = $(btn).attr("data-content");
        var id = btn.id;
        var itemArr = id.split('_');
        var qty = $("#Qty_" + itemArr[1]).val();
        var price = $("#ProductPrice").attr("data-content");
        Cart.AddToCart(json, itemArr[1], qty, price);
    },
    onChangeQty: (c, p, v) => {
        var tp = $("#ProdTotalPrice_" + c).val(p * v);

        $.ajax({
            url: "/Cart/ChangeQty",
            type: "POST",
            data: { id: c, qty: v },
            success: function (data) {

            }
        });

    },
    checkCoupon: (cpn) => {
        if (cpn != "" && cpn != null && cpn != undefined) {

            $.ajax({
                url: "/Cart/ValidateCoupon",
                type: "POST",
                data: { cpn },
                success: function (data) {
                    if (data.validate) {
                        $('#valid_cpn').show();
                        setTimeout(function () {
                            $('#valid_cpn').fadeOut('slow');
                        }, 3000);
                        $('#coupon_value').html(cpn);

                        $('#coupon_info').attr('data-content', "Discount: Rs." + data.obj.WalletPoints);
                        $('#coupon_info').show();

                        $('#invalid_cpn').hide();
                    }
                    else {
                        $('#invalid_cpn').html("*Invalid Coupon");
                        $('#invalid_cpn').show();
                        setTimeout(function () {
                            $('#invalid_cpn').fadeOut('slow');
                        }, 3000);

                        $('#coupon_info').hide();
                        $('#valid_cpn').hide();
                    }
                },
                error: function (er) {
                    alert(er);
                }
            });
        }
        else {
            $('#invalid_cpn').html("No Coupon Applied");
            $('#invalid_cpn').show();
            setTimeout(function () {
                $('#invalid_cpn').fadeOut('slow');
            }, 3000);
            $('#coupon_info').hide();

        }
    },

    AddToCart: (data, code, qty, price) => {
        var obj = new Object();
        var arr = data.split(',');
        for (let i = 0; i < arr.length; i++) {
            var arrObj = arr[i].split(':');
            obj[arrObj[0]] = arrObj[1];
        }

        var cart = new Object();
        cart.Id = code;
        cart.Variation = obj;
        cart.Quantity = qty;
        cart.Price = price

        var str = JSON.stringify(cart);
        $.ajax({
            url: "/Cart/AddToCart",
            type: "POST",
            data: { str },
            beforeSend: function (jqXHR, settings) {
                if (settings.data != null) {
                    var data = decodeURIComponent(settings.data);
                    if (data != null) {
                        data = data.split('=');
                        if (data[1] != null) {
                            var obj = JSON.parse(data[1]);
                            PPage.loader(obj.Id);
                        }
                    }
                }
            },
            success: function (data) {

                //code after success
                let l = "#ld_" + data.Id;
                $(l).hide();
                if (data.msg != "") {
                    //alert(data.msg);

                    $("#msg_heading").html("Cannot Add Product to Cart.......!!!");
                    $("#msg_body").html(data.msg);
                    $("#msg_notification").show();
                }
                else {
                    let b = "#btnCart_" + data.Id;
                    $(b).val("Product Added");

                    $('#cartCount').html(data.count);
                    $('#cartCount').show();
                }

                if (data.count == 0) {
                    $('#cartCount').hide();
                }
            },
            complete: function (jqXHR, settings) {
                Cart.UpdateNavbarCart();
            },
            error: function (er) {
                alert(er);
            }

        });
    },

    UpdateNavbarCart: () => {
        $.ajax({
            url: "/Cart/NavbarCart",
            success: function (data) {
                $('#Box').html(data);
            },
            error: function (er) {
                alert(er);
            }

        });
    },
    RemoveDiv: (divId) => {
        $('#' + divId).remove();
        var id = divId.split("_")[1];

        $.ajax({
            url: "/Cart/DeleteFromCart",
            type: "POST",
            data: { code: id },
            success: function (data) {

                $('#cartCount').html(data.count);
                $('#cartCount').show();

                if (data.count == 0) {
                    $('#cartCount').hide();
                }
            },
            complete: function (jqXHR, settings) {
                Cart.UpdateNavbarCart();
            },
            error: function (er) {
                alert(er);
            }
        })
    },
}