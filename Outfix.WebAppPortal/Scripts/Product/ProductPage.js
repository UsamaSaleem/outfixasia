﻿$(function () {
    $("#frmPriceFilter").on('submit', function (e) {
        e.preventDefault(); // prevent the form's normal submission
        PPage.filterByPrice();
    });

    $('#search_input').keyup(function (e) {
        clearTimeout($.data(this, 'timer'));
        if (e.keyCode == 13)
            search(true);
        else
            $(this).data('timer', setTimeout(search, 500));
    });
    $('#mbl_search_input').keyup(function (e) {
        clearTimeout($.data(this, 'timer'));
        if (e.keyCode == 13)
            search(true);
        else
            $(this).data('timer', setTimeout(searchMbl, 500));
    });

    $('#SubmitSearch').on('submit', function (e) {
        e.preventDefault();
        var str = $('#search_input').val();

        args = "search_input" + "=" + str;
        var nUrl = PPage.makeURL(args);
        window.location.replace(nUrl);
    });

    function search(force) {
        var existingString = $("#search_input").val();
        if (!force && existingString.length < 3) {
            if (existingString.length === 0) {
                $('#SearchResults').html("");
                $('#SearchResults').hide();
            }
            return; //wasn't enter, not > 2 char
        }
        PPage.Search(existingString, "web");
    }
    function searchMbl(force) {
        var existingString = $("#mbl_search_input").val();
        if (!force && existingString.length < 3) {
            if (existingString.length === 0) {
                $('#search_results_mobile').html("");
                $('#search_results_mobile').hide();
            }
            return; //wasn't enter, not > 2 char
        }
        PPage.Search(existingString, "mbl");
    }
});


var PPage = {
    filterByPrice: () => {
        var min = $('#min').val();
        var max = $('#max').val();

        args = "PMin=" + min + "&PMax=" + max;
        var nUrl = PPage.makeURL(args);
        window.location.replace(nUrl);

    },
    filterByRating: (rating) => {
        args = "Rt=" + rating;
        var nUrl = PPage.makeURL(args);
        window.location.replace(nUrl);
    },
    universalFilter: (type, value) => {
        args = type + "=" + value;
        var previous = window.location.href;
        var nUrl = '';
        if (previous.includes(args)) {
            var splt = previous.split(args);
            if (splt[0][splt[0].length - 1] === "&") {
                splt[0] = splt[0].substring(0, splt[0].length - 1);
            }
            nUrl = splt[0] + splt[1];
        }
        else {
            nUrl = PPage.makeURL(args);
        }

        window.location.replace(nUrl);
    },
    makeURL: (newArgs) => {
        var previous = window.location.href;
        if (!previous.includes("ProductsPage")) {
            previous = window.location.origin + "/Product/ProductsPage/";
        }

        var pArgs = "";
        var newUrl = "";

        if (previous.includes("?")) {
            pArgs = previous.split("?")[1].split("&");
            newUrl = previous.split("?")[0] + "?";
        }
        else {
            newUrl = previous + "?";
        }
        var nArgs = newArgs.split("&");

        newArgsId = newArgs.split("=")[0];
        newArgsValue = newArgs.split("=")[1];
        ArgsWithoutEncoding = '';
        for (let i = 0; i < pArgs.length; i++) {
            let value = pArgs[i].split("=");
            let exist = nArgs.find(x => x.split("=")[0] == value[0]) != null;
            if (!exist) {
                ArgsWithoutEncoding += value[0] + "=" + value[1] + "&";
            }
        }
        for (let i = 0; i < nArgs.length; i++) {
            let value = nArgs[i].split("=");
            ArgsWithoutEncoding += value[0] + "=" + value[1] + "&";
        }
        ArgsWithoutEncoding = ArgsWithoutEncoding.substring(0, ArgsWithoutEncoding.length - 1);
        newUrl = newUrl + ArgsWithoutEncoding;
        return newUrl;
    },
    loader: (id) => {
        var l = "#ld_" + id;
        $(l).show();
    },
    Search: (data, cntnt) => {
        $.ajax({
            url: "/Product/searchProduct",
            type: "GET",
            data: { str: data, device: cntnt },
            success: function (data) {
                if (data === "mbl") {
                    $('#search_results_mobile').html(data);
                    $('#search_results_mobile').show();
                }
                else {
                    $('#SearchResults').html(data);
                    $('#SearchResults').show();
                }

            },
            error: function (er) {
                alert(er);
            }
        });
    },
    ClickSearchItem: (data) => {
        args = "search_input" + "=" + data;
        var nUrl = PPage.makeURL(args);
        window.location.replace(nUrl);
    },
    ddlValueChange: (ddl) => {
        var val = "";
        var id = "";
        var name = "";
        var value = "";

        if (ddl.selectedIndex != null && ddl.selectedIndex != undefined && ddl.selectedIndex != "") {
            val = ddl[ddl.selectedIndex].getAttribute("data-content");
            id = ddl.id;
            name = ddl.name;
            value = ddl.value;
        }
        else {
            val = ddl.getAttribute("data-content");
            id = ddl.getAttribute("id");
            name = ddl.getAttribute("name");
            value = ddl.getAttribute("value");
        }

        if (val != null && val != undefined) {
            val = val.substring(0, val.length - 1);
            $.ajax({
                url: "/Product/getDDLs",
                type: "GET",
                data: { ids: val, itemcode: id, field: name, value: value },
                success: function (data) {
                    $('#product_variation').html(data);
                },
                error: function (er) {
                    alert(er);
                }
            });
        }
        else {
            //var reset = ddl[ddl.selectedIndex].getAttribute("value");
            //if (reset == "clear") {
            //    $.ajax({
            //        url: "/Product/clearDDLs",
            //        type: "GET",
            //        data: { itemcode: id, field: name },
            //        success: function (data) {
            //            $('#product_variation').html(data);
            //        },
            //        error: function (er) {
            //            alert(er);
            //        }
            //    });
            //}
        }

    },
    ClearAllDDLs: (ic) => {
        if (ic != null && ic != undefined) {
            $.ajax({
                url: "/Product/clearAllDDLs",
                type: "GET",
                data: { Code: ic },
                success: function (data) {
                    $('#product_variation').html(data);
                },
                error: function (er) {
                    alert(er);
                }
            });
        }
    },
    sortBy: (ddl) => {
        var data = ddl.value;
        args = "SB" + "=" + data;
        var nUrl = PPage.makeURL(args);
        window.location.replace(nUrl);
    },
    IFrame: (data) => {
        $.ajax({
            url: "/Product/ProductsDetailsPage",
            type: "GET",
            data: { id: data },
            success: function (data) {
                $('#divIFrame').html(data);
            },
            error: function (er) {
                alert(er);
            }
        });
    }
}