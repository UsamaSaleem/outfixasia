﻿using Outfix.Commons;
using Outfix.Models;
using Outfix.ViewModels;
using System.Collections.Generic;
using System.Web.Mvc;

namespace Outfix.WebAppPortal.Controllers
{
    [RequireHttps]
    public class HomeController : Controller
    {
        WP_Common common = new WP_Common();
        public ActionResult Index()
        {

            var msg = ViewBag.Message != null ? ViewBag.Message : "";
            return View();
        }

        public ActionResult FAQs()
        {
            return View();
        }
        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }
        public ActionResult FAQ()
        {
            return View();
        }
        public ActionResult PrivacyPolicy()
        {
            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
        [HttpPost]
        public ActionResult PlaceOrder(CartViewModel model)
        {
            if (Session.Count > 0)
            {
                var cart = (List<CartModel>)Session["Cart"];
                var customer = (CustomerModel)Session["Customer"];
                if (model.SaveLocation)
                {
                    customer.FirstName = model.Customer.FirstName;
                    customer.LastName = model.Customer.LastName;
                    customer.Email = model.Customer.Email;
                    customer.Contact1 = model.Customer.Contact1;
                    customer.Contact2 = model.Customer.Contact2;
                    customer.Location = new LocationModel()
                    {
                        Latitude = model.Customer.Location.Latitude,
                        Longitude = model.Customer.Location.Longitude,
                        Address = model.Customer.Location.Address,
                        City = model.Customer.Location.City,
                        Country = model.Customer.Location.Country,
                    };
                }
                var coupon = Session["CartCoupon"] != null ? (ReferralModel)Session["CartCoupon"] : null;
                model.Customer = customer;
                cart.ForEach(x =>
                {
                    var Variation = new List<ProductVariationModel>();
                    x.Variation.Qty = x.Quantity;

                    Variation.Add(x.Variation);

                    model.Product.Add(new ProductModel()
                    {
                        ItemCode = x.Id,
                        Variation = Variation
                    });
                });
                if (coupon != null)
                {
                    model.Voucher = coupon;
                }
                common.PlaceOrder(model);

                Session.Remove("Cart");
                Session.Remove("CartCount");
                Session.Remove("VariationValues");
            }
            return RedirectToAction("Index");
        }

        public ActionResult Logout()
        {
            Session.Remove("Customer");
            return RedirectToAction("Index");
        }
        public JsonResult GetCurrentCustomer()
        {
            CustomerModel customer = new CustomerModel();
            if (Session["Customer"] != null)
            {
                customer = (CustomerModel)Session["Customer"];
            }

            return Json(customer, JsonRequestBehavior.AllowGet);
        }
        public ActionResult Register(string id)
        {
            if (!string.IsNullOrEmpty(id))
            {
                RegisterViewModel model = new RegisterViewModel();
                model.Refferal = id;
                return View(model);
            }

            // If we got this far, something failed, redisplay form
            return View("~/Views/Shared/Error.cshtml");
        }
        public ActionResult Error()
        {
            return View("Error");
        }
    }
}