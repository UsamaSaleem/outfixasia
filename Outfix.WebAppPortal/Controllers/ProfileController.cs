﻿using Facebook;
using Newtonsoft.Json;
using Outfix.Commons;
using Outfix.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Dynamic;
using System.IO;
using System.Linq;
using System.Web.Mvc;

namespace Outfix.WebAppPortal.Controllers
{
    public class ProfileController : Controller
    {
        WP_Common commons;
        StyleProfileSurveyModel surveyCust;
        public ProfileModel profile { get; set; }

        public ProfileController()
        {

        }
        // GET: Profile
        public ActionResult Index()
        {
            commons = new WP_Common();
            var customer = (CustomerModel)Session["Customer"];
            profile = new ProfileModel();
            if (customer != null)
            {
                surveyCust = new StyleProfileSurveyModel();
            }

            surveyCust.Customer = customer;
            var srv = commons.GetSurveyForUser(surveyCust);

            profile.StyleProfile = commons.getAllQuestionAnswers();
            profile.Customer = customer;
            profile.UserAnswers = srv;
            //profile.ReferCode = commons.getCustomerReferCode(customer.ID);
            //shareToFb();
            return View(profile);
        }

        public ActionResult Refer()
        {
            commons = new WP_Common();
            var customer = (CustomerModel)Session["Customer"];
            profile = new ProfileModel();
            profile.Customer = customer;
            
            return View(profile);
        }

        public ActionResult OrderReview(int id)
        {
            commons = new WP_Common();
            var path = ConfigurationManager.AppSettings["ServerPath"].ToString();
            var customer = (CustomerModel)Session["Customer"];
            profile = new ProfileModel();
            profile.Customer = customer;
            profile.Customer.Orders = new List<OrderModel>();
            var order = commons.GetOrderDetails(id);
            order.Products.ForEach(y => y.Picture1 = string.IsNullOrEmpty(y.Variation.FirstOrDefault().Picture) ? "" : Path.Combine(path, y.Variation.FirstOrDefault().Picture));
            profile.Customer.Orders.Add(order);
            
            return View(profile);
        }
        public ActionResult OrderHistory()
        {
            var path = ConfigurationManager.AppSettings["ServerPath"].ToString();

            commons = new WP_Common();
            var customer = (CustomerModel)Session["Customer"];
            profile = new ProfileModel();
            profile.Customer = customer;
            profile.Customer.Orders = commons.getCustomerOrders(customer.ID);
            profile.Customer.Orders.ForEach(y =>
            {
                y.Products.ForEach(x =>
                {
                    x.Picture1 = string.IsNullOrEmpty(x.Picture1) ? "" : Path.Combine(path, x.Picture1);
                });
            });
            return View(profile);
        }
        [HttpPost]
        public void SaveSurveyForm(String str)
        {
            commons = new WP_Common();
            var Survey = JsonConvert.DeserializeObject<List<StyleProfileUserAnswersModel>>(str);
            var Cust = (CustomerModel)(Session["Customer"]);
            surveyCust = new StyleProfileSurveyModel();
            surveyCust.Customer = Cust;
            surveyCust.survey = Survey;
            commons.CreateSurveyForUser(surveyCust);

        }
        [HttpGet]
        public void GetSurveyForm()
        {
            commons = new WP_Common();
            var Cust = (CustomerModel)(Session["Customer"]);
            surveyCust = new StyleProfileSurveyModel();
            surveyCust.Customer = Cust;
            commons.GetSurveyForUser(surveyCust);

        }

        public void shareToFb()
        {
            var fbtoken = "";
            if(Session["AuthIdFb"] != null)
            {
                fbtoken = Session["AuthIdFb"].ToString();
            }
            dynamic messagePost = new ExpandoObject();

            messagePost.name = "Post For Fun"; // "{*actor*} " + "posted news...";
            messagePost.caption = "Hello Guys";
            messagePost.description = "Just for checking";
            messagePost.message = "No hidden Message";
            string acccessToken = "439132446670385|bf3c34a62fd83c2bd2f13e332cb57633";
            var appp = new FacebookClient(acccessToken);
            // Calling Graph API for user info
            dynamic me = appp.Get(fbtoken+"?fields=friends,name,email");
            //dynamic me = appp.Get(fbtoken);

            string id = me.id; // You can store it in the database
            string name = me.name;
            string email = me.email;

            //var client = new FacebookClient("asd");
            var args = new Dictionary<string, object>();
            args["message"] = "Just for checking";

            var path =  id + "/feed";
            try
            {
                //var postId = appp.Post(path + "/feed", messagePost);
                appp.Post(path, args);
            }
            catch (FacebookOAuthException ex)
            { 
                //handle oauth exception } catch (FacebookApiException ex) { //handle facebook exception
            }
        }

        #region [Map]    
        [HttpPost]
        public JsonResult GetMap()
        {
            var data1 = GetMapModel();
            return Json(data1, JsonRequestBehavior.AllowGet);
        }

        public ActionResult UserLocation()
        {
            return View();
        }
        public List<MapModel> GetMapModel()
        {
            var model = new List<MapModel>();
            //return (from p in db.
            //        select new
            //        {
            //            Name = p.Name,
            //            Latitude = p.Latitude,
            //            Longitude = p.Longitude,
            //            Location = p.Location,
            //            Description = p.Description,
            //            Id = p.Id
            //        }).ToList()
            //    .Select(res => new Map
            //    {
            //        Name = res.Name,
            //        Latitude = res.Latitude,
            //        Longitude = res.Longitude,
            //        Location = res.Location,
            //        Description = res.Description,
            //        Id = res.Id


            //    });
            return model;
        }
        #endregion
    }
}