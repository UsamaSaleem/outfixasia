﻿using Newtonsoft.Json;
using Outfix.Commons;
using Outfix.Models;
using Outfix.ViewModels;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Web.Mvc;

namespace Outfix.WebAppPortal.Controllers
{
    public class CartController : Controller
    {
        WP_Common common;

        public CartController()
        {
            common = new WP_Common();
        }
        // GET: Cart
        public ActionResult Index()
        {
            List<CartModel> model = new List<CartModel>();
            CartViewModel cart = new CartViewModel();
            int count = 0;

            try
            {
                var path = ConfigurationManager.AppSettings["ServerPath"].ToString();

                if (Session.Count > 0)
                {
                    if (Session["Cart"] != null)
                    {
                        model = (List<CartModel>)Session["Cart"];
                    }
                    if (Session["CartCoupon"] != null)
                    {
                        cart.Voucher = (ReferralModel)Session["CartCoupon"];
                    }
                }
                foreach (var m in model)
                {
                    var product = common.getProductbyId(m.Id);
                    product.VendorId = m.Vendor;
                    product.Qty = m.Quantity;
                    product.Price = m.Price;

                    count += m.Quantity;
                    product.Picture1 = string.IsNullOrEmpty(product.Picture1) ? "" : Path.Combine(path, product.Picture1);
                    product.Variation.Add(m.Variation);
                    cart.Product.Add(product);

                }

                cart.TotalItems = count;

                return View(cart);
            }
            catch (Exception ex)
            {
                return Json(ex.Message, JsonRequestBehavior.AllowGet);
            }
        }
        public ActionResult Checkout()
        {
            List<CartModel> model = new List<CartModel>();
            CartViewModel cart = new CartViewModel();
            int count = 0;

            try
            {

                if (Session.Count > 0)
                {
                    if (Session["Cart"] != null)
                    {
                        model = (List<CartModel>)Session["Cart"];
                    }
                    if (Session["Customer"] != null)
                    {
                        var customer = (CustomerModel)Session["Customer"];
                        cart.Customer = customer;
                    }
                    if (Session["CartCoupon"] != null)
                    {
                        cart.Voucher = (ReferralModel)Session["CartCoupon"];
                    }
                }
                foreach (var m in model)
                {
                    var product = common.getProductbyId(m.Id);
                    product.VendorId = m.Vendor;
                    product.Qty = m.Quantity;
                    product.Variation.Add(m.Variation);
                    count += m.Quantity;
                    cart.Product.Add(product);
                }

                cart.TotalItems = count;

                return View(cart);
            }
            catch (Exception ex)
            {
                return Json(ex.Message, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public JsonResult AddToCart(String str, string code)
        {
            string msg = "";
            var vendorExceeds = false;

            try
            {

                var path = ConfigurationManager.AppSettings["ServerPath"].ToString();
                CartModel Cart = JsonConvert.DeserializeObject<CartModel>(str);
                List<CartModel> products = new List<CartModel>();
                int count = 0;
                var product = common.getProductbyId(Cart.Id);

                product.Picture1 = string.IsNullOrEmpty(product.Picture1) ? "" : Path.Combine(path, product.Picture1);

                var cm = new CartModel
                {
                    Id = Cart.Id,
                    Name = product.Name,
                    Pic = product.Picture1,
                    Quantity = Cart.Quantity,
                    Price = Cart.Price,
                    Vendor = product.VendorId,
                    Variation = Cart.Variation,

                };
                cm.Variation = common.getVariationProductbyId(cm);

                if (Session.Count > 0)
                {
                    if (Session["Cart"] == null)
                    {
                        count = Cart.Quantity;

                        products.Add(cm);

                        Session["Cart"] = products;
                        Session["CartCount"] = count;
                    }
                    else
                    {
                        count = Convert.ToInt32(Session["CartCount"].ToString());
                        products = (List<CartModel>)Session["Cart"];
                        if (count + Cart.Quantity < 8)
                        {
                            var check = products.SingleOrDefault(x => x.Id == Cart.Id && x.Variation.ID == cm.Variation.ID);
                            var vids = products.GroupBy(x => x.Vendor);
                            if (vids.Count() >= 2)
                            {
                                vendorExceeds = products.Exists(x => x.Vendor == check.Vendor) == false;
                            }
                            if (!vendorExceeds)
                            {
                                if (check == null)
                                {
                                    count += Cart.Quantity;

                                    products.Add(cm);
                                    Session["CartCount"] = count;
                                }
                                else
                                {
                                    if (Cart.Quantity + check.Quantity <= check.Variation.Qty)
                                    {
                                        count += Cart.Quantity;
                                        check.Quantity += Cart.Quantity;
                                        Session["CartCount"] = count;
                                    }
                                    else
                                    {
                                        msg = "Sorry we only have "+ check.Variation.Qty +". Cannot add more than the stock quantity";
                                    }
                                }
                            }
                            else
                            {
                                msg = "You can only purchase from 2 vendors";
                            }
                        }
                        else
                        {
                            msg = "Box can only contain 8 products";
                        }
                        Session["Cart"] = products;
                    }
                }
                else
                {
                    count = Cart.Quantity;

                    products.Add(cm);

                    Session["Cart"] = products;
                    Session["CartCount"] = count;
                }
                return Json(new
                {
                    Id = Cart.Id,
                    products,
                    count,
                    msg = msg
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(ex.Message, JsonRequestBehavior.AllowGet);
            }

        }
        [HttpPost]
        public JsonResult DeleteFromCart(string code)
        {
            var count = 0;
            var products = new List<CartModel>();
            try
            {
                var path = ConfigurationManager.AppSettings["ServerPath"].ToString();
                if (Session.Count > 0)
                {
                    if (Session["Cart"] != null)
                    {
                        count = Convert.ToInt32(Session["CartCount"].ToString());
                        products = (List<CartModel>)Session["Cart"];

                        var dlt = products.FirstOrDefault(x => x.Variation.ID.ToString() == code);
                        if (dlt != null)
                        {
                            count -= dlt.Quantity;
                            products.Remove(dlt);
                        }
                        Session["Cart"] = products;
                        Session["CartCount"] = count;
                    }
                }

                return Json(new
                {
                    products,
                    count
                }, JsonRequestBehavior.AllowGet);

            }
            catch (Exception ex)
            {
                return Json(ex.Message, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public JsonResult ChangeQty(String id, string qty)
        {
            int tqty = 0;
            var msg = "";
            try
            {
                var products = (List<CartModel>)Session["Cart"];
                var check = products.SingleOrDefault(x => x.Variation.ID.ToString() == id);
                if (check != null)
                {
                    check.Quantity = Convert.ToInt32(qty);
                }
                products.ForEach(x => tqty += x.Quantity);
                if (tqty > 8)
                {
                    msg = "Box can only contain 5 products";
                }
                return Json(new
                {
                    products,
                    msg = msg
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(ex.Message, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult NavbarCart()
        {
            try
            {
                var cart = (List<CartModel>)Session["Cart"];
                return View(cart);
            }
            catch (Exception ex)
            {
                return Json(ex.Message, JsonRequestBehavior.AllowGet);
            }
        }
        [HttpPost]
        public JsonResult ValidateCoupon(string cpn)
        {
            try
            {
                var obj = common.ValidateCoupon(cpn);
                if (obj != null)
                {
                    Session["CartCoupon"] = obj;
                    return Json(new { obj, validate = true });
                }
                return Json(false);
            }
            catch (Exception ex)
            {
                return Json(ex.Message, JsonRequestBehavior.AllowGet);
            }
        }
    }
}