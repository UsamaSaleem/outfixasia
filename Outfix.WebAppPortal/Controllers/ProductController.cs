﻿using Outfix.Commons;
using Outfix.Models;
using Outfix.ViewModels;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Web.Mvc;

namespace Outfix.WebAppPortal.Controllers
{
    public class ProductController : Controller
    {
        WP_Common common = new WP_Common();
        private static List<ProductModel> products;

        public static List<ProductModel> Products
        {
            get => products;
            set => products = value;
        }

        // GET: Product
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult ProductsPage(SearchFilterModel search = null)
        {
            ViewBag.Hierarchy = common.setHierarchy(search);
            if (search != null)
            {
                ViewBag.SideFilters = search;
            }

            var path = ConfigurationManager.AppSettings["ServerPath"].ToString();

            List<ProductModel> products = new List<ProductModel>();

            if (string.IsNullOrEmpty(search.search_input))
            {
                Products = common.getAllProducts(search);
            }
            else
            {
                Products = common.getProductbySearch(search.search_input);
            }
            Products.ForEach(x =>
            {
                //x = common.getProductWithVariationById(x.ItemCode);

                //x.Cart.Pic = string.IsNullOrEmpty(x.Picture1) ? "" : Path.Combine(path, x.Picture1);

                x.Picture1 = string.IsNullOrEmpty(x.Picture1) ? "" : Path.Combine(path, x.Picture1);
                x.Picture2 = string.IsNullOrEmpty(x.Picture2) ? "" : Path.Combine(path, x.Picture2);
                x.Picture3 = string.IsNullOrEmpty(x.Picture3) ? "" : Path.Combine(path, x.Picture3);
                x.Picture4 = string.IsNullOrEmpty(x.Picture4) ? "" : Path.Combine(path, x.Picture4);
                x.Picture5 = string.IsNullOrEmpty(x.Picture5) ? "" : Path.Combine(path, x.Picture5);
                x.Variation.ForEach(y => y.Picture = string.IsNullOrEmpty(y.Picture) ? "" : Path.Combine(path, y.Picture));
            });

            return View(Products);
        }

        public ActionResult ShowFilters(SearchFilterModel search)
        {
            ViewBag.SideFilters = search;

            var Filters = common.getAllFilters();
            return View(Filters);
        }
        public ActionResult ProductsDetailsPage(string id)
        {
            try
            {
                if (id == null)
                {
                    return View("~/Views/Shared/Error.cshtml");
                }
                var path = ConfigurationManager.AppSettings["ServerPath"].ToString();

                ProductDetailViewModel model = new ProductDetailViewModel();
                model.Product = common.getProductWithVariationById(id);
                if (model.Product != null)
                {

                    model.Reviews = common.getProductReviews(id);

                    model.Product.Picture1 = string.IsNullOrEmpty(model.Product.Picture1) ? "" : Path.Combine(path, model.Product.Picture1);
                    model.Product.Picture2 = string.IsNullOrEmpty(model.Product.Picture2) ? "" : Path.Combine(path, model.Product.Picture2);
                    model.Product.Picture3 = string.IsNullOrEmpty(model.Product.Picture3) ? "" : Path.Combine(path, model.Product.Picture3);
                    model.Product.Picture4 = string.IsNullOrEmpty(model.Product.Picture4) ? "" : Path.Combine(path, model.Product.Picture4);
                    model.Product.Picture5 = string.IsNullOrEmpty(model.Product.Picture5) ? "" : Path.Combine(path, model.Product.Picture5);
                    model.Product.Variation.ForEach(y => y.Picture = string.IsNullOrEmpty(y.Picture) ? "" : Path.Combine(path, y.Picture));
                    //model.Product.Cart.Pic = string.IsNullOrEmpty(model.Product.Cart.Pic) ? "" : Path.Combine(path, model.Product.Cart.Pic);


                    var search = new SearchFilterModel
                    {
                        Ct = model.Product.MainCategory,
                        SCt = model.Product.SubCategory,
                        Bd = model.Product.Brand,
                    };
                    ViewBag.Hierarchy = common.setHierarchy(search);

                    if (Session["VariationValues"] != null)
                    {
                        Session.Remove("VariationValues");
                    }
                }

                return View(model);
            }
            catch (Exception ex)
            {
                return View("~/Views/Shared/Error.cshtml", ex);
            }
        }
        public ActionResult SimilarProducts(string ic)
        {
            var path = ConfigurationManager.AppSettings["ServerPath"].ToString();
            var model = common.getSimilarProducts(ic);
            model.ForEach(x =>
            {
                x.Picture1 = string.IsNullOrEmpty(x.Picture1) ? "" : Path.Combine(path, x.Picture1);
            });
            return View(model);
        }
        public ActionResult searchProduct(string str, string device = "web")
        {
            var names = common.getNamesbySearch(str);
            ViewBag.search = device;
            return View("SearchResults", names);
        }

        public ActionResult getDDLs(string ids, string itemcode, string field, string value)
        {
            List<ProductVariationModel> VariationValues;
            ProductVariationModel Variation;

            if (Session["VariationValues"] != null)
            {
                VariationValues = (List<ProductVariationModel>)Session["VariationValues"];
                Variation = VariationValues.FirstOrDefault(x => x.ProductId == itemcode);
                if (Variation == null)
                {
                    Variation = new ProductVariationModel();

                    Variation.ProductId = itemcode;
                    Variation.GetType().GetProperty(field).SetValue(Variation, value);
                    VariationValues.Add(Variation);
                }
                else
                {
                    Variation.GetType().GetProperty(field).SetValue(Variation, value);
                }
            }
            else
            {
                VariationValues = new List<ProductVariationModel>();
                Variation = new ProductVariationModel();

                Variation.ProductId = itemcode;
                Variation.GetType().GetProperty(field).SetValue(Variation, value);
                VariationValues.Add(Variation);

            }

            Session["VariationValues"] = VariationValues;
            var model = common.getVariationDropDowns(ids);

            var path = ConfigurationManager.AppSettings["ServerPath"].ToString();
            model.ForEach(x => x.Picture = string.IsNullOrEmpty(x.Picture) ? "" : Path.Combine(path, x.Picture));

            return View("ProductVariations", model);
        }

        public ActionResult ProductVariations(List<ProductVariationModel> p)
        {
            return View(p);
        }
        [HttpPost]
        public ActionResult ProductIFrame(string id)
        {
            ProductDetailViewModel model = new ProductDetailViewModel();
            model.Product = common.getProductWithVariationById(id);

            var path = ConfigurationManager.AppSettings["ServerPath"].ToString();
            //p.ForEach(y => y.Picture = string.IsNullOrEmpty(y.Picture) ? "" : Path.Combine(path, y.Picture));
            return View(model);
        }
        //public ActionResult clearDDLs(string itemcode, string field)
        //{
        //    List<ProductVariationModel> VariationValues;
        //    ProductVariationModel Variation;

        //    if (Session["VariationValues"] != null)
        //    {
        //        VariationValues = (List<ProductVariationModel>)Session["VariationValues"];
        //        Variation = VariationValues.FirstOrDefault(x => x.ProductId == itemcode);
        //        if (Variation == null)
        //        {
        //            Variation = new ProductVariationModel();
        //            Variation.ProductId = itemcode;
        //            Variation.GetType().GetProperty(field).SetValue(Variation, null);
        //            VariationValues.Add(Variation);
        //        }
        //        else
        //        {
        //            Variation.GetType().GetProperty(field).SetValue(Variation, null);
        //        }
        //    }
        //    else
        //    {
        //        VariationValues = new List<ProductVariationModel>();
        //        Variation = new ProductVariationModel();

        //        Variation.ProductId = itemcode;
        //        Variation.GetType().GetProperty(field).SetValue(Variation, null);
        //        VariationValues.Add(Variation);

        //    }

        //    Session["VariationValues"] = VariationValues;
        //    var model = common.getVariationDropDowns(ids);

        //    var path = ConfigurationManager.AppSettings["ServerPath"].ToString();
        //    model.ForEach(x => x.Picture = string.IsNullOrEmpty(x.Picture) ? "" : Path.Combine(path, x.Picture));

        //    return View("ProductVariations", model);
        //}
        public ActionResult clearAllDDLs(string Code)
        {
            if (Session["VariationValues"] != null)
            {
                Session.Remove("VariationValues");
            }

            var model = common.getProductWithVariationById(Code);
            var path = ConfigurationManager.AppSettings["ServerPath"].ToString();
            model.Variation.ForEach(x => x.Picture = string.IsNullOrEmpty(x.Picture) ? "" : Path.Combine(path, x.Picture));
            return View("ProductVariations", model.Variation);
        }
    }
}