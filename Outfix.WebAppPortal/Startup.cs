﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(Outfix.WebAppPortal.Startup))]
namespace Outfix.WebAppPortal
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
