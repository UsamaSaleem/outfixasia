﻿using Newtonsoft.Json;
using OutfixDB;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Outfix.Models
{
    public class BrandModel
    {
        public string Name { get; set; }
        public string Description { get; set; }
        public string MainCategoryName { get; set; }
        public string SubCategoryName { get; set; }


        public BrandModel()
        {

        }
        public BrandModel(Brand brand)
        {
            Name = brand.Name;
            Description = brand.Description;
            MainCategoryName = brand.CategoryName;
            SubCategoryName = brand.SubCategoryName;
        }
        public override string ToString()
        {
            return JsonConvert.SerializeObject(this);
        }
    }
}
