﻿using Newtonsoft.Json;
using OutfixDB;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace Outfix.Models
{
    public class CustomerModel
    {
        [DisplayName("ID")]
        public int ID { get; set; }

        [DisplayName("First Name")]
        [Required(ErrorMessage = "Please enter First Name.")]
        public string FirstName { get; set; }

        [DisplayName("Last Name")]
        [Required(ErrorMessage = "Please enter Last Name.")]
        public String LastName { get; set; }

        [DisplayName("Username")]
        public String Username { get; set; }

        [DisplayName("Email")]
        [Required(ErrorMessage = "Please enter email address.")]
        public String Email { get; set; }
        [DisplayName("Password")]
        public String Password { get; set; }
        public String PasswordKey { get; set; }

        public LocationModel Location {get;set;}

        //[DisplayName("Address")]
        //public String Address { get; set; }
        //[DisplayName("City")]
        //public string City { get; set; }

        //[DisplayName("Country")]
        //public String Country { get; set; }

        [DisplayName("Phone No")]
        [Required(ErrorMessage = "Please enter your contact number.")]
        public String Contact1 { get; set; }
        [DisplayName("Phone No")]
        public String Contact2 { get; set; }

        [DisplayName("Zip Code")]
        public String ZipCode { get; set; }

        [DisplayName("Type")]
        public String Type { get; set; }

        [DisplayName("Gender")]
        public String Gender { get; set; }
        [DisplayName("Status")]
        public String IsActive { get; set; }
        public String CustomerReference { get; set; }

        public WalletModel Wallet { get; set; }
        public List<OrderModel> Orders { get; set; }
        public CustomerModel()
        {
            Orders = new List<OrderModel>();
        }
        public CustomerModel(Customer customer)
        {
            Orders = new List<OrderModel>();

            ID = customer.ID;
            FirstName = customer.FirstName;
            LastName = customer.LastName;
            Username = customer.Username;
            Email = customer.Email;
            ZipCode = customer.Postal_code;
            Contact1 = customer.Phone1;
            Contact2 = customer.Phone2;
            Type = customer.Type;
            Gender = Convert.ToInt32(customer.Gender) == 1 ? "Men" : "Women";

            Location = new LocationModel();
            Location.CustomerId = customer.ID;
            Location.Address = customer.Address;
            Location.City = customer.CIty;
            Location.Country = customer.Country;

            
        }

        public void SetCustomerLocation(CustomerLocation customer)
        {
            Location = new LocationModel();
            Location.CustomerId = Convert.ToInt32(customer.CustomerId);
            Location.Address = customer.Address;
            Location.City = customer.City;
            Location.Country = customer.Country;
            Location.Latitude = customer.Latitude.ToString();
            Location.Longitude = customer.Longitude.ToString();
        }

        public override string ToString()
        {
            return JsonConvert.SerializeObject(this);
        }
    }
}
