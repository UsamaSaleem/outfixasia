﻿using OutfixDB;
using System;

namespace Outfix.Models
{
    public class OfferModel
    {
        public int Id { get; set; }
        public String Name { get; set; }
        public String Description { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public decimal DiscountValue { get; set; }
        public bool IsPercentage { get; set; }
        public decimal PercentageValue { get; set; }
        public decimal MaxDiscount { get; set; }
        public decimal MinPurchasePrice { get; set; }
        public bool IsActive { get; set; }

        public OfferModel()
        {

        }

        public OfferModel(Offer offer)
        {
            Id = offer.Id;
            Name = offer.Name;
            Description = offer.Description;
            StartDate = Convert.ToDateTime(offer.StartDate);
            EndDate = Convert.ToDateTime(offer.EndDate);
            DiscountValue = Convert.ToDecimal(offer.DiscountValue);
            IsPercentage = Convert.ToBoolean(offer.IsPercentage);
            PercentageValue = Convert.ToDecimal(offer.PercentageValue);
            MaxDiscount = Convert.ToDecimal(offer.MaxDiscount);
            MinPurchasePrice = Convert.ToDecimal(offer.MinPurchasingAmount);
            IsActive = Convert.ToBoolean(offer.IsActive);
        }
    }

}
