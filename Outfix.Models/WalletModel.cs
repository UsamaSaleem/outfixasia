﻿using Newtonsoft.Json;
using OutfixDB;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Outfix.Models
{
   public class WalletModel
    {
        [DisplayName("ID")]
        public int ID { get; set; }
        [DisplayName("UserID")]
        public int? UserID { get; set; }
        [DisplayName("Credit")]
        public decimal? Credit { get; set; }

        public WalletModel()
        {

        }
        public WalletModel(Wallet wallet)
        {
            ID = wallet.Id;
            UserID = wallet.UserId;
            Credit = wallet.Credit;

        }
        public override string ToString()
        {
            return JsonConvert.SerializeObject(this);
        }


    }
    public class WalletDetailsModel
    {
        [DisplayName("ID")]
        public int ID { get; set; }
        [DisplayName("WalletID")]
        public int? WalletID { get; set; }
        [DisplayName("DateUsed")]
        public DateTime? DateUsed { get; set; }
        [DisplayName("CodeUsed")]
        public string CodeUsed { get; set; }
        [DisplayName("Description")]
        public string Description { get; set; }
        [DisplayName("TotalCreditUsed")]
        public decimal? TotalCreditUsed { get; set; }
        [DisplayName("TotalCreditUsed")]
        public decimal? TotalCreditAdded { get; set; }
        [DisplayName("ExpiryDate")]
        public DateTime? ExpiryDate { get; set; }
        [DisplayName("IsActive")]
        public int? IsActive { get; set; }
        [DisplayName("WalletModel")]
        public WalletModel WalletModel { get; set; }

        public WalletDetailsModel()
        {

        }
        public WalletDetailsModel(WalletDetail walletDetail)
        {
            ID = walletDetail.Id;
            WalletID = walletDetail.WalletId;
            DateUsed = walletDetail.Date;
            CodeUsed = walletDetail.CodeUsed;
            TotalCreditUsed = walletDetail.TotalCreditUsed;
            TotalCreditAdded = walletDetail.TotalCreditAdded;
            ExpiryDate = walletDetail.ExpiryDate;
            IsActive = walletDetail.IsActive;
            Description = walletDetail.Description;


        }
        public override string ToString()
        {
            return JsonConvert.SerializeObject(this);
        }

    }
}
