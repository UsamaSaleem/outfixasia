﻿using Newtonsoft.Json;
using OutfixDB;
using System;
using System.ComponentModel;

namespace Outfix.Models
{
    public class ReviewModel
    {
        [DisplayName("ID")]
        public int ID { get; set; }
        [DisplayName("Product Code")]
        public String ProductId { get; set; }
        [DisplayName("Product Name")]
        public String ProductName { get; set; }

        [DisplayName("Customer Id")]
        public String CustomerId { get; set; }

        [DisplayName("Order Id")]
        public String OrderId { get; set; }

        [DisplayName("Customer Name")]
        public String CustomerName { get; set; }

        [DisplayName("Date")]
        public DateTime CreatedDate { get; set; }

        [DisplayName("Rating")]
        public int Rating { get; set; }

        [DisplayName("Description")]
        public String Description { get; set; }
        [DisplayName("Remarks")]
        public String Remarks { get; set; }
        [DisplayName("Status")]
        public String Status { get; set; }

        public ReviewModel()
        {

        }
        public ReviewModel(Review review)
        {
            ID = review.Id;
            Description = review.Description;
            ProductId = review.ProductId;
            Status = review.Status.ToString();
            Rating = Convert.ToInt32(review.Rating);
            OrderId = review.OrderId.ToString();
            CreatedDate = Convert.ToDateTime(review.Date);
            CustomerId = review.CustomerId.ToString();
        }

        public override string ToString()
        {
            return JsonConvert.SerializeObject(this);
        }
    }
}
