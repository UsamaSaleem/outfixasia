﻿using Newtonsoft.Json;
using OutfixDB;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Outfix.Models
{
    public class ProductVariationModel
    {
        public int ID { get; set; }
        public string ProductId { get; set; }
        [DisplayName("Size")]
        public String Size { get; set; }
        [DisplayName("Collar Size")]
        public String ColarSize { get; set; }
        [DisplayName("Fit Type")]
        public String FitType { get; set; }
        [DisplayName("Color")]
        public String Color { get; set; }
        public String Picture { get; set; }
        public decimal Price { get; set; }
        public int Qty { get; set; }

        public ProductVariationModel()
        {

        }
        public ProductVariationModel(ProductVariation p)
        {
            ID = p.Id;
            ProductId = p.ProductId;
            ColarSize = p.ColarSize;
            Color = p.Color;
            Picture = p.Picture;
            ColarSize = p.ColarSize;
            FitType = p.FitType;
            Price = Convert.ToDecimal(p.VPrice);
            Qty = Convert.ToInt32(p.Qty);
            Size = p.Size;
        }

        public override string ToString()
        {
            return JsonConvert.SerializeObject(this);
        }
    }
}
