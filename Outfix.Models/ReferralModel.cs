﻿using Newtonsoft.Json;
using OutfixDB;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Outfix.Models
{
    public class ReferralModel
    {
        [DisplayName("ID")]
        public int ID { get; set; }
        [DisplayName("Code")]
        public string Code { get; set; }
        [DisplayName("Name")]
        public string Name { get; set; }
        [DisplayName("Description")]
        public string Description { get; set; }
        [DisplayName("Wallet Points")]
        public int WalletPoints { get; set; }
        [DisplayName("Limit")]
        public int Limit { get; set; }
        [DisplayName("User")]
        public int UserId { get; set; }
        public ReferralModel()
        {

        }
        public ReferralModel(ReferalCode referal)
        {
            ID = referal.Id;
            Code = referal.Code;
            Description = referal.Description;
            Name = referal.Name;
            WalletPoints = Convert.ToInt32(referal.WalletPoints);
            Limit = Convert.ToInt32(referal.Limit);
            UserId = Convert.ToInt32(referal.UserId);

        }

        public override string ToString()
        {
            return JsonConvert.SerializeObject(this);
        }
    }
    public class ReferralDetailsModel
    {
        [DisplayName("ID")]
        public int ID { get; set; }
        [DisplayName("ReferralId")]
        public int ReferralId { get; set; }
        [DisplayName("Description")]
        public string Description { get; set; }
        [DisplayName("WalletPoints")]
        public int WalletPoints { get; set; }
        [DisplayName("OrderId")]
        public int OrderId { get; set; }
        [DisplayName("UserId")]
        public int UserId { get; set; }
        [DisplayName("Date")]
        public DateTime Date { get; set; }
        public bool SignUpCheck { get; set; }
        public ReferralDetailsModel()
        {

        }
        public ReferralDetailsModel(ReferalCodeDetail referalDet)
        {
            ID = referalDet.Id;
            ReferralId = Convert.ToInt32(referalDet.ReferalCodeId);
            Date = Convert.ToDateTime(referalDet.Date);
            OrderId = Convert.ToInt32(referalDet.OrderId);
            UserId = Convert.ToInt32(referalDet.UserId);
            Description = referalDet.Description;
            SignUpCheck = Convert.ToBoolean(referalDet.IsSignup);
        }

        public override string ToString()
        {
            return JsonConvert.SerializeObject(this);
        }
    }
}
