﻿using Newtonsoft.Json;

namespace Outfix.Models
{
    public class SearchFilterModel
    {
        public string Ct { get; set; }      //Main Category
        public string SCt { get; set; }     //Sub Category
        public string Bd { get; set; }      //Brand
        public string PMin { get; set; }    //Price min
        public string PMax { get; set; }    //Price Max
        public string Tp { get; set; }      //Type
        public string Rt { get; set; }      //Rating
        public string Cr { get; set; }      //Color
        public string Sz { get; set; }      //Size
        public string CSz { get; set; }     //Colar Size
        public string FtTp { get; set; }    //Fit Type
        public string Mt { get; set; }    //Material
        public string SB { get; set; }    //Sort By
        public string SO { get; set; }    //Sort Order
        public string search_input { get; set; }    //search input


        public override string ToString()
        {
            return JsonConvert.SerializeObject(this);
        }
    }
}
