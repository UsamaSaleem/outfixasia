﻿using Newtonsoft.Json;
using OutfixDB;

namespace Outfix.Models
{
    public class ProductTypeModel
    {
        public string Name { get; set; }
        public string Description { get; set; }
        public string Picture { get; set; }


        public ProductTypeModel()
        {

        }
        public ProductTypeModel(ProductType p)
        {
            Name = p.Name;
            Description = p.Description;
            Picture = p.Picture;
        }
        public override string ToString()
        {
            return JsonConvert.SerializeObject(this);
        }
    }

}
