﻿using OutfixDB;
using System;

namespace Outfix.Models
{
    public class OfferDetailsModel
    {
        public int Id { get; set; }
        public int OfferId { get; set; }
        public String ProductId { get; set; }
        public decimal DiscountedPrice { get; set; }
       

        public OfferDetailsModel()
        {

        }

        public OfferDetailsModel(OfferDetail offer)
        {
            Id = offer.Id;
            OfferId = Convert.ToInt32(offer.OfferId);
            ProductId = offer.ProductId;
            DiscountedPrice = Convert.ToDecimal(offer.DiscountedPrice);
        }

    }
}
