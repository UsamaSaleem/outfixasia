﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Outfix.Models
{
    public class ProfileModel
    {
        public CustomerModel Customer { get; set; }
        public string ReferCode { get; set; }
        public List<StyleProfileQuestionsModel> StyleProfile { get; set; }
        public StyleProfileSurveyModel UserAnswers { get; set; }

        public ProfileModel()
        {
            StyleProfile = new List<StyleProfileQuestionsModel>();
        }
    }
}
