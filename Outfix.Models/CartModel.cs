﻿using Newtonsoft.Json;

namespace Outfix.Models
{
    public class CartModel
    {
        public string Id { get; set; }
        public int Quantity { get; set; }
        public decimal Price { get; set; }
        public string Name { get; set; }
        public string Pic { get; set; }
        public string Vendor { get; set; }
        public ProductVariationModel Variation { get; set; }
        public CartModel()
        {

        }
        public CartModel(CartModel c)
        {
            Id = c.Id;
            Quantity = c.Quantity;
            Name = c.Name;
            Pic = c.Pic;
            Vendor = c.Vendor;
            Variation = c.Variation;
            Price = c.Price;
        }
        public override string ToString()
        {
            return JsonConvert.SerializeObject(this);
        }
    }
}
