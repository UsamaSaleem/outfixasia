﻿using Newtonsoft.Json;
using OutfixDB;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Outfix.Models
{
    public class MainCategoryModel
    {
        public string Name { get; set; }
        public string Description { get; set; }
        public List<string> Type { get; set; }
        public MainCategoryModel()
        {

        }
        public MainCategoryModel(Category cat)
        {
            Name = cat.Name;
            Description = cat.Description;
            Type = cat.Type.Split(',').ToList();
        }
        public override string ToString()
        {
            return JsonConvert.SerializeObject(this);
        }
    }
}
