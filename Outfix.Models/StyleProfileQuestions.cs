﻿using Newtonsoft.Json;
using OutfixDB;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Outfix.Models
{
    public class StyleProfileQuestionsModel
    {

        [DisplayName("QuestionID")]
        public int Id { get; set; }
        [DisplayName("Question")]
        public string Question { get; set; }
        [DisplayName("Placeholder")]
        public string Placeholder { get; set; }
        [DisplayName("AnswerType")]
        public string AnswerType { get; set; }

        
        public List<StyleProfileAnswersModel> Answers { get; set; }

        
        public StyleProfileQuestionsModel()
        {
            Answers = new List<StyleProfileAnswersModel>();
        }
        public StyleProfileQuestionsModel(StyleProfileQuestion db)
        {
            Id = db.Id;
            Question = db.Description;
            Placeholder = db.Placeholder;
            AnswerType = db.AnswerType;
            Answers = new List<StyleProfileAnswersModel>();

        }
        public override string ToString()
        {
            return JsonConvert.SerializeObject(this);
        }

    }
    public class StyleProfileAnswersModel
    {

        [DisplayName("Id")]
        public int Id { get; set; }

        [DisplayName("QuestionID")]
        public int QuestionId { get; set; }
        [DisplayName("Answer")]
        public string Answer { get; set; }
        [DisplayName("ImagePath")]
        public string ImagePath { get; set; }
        [DisplayName("AnswerType")]
        public string AnswerType { get; set; }

        public StyleProfileAnswersModel()
        {
        }
        public StyleProfileAnswersModel(StyleProfileAnswer db)
        {
            Id = db.Id;
            QuestionId = Convert.ToInt32(db.QuestionId);
            Answer = db.Answer;
            AnswerType = db.AnswerType;
            ImagePath = db.ImagePath;

        }
        public override string ToString()
        {
            return JsonConvert.SerializeObject(this);
        }
    }
    public class StyleProfileUserAnswersModel
    {
        [DisplayName("Id")]
        public int Id { get; set; }

        [DisplayName("CustomerID")]
        public int CustomerID { get; set; }
        [DisplayName("QuestionID")]
        public string QuestionID { get; set; }
        [DisplayName("Answer")]
        public string Answer { get; set; }
        [DisplayName("AnswerId")]
        public string AnswerId { get; set; }

        public StyleProfileUserAnswersModel()
        {
        }
        public StyleProfileUserAnswersModel(UserAnswerStyleProfile db)
        {
            Id = db.Id;
            CustomerID = Convert.ToInt32(db.CustomerId);
            QuestionID = db.QuestionId.ToString();
            Answer = db.Answer;

        }
        public override string ToString()
        {
            return JsonConvert.SerializeObject(this);
        }

    }
    public class StyleProfileSurveyModel
    {

        [DisplayName("SurveyUserAnswers")]
        public List<StyleProfileUserAnswersModel> survey { get; set; }
       
        [DisplayName("Customer")]
        public CustomerModel Customer { get; set; }
        [DisplayName("SurveyQuestions")]
        public List<StyleProfileQuestionsModel> surveyQuestions { get; set; }

     
        public StyleProfileSurveyModel()
        {
            surveyQuestions = new List<StyleProfileQuestionsModel>();
        }
        
        public override string ToString()
        {
            return JsonConvert.SerializeObject(this);
        }

    }



}
