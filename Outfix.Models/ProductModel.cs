﻿using Newtonsoft.Json;
using OutfixDB;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Web.Mvc;

namespace Outfix.Models
{
    public class ProductModel
    {
        public int ID { get; set; }

        [DisplayName("Item Code")]
        public String ItemCode { get; set; }

        public String MainCategory { get; set; }
        public String SubCategory { get; set; }
        public String Brand { get; set; }
        public String Name { get; set; }
        [AllowHtml]
        [DisplayName("Description")]
        public String Description { get; set; }

        [AllowHtml]
        [DisplayName("Return Guide")]
        public String ReturnPolicy { get; set; }
        
        public String Picture1 { get; set; }
        public String Picture2 { get; set; }
        public String Picture3 { get; set; }
        public String Picture4 { get; set; }
        public String Picture5 { get; set; }
        public int UnitSold { get; set; }
        public decimal Rating { get; set; }
        public int IsActive { get; set; }

        public String ProductType { get; set; }
        public String Type { get; set; }
        public String Status { get; set; }

        [DisplayName("Quantity")]
        public int Qty { get; set; }
        public decimal Price { get; set; }
        public decimal DiscountedPrice { get; set; }
        public int Discount { get; set; }

        [DisplayName("Unit Measurement")]
        public String UnitMeasurement { get; set; }

        public String Remarks { get; set; }
        public String VendorId { get; set; }

        public List<ProductVariationModel> Variation { get; set; }
        public String VarStr { get; set; }

        public ProductModel()
        {
            Variation = new List<ProductVariationModel>();
        }
        public ProductModel(Product p)
        {
            ID = p.ID;
            Name = p.Name;
            Type = p.Type;
            ProductType = p.ProductType;
            Description = p.Description;
            ReturnPolicy = p.ReturnPolicy;
            Remarks = p.Remarks;
            Brand = p.Brand;
            MainCategory = p.MainCategory;
            SubCategory = p.SubCategory;
            Price = p.Price != null ? p.Price.Value : 0;
            DiscountedPrice = p.DiscountedPrice != null ? p.DiscountedPrice.Value : 0;
            Rating = p.Rating != null ? p.Rating.Value : 0;
            Picture1 = p.Picture1;
            Picture2 = p.Picture2;
            Picture3 = p.Picture3;
            Picture4 = p.Picture4;
            Picture5 = p.Picture5;
            UnitSold = p.UnitSold != null ? p.UnitSold.Value : 0;
            ItemCode = p.ItemCode;
            IsActive = p.IsActive != null ? p.IsActive.Value : 0;
            VendorId = p.VendorId;

            Variation = new List<ProductVariationModel>();
        }
        public override string ToString()
        {
            return JsonConvert.SerializeObject(this);
        }
    }



}
