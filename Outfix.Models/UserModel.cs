﻿using Newtonsoft.Json;
using OutfixDB;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Outfix.Models
{
    public class UserModel
    {
        [DisplayName("ID")]
        public int ID { get; set; }

        [DisplayName("Name")]
        public String Name { get; set; }

        [DisplayName("Username")]
        public String Username { get; set; }
        [DisplayName("Email")]
        public String Email { get; set; }
        [DisplayName("Password")]
        public String Password { get; set; }
        public String PasswordKey { get; set; }
        [DisplayName("Contact")]
        public String Contact { get; set; }
        [DisplayName("Status")]
        public String Status { get; set; }
        [DisplayName("LoginStatus")]
        public String LoginStatus { get; set; }

        [DisplayName("Type")]
        public String Type { get; set; }

        [DisplayName("Description")]
        public String Description { get; set; }

        public UserModel()
        {
        }
        public UserModel(User user)
        {
            ID = user.ID;
            Name = user.Name;
            Username = user.Username;
            Description = user.Description;
            Type = user.UserType;
            Email = user.Email;
            Contact = user.Contact;
            Status = user.Status;
            LoginStatus = user.LoginStatus;
        }
        public override string ToString()
        {
            return JsonConvert.SerializeObject(this);
        }
    }
}
