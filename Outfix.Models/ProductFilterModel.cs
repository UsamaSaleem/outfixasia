﻿using OutfixDB;
using System.Collections.Generic;

namespace Outfix.Models
{
    public class ProductFilterModel
    {
        public int ID { get; set; }
        public string Name { get; set; }
        public string Alias { get; set; }
        public string FullName { get; set; }
        public List<string> Values { get; set; }

        public ProductFilterModel()
        {
            Values = new List<string>();
        }
        public ProductFilterModel(ProductFilter filter)
        {
            ID = filter.ID;
            Name = filter.Name;
            Alias = filter.Alias;
            FullName = filter.FullName;
            Values = new List<string>();
        }
    }
}
