﻿using Newtonsoft.Json;
using OutfixDB;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Outfix.Models
{
    public class SubCategoryModel
    {
        public string Name { get; set; }
        public string Description { get; set; }
        public string MainCategoryName { get; set; }
        public List<string> Type { get; set; }

        public SubCategoryModel()
        {

        }
        public SubCategoryModel(SubCategory cat)
        {
            Name = cat.Name;
            Description = cat.Description;
            MainCategoryName = cat.CategoryName;
            Type = cat.Type.Split(',').ToList();
        }
        public override string ToString()
        {
            return JsonConvert.SerializeObject(this);
        }
    }
}
