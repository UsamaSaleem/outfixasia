﻿using OutfixDB;
using System;
using System.Collections.Generic;

namespace Outfix.Models
{
    public class OrderModel
    {
        public int ID { get; set; }
        public int CustomerId { get; set; }
        public string CustomerName { get; set; }
        public String CreatedDate { get; set; }
        public int TotalItems { get; set; }
        public decimal NetPayable { get; set; }
        public string VoucherCode { get; set; }
        public decimal Discount { get; set; }
        public string Status { get; set; }
        public List<ProductModel> Products { get; set; }

        public OrderModel()
        {
            Products = new List<ProductModel>();
        }
        public OrderModel(OrderCart order)
        {
            ID = order.ID;
            CustomerId = order.CustomerId;
            CreatedDate = order.CreatedDate.ToString();
            TotalItems = Convert.ToInt32(order.TotalItems);
            NetPayable = Convert.ToInt32(order.NetPayable);
            VoucherCode = order.VoucherCode;
            Discount = Convert.ToInt32(order.Discount);
            Status = order.Status;
            Products = new List<ProductModel>();
        }







    }
}
