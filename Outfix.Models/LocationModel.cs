﻿using OutfixDB;
using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace Outfix.Models
{
    public class LocationModel
    {
        public int Id { get; set; }
        public int CustomerId { get; set; }
        [DisplayName("Address")]
        public string Address { get; set; }
        [DisplayName("City")]
        [Required(ErrorMessage = "Please enter your city.")]
        public string City { get; set; }
        [DisplayName("Country")]
        [Required(ErrorMessage = "Please enter your country.")]
        public string Country { get; set; }
        public string Latitude { get; set; }
        public string Longitude { get; set; }

        public LocationModel()
        {

        }
        public LocationModel(CustomerLocation loc)
        {
            Id = loc.Id;
            CustomerId = Convert.ToInt32(loc.CustomerId);
            Address = loc.Address;
            City = loc.City;
            Country = loc.Country;
            Latitude = loc.Latitude.ToString();
            Longitude = loc.Longitude.ToString();
        }
    }
}
