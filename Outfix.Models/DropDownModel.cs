﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Outfix.Models
{
    public class DropDownModel
    {
        public String ID { get; set; }
        public String Description { get; set; }
    }
}
