﻿using Outfix.Models;
using OutfixDB;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;

namespace OutfixAsiaAPI.Controllers
{
    public class ReviewController : ApiController
    {
        OutfixEntities db;

        public ReviewController()
        {
            db = new OutfixEntities();
        }
        #region Get Reviews

        // GET: api/Review
        public List<ReviewModel> GetReviewList()
        {
            var reviews = db.Reviews.ToList();
            if (reviews == null)
            {
                return new List<ReviewModel>();
            }
            var model = new List<ReviewModel>();
            reviews.ForEach(r =>
            {
                var review = new ReviewModel(r);
                var customer = db.Customers.SingleOrDefault(c => c.ID == r.CustomerId);
                review.CustomerId = customer.ID.ToString();
                review.CustomerName = customer.FirstName + customer.LastName;

                var product = db.Products.SingleOrDefault(p => p.ID.ToString() == r.ProductId);
                review.ProductName = product.Name;

                model.Add(review);
            });
            return model;
        }
        public List<ReviewModel> GetReviewList(string state)
        {
            List<Review> reviews = null;

            reviews = db.Reviews.Where(x => x.Status == state.ToLower()).ToList();

            if (reviews == null)
            {
                return new List<ReviewModel>();
            }
            var model = new List<ReviewModel>();
            reviews.ForEach(r =>
            {
                var review = new ReviewModel(r);
                var customer = db.Customers.SingleOrDefault(c => c.ID == r.CustomerId);
                review.CustomerId = customer.ID.ToString();
                review.CustomerName = customer.FirstName + customer.LastName;

                var product = db.Products.SingleOrDefault(p => p.ID.ToString() == r.ProductId);
                review.ProductName = product.Name;

                model.Add(review);
            });
            return model;
        }
        // GET: api/Review/5
        public ReviewModel GetReviewById(int id)
        {
            var review = db.Reviews.FirstOrDefault(x => x.Id == id);
            if (review == null)
            {
                return null;
            }
            var model = new ReviewModel(review);
            var customer = db.Customers.SingleOrDefault(c => c.ID == review.CustomerId);
            model.CustomerId = customer.ID.ToString();
            model.CustomerName = customer.FirstName + customer.LastName;

            var product = db.Products.SingleOrDefault(p => p.ID.ToString() == review.ProductId);
            model.ProductName = product.Name;

            return model;
        }

        public List<ReviewModel> GetReviewByProductId(string id)
        {
            var reviews = db.Reviews.Where(x => x.ProductId == id).ToList();
            if (reviews == null)
            {
                return null;
            }
            var model = new List<ReviewModel>();

            foreach (var x in reviews)
            {
                var m = new ReviewModel(x);
                var customer = db.Customers.FirstOrDefault(c => c.ID == x.CustomerId);
                if (customer != null)
                {
                    m.CustomerId = customer.ID.ToString();
                    m.CustomerName = customer.FirstName + customer.LastName;
                }

                var product = db.Products.FirstOrDefault(p => p.ItemCode == x.ProductId);
                if (product != null)
                {
                    m.ProductName = product.Name;
                }

                model.Add(m);
            }

            return model;
        }
        [HttpPut]
        public bool UpdateReview(ReviewModel model)
        {
            try
            {
                var review = db.Reviews.Single(x => x.Id == model.ID);
                review.Status = model.Status;
                db.SaveChanges();
                model = new ReviewModel(review);
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }
        #endregion
    }
}
