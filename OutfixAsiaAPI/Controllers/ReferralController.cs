﻿using Outfix.Models;
using OutfixDB;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;

namespace OutfixAsiaAPI.Controllers
{
    public class ReferralController : ApiController
    {
        OutfixEntities db;

        public ReferralController()
        {
            db = new OutfixEntities();
        }
        #region Get Referral
        public List<ReferralModel> GetReferralList()
        {
            var RefCode = db.ReferalCodes.ToList();
            if (RefCode == null || RefCode.Count <= 0)
            {
                return new List<ReferralModel>();
            }
            var model = new List<ReferralModel>();
            RefCode.ForEach(r => model.Add(new ReferralModel(r)));
            return model;
        }
        public ReferralModel GetReferralById(int Id)
        {
            var RefCode = db.ReferalCodes.SingleOrDefault(x => x.Id == Id);
            var model = new ReferralModel(RefCode);
            return model;
        }
        public List<ReferralModel> GetReferralByUserId(int UserId)
        {
            var RefCode = db.ReferalCodes.Where(x => x.UserId == UserId).ToList();
            if (RefCode == null)
            {
                return new List<ReferralModel>();
            }
            var model = new List<ReferralModel>();
            RefCode.ForEach(r => model.Add(new ReferralModel(r)));
            return model;
        }
        public ReferralModel GetUserIdByReferralCode(string Code)
        {
            var RefCode = db.ReferalCodes.SingleOrDefault(x => x.Code == Code);
            if (RefCode == null)
            {
                return null;
            }
            //var model = new ReferralModel();
            var model = new ReferralModel(RefCode);
            //RefCode.ForEach(r => model.Add(new ReferralModel(r)));
            return model;
        }
        public List<ReferralDetailsModel> GetReferralDetailsList(string ReferralId)
        {
            var RefCodeDet = db.ReferalCodeDetails.Where(x => x.ReferalCodeId.ToString() == ReferralId).ToList();
            if (RefCodeDet == null || RefCodeDet.Count <= 0)
            {
                return new List<ReferralDetailsModel>();
            }
            var model = new List<ReferralDetailsModel>();
            RefCodeDet.ForEach(r => model.Add(new ReferralDetailsModel(r)));
            return model;
        }
        public List<ReferralDetailsModel> GetReferralDetailsByUser(string UserId)
        {
            var RefCodeDel = db.ReferalCodeDetails.Where(x => x.UserId.ToString() == UserId).ToList();
            if (RefCodeDel == null || RefCodeDel.Count <= 0)
            {
                return new List<ReferralDetailsModel>();
            }
            var model = new List<ReferralDetailsModel>();
            RefCodeDel.ForEach(r => model.Add(new ReferralDetailsModel(r)));
            return model;
        }
        public List<ReferralDetailsModel> GetReferralDetailsByDate(DateTime Date)
        {
            var RefCodeDel = db.ReferalCodeDetails.Where(x => x.Date == Date).ToList();
            if (RefCodeDel == null || RefCodeDel.Count <= 0)
            {
                return new List<ReferralDetailsModel>();
            }
            var model = new List<ReferralDetailsModel>();
            RefCodeDel.ForEach(r => model.Add(new ReferralDetailsModel(r)));
            return model;
        }
        public ReferralDetailsModel GetReferralDetailsByOrder(int OrderId)
        {
            var RefCodeDel = db.ReferalCodeDetails.SingleOrDefault(x => x.OrderId == OrderId);
            var model = new ReferralDetailsModel(RefCodeDel);
            return model;
        }
        //public List<ReferralDetailsModel> GetReferralDetailsByState(int state)
        //{
        //    var RefCodeDel = db.ReferalCodeDetails.Where(x => x.IsActive == state).ToList();
        //    if(RefCodeDel == null || RefCodeDel.count <= 0)
        //    {
        //        return new List<ReferralDetailsModel>();
        //    }
        //    var model = new List<ReferralDetailsModel>();
        //    RefCodeDel.ForEach(r => model.Add(new ReferralDetailsModel(r)));
        //    return model;
        //}
        #endregion

        #region Create Referral
        [HttpPost]
        public IHttpActionResult CreateReferralForUser(ReferralModel model)
        {
            try
            {
                // var p = JsonConvert.DeserializeObject<ProductModel>(json);
                var referral = new ReferalCode
                {
                    Code = model.Code,
                    Name = model.Name,
                    Description = model.Description,
                    Limit = model.Limit,
                    WalletPoints = model.WalletPoints,
                    UserId = model.UserId,
                    //IsActive = 1,
                };
                db.ReferalCodes.Add(referral);
                db.SaveChanges();
                model.ID = referral.Id;
                return Created(new Uri(Request.RequestUri + "/" + referral.Id), model);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
        #endregion
        [HttpGet]
        public ReferralModel ValidateCoupon(string cpn)
        {
            var coupon = db.ReferalCodes.FirstOrDefault(x => x.Code == cpn && x.IsSignUp == false);
            ReferralModel model = null;
            if (coupon != null)
            {
                model = new ReferralModel(coupon);
            }
            return model;
        }
        [HttpGet]
        public bool ValidateReferral(string rfrl)
        {
            var referral = db.ReferalCodes.FirstOrDefault(x => x.Code == rfrl && x.IsSignUp == true);
            if (referral != null)
            {
                return true;
            }
            return false;
        }
    }

}
