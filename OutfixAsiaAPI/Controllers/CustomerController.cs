﻿using Outfix.Commons;
using Outfix.Models;
using Outfix.ViewModels;
using OutfixDB;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Http;

namespace OutfixAsiaAPI.Controllers
{
    public class CustomerController : ApiController
    {
        OutfixEntities db;

        public CustomerController()
        {
            db = new OutfixEntities();
        }
        #region Get Customer

        // GET: api/Customer
        public List<CustomerModel> GetCustomerList()
        {
            var customers = db.Customers.ToList();
            if (customers == null)
            {
                return new List<CustomerModel>();
            }
            var model = new List<CustomerModel>();
            customers.ForEach(p => model.Add(new CustomerModel(p)));
            return model;
        }

        // GET: api/Customer/5
        public CustomerModel GetCustomerById(int id)
        {
            var customer = db.Customers.FirstOrDefault(x => x.ID == id);
            if (customer == null)
            {
                return null;
            }
            var model = new CustomerModel(customer);
            var loc = db.CustomerLocations.Where(x => x.CustomerId == customer.ID).OrderByDescending(x => x.Id).FirstOrDefault();
            model.SetCustomerLocation(loc);
            return model;
        }
        public CustomerModel GetCustomerByUsername(string username)
        {
            var customer = db.Customers.FirstOrDefault(x => x.Username == username);
            if (customer == null)
            {
                return null;
            }
            var model = new CustomerModel(customer);
            return model;
        }
        public CustomerModel GetCustomerByEmail(string email)
        {
            var customer = db.Customers.FirstOrDefault(x => x.Email == email);
            if (customer == null)
            {
                return null;
            }
            var model = new CustomerModel(customer);
            var wallet = db.Wallets.FirstOrDefault(x => x.UserId == customer.ID);
            model.Wallet = new WalletModel(wallet);

            return model;
        }
        [HttpPost]
        public CustomerModel CheckLogin(LoginViewModel model)
        {
            var security = new SecurityCommons();
            CustomerModel c = null;

            var customer = db.Customers.FirstOrDefault(x => x.Email == model.Email);
            if (customer != null)
            {
                var hashedPassword = security.HashPasswordWithSalt(Encoding.UTF8.GetBytes(model.Password), Convert.FromBase64String(customer.PasswordKey));
                if (Convert.ToBase64String(hashedPassword) == customer.Password)
                {
                    c = new CustomerModel(customer);
                    var wallet = db.Wallets.FirstOrDefault(x => x.UserId == c.ID);
                    c.Wallet = new WalletModel(wallet);
                }
            }
            return c;
        }

        public List<OrderModel> GetCustomerOrders(int id)
        {
            var orders = db.OrderCarts.Where(x => x.CustomerId == id).OrderByDescending(x => x.CreatedDate).ToList();
            if (orders == null || orders.Count <= 0)
            {
                return new List<OrderModel>();
            }
            var model = new List<OrderModel>();
            foreach (var order in orders)
            {
                var orderDetail = db.OrderCartDetails.Where(x => x.OrderId == order.ID).ToList();
                var m = new OrderModel(order);
                orderDetail.ForEach(x =>
                {
                    var pvar = db.ProductVariations.FirstOrDefault(y => y.Id == x.ProductId);
                    pvar.Qty = x.Qty;
                    pvar.VPrice = x.Amount;

                    var pItem = db.Products.FirstOrDefault(y => y.ItemCode == pvar.ProductId);
                    var pmodel = new ProductModel(pItem);
                    pmodel.Status = x.Status;
                    pmodel.Variation.Add(new ProductVariationModel(pvar));
                    m.Products.Add(pmodel);
                });
                model.Add(m);
            }

            return model;
        }

        public WalletModel GetCustomerWallet(int id)
        {
            var wallet = db.Wallets.SingleOrDefault(x => x.UserId == id);
            if (wallet == null)
            {
                return null;
            }
            var model = new WalletModel(wallet);
            return model;
        }

        public String GetReferCode(int id)
        {
            var code = db.ReferalCodes.SingleOrDefault(x => x.UserId == id).Code;
            if (code == null)
            {
                return null;
            }
            return code;
        }

        #endregion

        #region Create Customer
        [HttpPost]
        public IHttpActionResult CreateCustomer(CustomerModel c)
        {
            try
            {
                // var p = JsonConvert.DeserializeObject<ProductModel>(json);
                var customer = new Customer
                {
                    Email = c.Email,
                    FirstName = c.FirstName,
                    LastName = c.LastName,
                    Username = c.Username,
                    Password = c.Password,
                    PasswordKey = c.PasswordKey
                };
                db.Customers.Add(customer);
                db.SaveChanges();
                c.ID = customer.ID;
                return Created(new Uri(Request.RequestUri + "/" + customer.ID), c);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
        #endregion

        public string AddNewLocation(CustomerModel customer)
        {
            bool customerDetailsChange = false;
            var cust = db.Customers.FirstOrDefault(x => x.Email == customer.Email);
            if (cust != null)
            {
                if(cust.FirstName == null && customer.FirstName != null)
                {
                    cust.FirstName = customer.FirstName;
                    customerDetailsChange = true;
                }
                else if (!cust.FirstName.Equals(customer.FirstName))
                {
                    cust.FirstName = customer.FirstName;
                    customerDetailsChange = true;
                }
                if (cust.LastName == null && customer.LastName != null)
                {
                    cust.LastName = customer.LastName;
                    customerDetailsChange = true;
                }
                else if (!cust.LastName.Equals(customer.LastName))
                {
                    cust.LastName = customer.LastName;
                    customerDetailsChange = true;
                }
                if (cust.Phone1 == null && customer.Contact1 != null)
                {
                    cust.Phone1 = customer.Contact1;
                    customerDetailsChange = true;
                }
                else if (!cust.Phone1.Equals(customer.Contact1))
                {
                    cust.Phone1 = customer.Contact1;
                    customerDetailsChange = true;
                }
                if (cust.Phone2 == null && customer.Contact2 != null)
                {
                    cust.Phone2 = customer.Contact2;
                    customerDetailsChange = true;
                }
                else if (!cust.Phone2.Equals(customer.Contact2))
                {
                    cust.Phone2 = customer.Contact2;
                    customerDetailsChange = true;
                }
                if (customerDetailsChange)
                {
                    db.Entry(cust).State = System.Data.Entity.EntityState.Modified;
                }
                var loc = db.CustomerLocations.FirstOrDefault(x => x.CustomerId == cust.ID && x.Address.Equals(customer.Location.Address));
                if(loc == null)
                {
                    db.CustomerLocations.Add(new CustomerLocation
                    {
                        CustomerId = cust.ID,
                        Address = customer.Location.Address,
                        City = customer.Location.City,
                        Country = customer.Location.Country,
                        Latitude = Convert.ToDecimal(customer.Location.Latitude),
                        Longitude = Convert.ToDecimal(customer.Location.Longitude),
                    });
                    db.SaveChanges();
                }
            }
            return "OK";
        }

        public List<LocationModel> getAllAddresses(CustomerModel customer)
        {
            var ml = new List<LocationModel>();
            var cust = db.Customers.FirstOrDefault(x => x.Email == customer.Email);
            if (cust != null)
            {
                var locs = db.CustomerLocations.Where(x => x.CustomerId == cust.ID).ToList();
                if (locs != null && locs.Count > 0)
                {
                    locs.ForEach(x => ml.Add(new LocationModel(x)));
                }
            }
            return ml;
        }

    }
}
