﻿using Outfix.Commons;
using Outfix.Models;
using Outfix.ViewModels;
using OutfixDB;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Web.Http;

namespace OutfixAsiaAPI.Controllers
{
    public class UserController : ApiController
    {
        OutfixEntities db;

        public UserController()
        {
            db = new OutfixEntities();
        }
        #region Get Users

        // GET: api/User
        public List<UserModel> GetUserList()
        {
            var users = db.Users.ToList();
            if (users == null)
            {
                return new List<UserModel>();
            }
            var model = new List<UserModel>();
            users.ForEach(p => model.Add(new UserModel(p)));
            return model;
        }

        // GET: api/User/5
        public UserModel GetUserById(int id)
        {
            var user = db.Users.FirstOrDefault(x => x.ID == id);
            if (user == null)
            {
                return null;
            }
            var model = new UserModel(user);
            return model;
        }
        #endregion

        public UserModel CheckLogin(LoginViewModel model)
        {
            var security = new SecurityCommons();
            UserModel c = null;

            var user = db.Users.FirstOrDefault(x => x.Email == model.Email);
            if (user != null)
            {
                var hashedPassword = security.HashPasswordWithSalt(Encoding.UTF8.GetBytes(model.Password), Convert.FromBase64String(user.PasswordKey));
                if (Convert.ToBase64String(hashedPassword) == user.Password)
                {
                    c = new UserModel(user);
                }
            }
            return c;
        }

        #region Create User
        [HttpPost]
        public IHttpActionResult CreateUser(UserModel u)
        {
            try
            {
                // var p = JsonConvert.DeserializeObject<ProductModel>(json);
                var usr = new User
                {
                    Email = u.Email,
                    Name = u.Name,
                    Username = u.Username,
                    Password = u.Password,
                    PasswordKey = u.PasswordKey
                };
                db.Users.Add(usr);
                db.SaveChanges();
                u.ID = usr.ID;
                return Created(new Uri(Request.RequestUri + "/" + usr.ID), u);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
        #endregion
    }
}
