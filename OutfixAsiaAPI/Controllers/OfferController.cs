﻿using Outfix.Models;
using OutfixDB;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;

namespace OutfixAsiaAPI.Controllers
{
    public class OfferController : ApiController
    {
        OutfixEntities db;

        #region Constructor / Destructor
        public OfferController()
        {
            db = new OutfixEntities();
        }
        #endregion

        public List<OfferModel> GetOfferList()
        {
            var dt = DateTime.Now;
            var offers = db.Offers.Where(x => x.IsActive == true).ToList();
            if (offers == null)
            {
                return new List<OfferModel>();
            }
            var model = new List<OfferModel>();
            offers.ForEach(p =>
            {
                model.Add(new OfferModel(p));
            });
            return model;
        }

        public OfferDetailsModel GetOfferDetails(int id)
        {
            var dt = DateTime.Now;
            var offer = db.OfferDetails.FirstOrDefault(x => x.OfferId == id);
            if (offer == null)
            {
                return null;
            }
            var model = new OfferDetailsModel(offer);

            return model;
        }

        public OfferDetailsModel GetOfferDetailsForProduct(string id)
        {
            var dt = DateTime.Now;
            var str = (from o in db.Offers
                       join d in db.OfferDetails on o.Id equals d.OfferId
                       where d.ProductId == id && o.IsActive == true
                       select d).ToList();

            if (str.Count > 0)
            {
                return new OfferDetailsModel(str.FirstOrDefault());
            }
            return null;
        }
    }
}
