﻿using Outfix.Models;
using OutfixDB;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;

namespace OutfixAsiaAPI.Controllers
{
    public class ProfileController : ApiController
    {
        OutfixEntities db;

        public ProfileController()
        {
            db = new OutfixEntities();
        }
        #region Get Questions
        [HttpGet]
        public List<StyleProfileQuestionsModel> GetAllQuestionAnswers()
        {
            var QA = db.StyleProfileQuestions.ToList();
            if (QA == null || QA.Count <= 0)
            {
                return new List<StyleProfileQuestionsModel>();
            }

            var model = new List<StyleProfileQuestionsModel>();
            QA.ForEach(r =>
            {
                var question = new StyleProfileQuestionsModel(r);
                var answer = db.StyleProfileAnswers.Where(x => x.QuestionId == r.Id).ToList();
                answer.ForEach(y => question.Answers.Add(new StyleProfileAnswersModel(y)));

                model.Add(question);

            });
            return model;

        }

        [HttpGet]
        public StyleProfileSurveyModel GetAllQuestionAnswersByUser(string CustId)
        {
            StyleProfileSurveyModel List = new StyleProfileSurveyModel();

            var Question = db.StyleProfileQuestions.ToList();

            for (int i = 0; i < Question.Count; i++)
            {
                StyleProfileSurveyModel survey_qa = new StyleProfileSurveyModel();
                List.surveyQuestions.Add(new StyleProfileQuestionsModel
                {
                    Id = Question[i].Id,
                    Question = Question[i].Description,
                    AnswerType = Question[i].AnswerType,
                    Placeholder = Question[i].Placeholder
                });
            }

            var survey = new StyleProfileSurveyModel();
            var QA = db.UserAnswerStyleProfiles.ToList();
            if (QA == null || QA.Count <= 0)
            {
                return List;
            }

            var model = new List<StyleProfileSurveyModel>();
            QA.ForEach(r =>
            {
                var srv = new StyleProfileUserAnswersModel();
                var useranswer = db.UserAnswerStyleProfiles.Where(x => x.CustomerId == srv.CustomerID).ToList();
                useranswer.ForEach(y => List.survey.Add(new StyleProfileUserAnswersModel(y)));
                // survey.survey.Add(srv);
            });

            return List;

        }


        #endregion
        #region Create Survey
        [HttpPost]
        public IHttpActionResult CreateSurveyForUser(StyleProfileSurveyModel model)
        {
            try
            {
                model.survey.ForEach(i =>
                {
                    var surveyResult = new UserAnswerStyleProfile
                    {
                        CustomerId = Convert.ToInt32(model.Customer.ID),
                        QuestionId = !String.IsNullOrEmpty(i.QuestionID) ? Convert.ToInt32(i.QuestionID) : 0,
                        AnswerId = !String.IsNullOrEmpty(i.AnswerId) ? Convert.ToInt32(i.AnswerId) : 0,
                        Answer = i.Answer != null ? i.Answer : "",


                    };
                    db.UserAnswerStyleProfiles.Add(surveyResult);

                });
                db.SaveChanges();


                return Created(new Uri(Request.RequestUri + "/"), "Customer Not Found");
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }

        }
        #endregion
    }
}
