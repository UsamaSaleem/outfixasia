﻿using Newtonsoft.Json;
using Outfix.Models;
using Outfix.ViewModels;
using OutfixDB;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Web.Http;

namespace OutfixAsiaAPI.Controllers
{
    public class OrderController : ApiController
    {
        OutfixEntities db;

        public OrderController()
        {
            db = new OutfixEntities();
        }
        public List<OrderModel> GetOrdersList()
        {
            var orders = db.OrderCarts.ToList();
            if (orders == null)
            {
                return new List<OrderModel>();
            }
            var model = new List<OrderModel>();
            orders.ForEach(or =>
            {
                var order = new OrderModel(or);
                var customer = db.Customers.SingleOrDefault(c => c.ID == or.CustomerId);
                order.CustomerId = customer.ID;
                order.CustomerName = customer.FirstName + customer.LastName;

                model.Add(order);
            });
            return model;
        }
        public List<OrderModel> GetOrderList(string state)
        {
            var orders = db.OrderCarts.Where(o => o.Status == state.ToLower()).ToList();
            if (orders == null)
            {
                return new List<OrderModel>();
            }
            var model = new List<OrderModel>();
            orders.ForEach(or =>
            {
                var order = new OrderModel(or);
                var customer = db.Customers.SingleOrDefault(c => c.ID == or.CustomerId);
                order.CustomerId = customer.ID;
                order.CustomerName = customer.FirstName + customer.LastName;

                model.Add(order);
            });
            return model;
        }
        // GET: api/Review/5
        public OrderModel GetOrderById(int id)
        {
            var order = db.OrderCarts.FirstOrDefault(x => x.ID == id);
            if (order == null)
            {
                return null;
            }
            var model = new OrderModel(order);
            var customer = db.Customers.FirstOrDefault(c => c.ID == order.CustomerId);
            model.CustomerId = customer.ID;
            model.CustomerName = customer.FirstName + customer.LastName;

            var products = db.OrderCartDetails.Where(x => x.OrderId == id).ToList();

            products.ForEach(x =>
            {
                var variation = db.ProductVariations.FirstOrDefault(p => p.Id == x.ProductId);
                var product = db.Products.FirstOrDefault(p => p.ItemCode == variation.ProductId);

                variation.Qty = x.Qty;
                variation.VPrice = x.Amount;
                ProductModel pr = new ProductModel(product);
                pr.Variation = new List<ProductVariationModel>();
                pr.Variation.Add(new ProductVariationModel(variation));
                model.Products.Add(pr);
            });

            return model;
        }
        [HttpPut]
        public List<OrderModel> ChangeOrderStatusInBulk(Message msg)
        {
            var listId = msg.ids;
            var ListModel = new List<OrderModel>();

            foreach (var id in listId)
            {
                var intID = Convert.ToInt32(id);
                var order = db.OrderCarts.FirstOrDefault(x => x.ID == intID);
                if (order == null)
                {
                    return null;
                }
                order.Status = msg.status;
                var model = new OrderModel(order);
                sendEmail(model);
                ListModel.Add(model);
            }
            db.SaveChanges();
            return ListModel;
        }

        [HttpPost]
        public IHttpActionResult PlaceCustomerOrder(CartViewModel model)
        {
            try
            {
                var customer = db.Customers.FirstOrDefault(x => x.ID == model.Customer.ID);

                if (customer != null)
                {
                    var order = db.OrderCarts.Add(new OrderCart
                    {
                        CreatedDate = DateTime.Now,
                        CustomerId = customer.ID,
                        NetPayable = model.TotalAmount,
                        Status = "pending",
                        TotalItems = model.TotalItems,
                        VoucherCode = model.Voucher != null ? model.Voucher.Code : null,
                        Discount = model.Discount
                    });
                    db.SaveChanges();

                    model.Product.ForEach(x =>
                    {
                        var p = x.Variation.FirstOrDefault();
                        db.OrderCartDetails.Add(new OrderCartDetail
                        {
                            OrderId = order.ID,
                            ProductId = p.ID,
                            Amount = p.Price,
                            Qty = p.Qty,
                            Status = "pending"
                        });
                    });
                    db.SaveChanges();

                    OrderModel om = new OrderModel
                    {
                        CustomerId = order.CustomerId,
                        Status = order.Status,
                        ID = order.ID
                    };
                    sendEmail(om);

                    return Created(new Uri(Request.RequestUri.ToString()), model);
                }
                return Created(new Uri(Request.RequestUri + "/"), "Customer Not Found");
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
        [HttpGet]
        public void sendTestMail()
        {
            try
            {
                var email = ConfigurationManager.AppSettings["Email"].ToString();
                var pass = ConfigurationManager.AppSettings["Password"].ToString();
                
                SmtpClient smtpClient = new SmtpClient();
                NetworkCredential smtpCredentials = new NetworkCredential(email, pass);

                MailMessage message = new MailMessage();
                MailAddress fromAddress = new MailAddress(email);
                MailAddress toAddress = new MailAddress("usamasaik@gmail.com");

                smtpClient.Host = "win10.hosterpk.com";
                smtpClient.Port = 25;
                smtpClient.EnableSsl = true;
                smtpClient.UseDefaultCredentials = false;
                smtpClient.Credentials = smtpCredentials;
                smtpClient.DeliveryMethod = SmtpDeliveryMethod.Network;
                //smtpClient.Timeout = 20000;

                message.From = fromAddress;
                message.To.Add(toAddress);
                message.IsBodyHtml = false;
                message.Subject = "example";
                message.Body = "example";

                smtpClient.Send(message);

            }
            catch (Exception ex)
            {
                if (ex != null)
                {

                }
            }

        }

        public void sendEmail(OrderModel order)
        {
            try
            {
                var email = ConfigurationManager.AppSettings["Email"].ToString();
                var pass = ConfigurationManager.AppSettings["Password"].ToString();

                var cust = db.Customers.SingleOrDefault(c => c.ID == order.CustomerId);

                SmtpClient smtp = new SmtpClient();
                smtp.Host = "win10.hosterpk.com"; //for gmail host  
                smtp.Port = 25;
                smtp.EnableSsl = true;
                smtp.UseDefaultCredentials = false;
                smtp.Credentials = new NetworkCredential(email, pass);
                smtp.DeliveryMethod = SmtpDeliveryMethod.Network;

                MailMessage message = new MailMessage();
                message.From = new MailAddress(email);
                message.To.Add(new MailAddress(cust.Email));
                message.Subject = "Order Status";
                message.IsBodyHtml = true; //to make message body as html  
                message.Body = "Your order# " + order.ID + " has been moved to" + order.Status;


                smtp.Send(message);
            }
            catch (Exception ex)
            {
                if (ex != null)
                {

                }
            }
        }
    }
    public class Message
    {
        public List<string> ids { get; set; }
        public string status { get; set; }
        public override string ToString()
        {
            return JsonConvert.SerializeObject(this);
        }
    }
}
