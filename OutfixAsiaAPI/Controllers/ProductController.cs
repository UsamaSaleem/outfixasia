﻿using Newtonsoft.Json;
using Outfix.Models;
using OutfixDB;
using Syncfusion.XlsIO;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Http;

namespace OutfixAsiaAPI.Controllers
{
    public class ProductController : ApiController
    {
        OutfixEntities db;

        #region Constructor / Destructor
        public ProductController()
        {
            db = new OutfixEntities();
        }
        #endregion

        #region Product

        #region Get Product

        // GET: api/Product
        public List<ProductModel> GetProductList()
        {
            var products = db.Products.ToList();
            if (products == null)
            {
                return new List<ProductModel>();
            }
            var model = new List<ProductModel>();
            products.ForEach(p =>
            {
                var pm = new ProductModel(p);   //Product Model Created 
                var pv = db.ProductVariations.Where(x => x.ProductId == p.ID.ToString()).ToList();  //get All variations of particular product 
                pv.ForEach(v => pm.Variation.Add(new ProductVariationModel(v)));    //Add Product variations to product model.
                model.Add(pm);  //Create a list of Product Models.
            });
            return model;
        }
        public List<ProductModel> GetActiveProductList()
        {
            var products = db.Products.Where(x => x.IsActive == 1).ToList();
            if (products == null)
            {
                return new List<ProductModel>();
            }
            var model = new List<ProductModel>();
            products.ForEach(p => model.Add(new ProductModel(p)));
            return model;
        }
        public List<ProductModel> GetInActiveProductList()
        {
            var products = db.Products.Where(x => x.IsActive == 0).ToList();
            if (products == null)
            {
                return new List<ProductModel>();
            }
            var model = new List<ProductModel>();
            products.ForEach(p => model.Add(new ProductModel(p)));
            return model;
        }
        public List<ProductModel> GetProductList(string filters)
        {
            try
            {
                var Filters = JsonConvert.DeserializeObject<SearchFilterModel>(filters);
                List<ProductModel> products = new List<ProductModel>();
                //x => x[field.Name] == field.GetValue()
                //var products = db.sp_GetFilteredProducts(Filters.Category,Filters.SubCategory,Filters.Brand,Filters.Type,Filters.PriceMin,Filters.PriceMax,Filters.Rating);

                using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["OutfixDB"].ConnectionString))
                {
                    SqlCommand storedProcCommand = new SqlCommand("sp_GetFilteredProducts", con);
                    SqlDataReader reader;

                    storedProcCommand.CommandText = "sp_GetFilteredProducts";
                    storedProcCommand.CommandType = CommandType.StoredProcedure;
                    storedProcCommand.Parameters.AddWithValue("@Category", Filters.Ct != null ? Filters.Ct : "");
                    storedProcCommand.Parameters.AddWithValue("@SubCategory", Filters.SCt != null ? Filters.SCt : "");
                    storedProcCommand.Parameters.AddWithValue("@Brand", Filters.Bd != null ? Filters.Bd : "");
                    storedProcCommand.Parameters.AddWithValue("@Type", Filters.Tp != null ? Filters.Tp : "");
                    storedProcCommand.Parameters.AddWithValue("@PriceMin", Filters.PMin != null ? Filters.PMin : "");
                    storedProcCommand.Parameters.AddWithValue("@PriceMax", Filters.PMax != null ? Filters.PMax : "");
                    storedProcCommand.Parameters.AddWithValue("@Rating", Filters.Rt != null ? Filters.Rt : "");
                    storedProcCommand.Parameters.AddWithValue("@ColarSize", Filters.CSz != null ? Filters.CSz : "");
                    storedProcCommand.Parameters.AddWithValue("@Size", Filters.Sz != null ? Filters.Sz : "");
                    storedProcCommand.Parameters.AddWithValue("@Color", Filters.Cr != null ? Filters.Cr : "");
                    storedProcCommand.Parameters.AddWithValue("@FitType", Filters.FtTp != null ? Filters.FtTp : "");
                    storedProcCommand.Parameters.AddWithValue("@Material", Filters.Mt != null ? Filters.Mt : "");
                    storedProcCommand.Parameters.AddWithValue("@SortBy", Filters.SB != null ? Filters.SB : "");
                    storedProcCommand.Parameters.AddWithValue("@SortOrder", Filters.SO != null ? Filters.SO : "");

                    // Execute the command.
                    con.Open();
                    reader = storedProcCommand.ExecuteReader();

                    while (reader.Read())
                    {
                        var ProductModel = new ProductModel
                        {
                            ID = Convert.ToInt32(reader["ID"].ToString()),
                            ItemCode = reader["ItemCode"].ToString(),
                            MainCategory = reader["MainCategory"].ToString(),
                            SubCategory = reader["SubCategory"].ToString(),
                            Brand = reader["Brand"].ToString(),
                            Price = reader["Price"].ToString() != "" ? Convert.ToDecimal(reader["Price"].ToString()) : 0,
                            DiscountedPrice = reader["DiscountedPrice"].ToString() != "" ? Convert.ToDecimal(reader["DiscountedPrice"].ToString()) : 0,
                            Type = reader["Type"].ToString(),
                            Name = reader["Name"].ToString(),
                            VendorId = reader["VendorId"].ToString(),
                            Description = reader["Description"].ToString(),
                            ReturnPolicy = reader["ReturnPolicy"].ToString(),
                            Picture1 = reader["Picture1"].ToString(),
                            Picture2 = reader["Picture2"].ToString(),
                            Picture3 = reader["Picture3"].ToString(),
                            Picture4 = reader["Picture4"].ToString(),
                            Picture5 = reader["Picture5"].ToString(),
                            Qty = reader["Qty"].ToString() != "" ? Convert.ToInt32(reader["Qty"].ToString()) : 0,
                            //  Price = reader["Price"].ToString() != "" ? Convert.ToDecimal(reader["Price"].ToString()) : 0,
                            //  Size = reader["Size"].ToString(),
                            //  Color = reader["Color"].ToString(),
                            Rating = reader["Rating"].ToString() != "" ? Convert.ToDecimal(reader["Rating"].ToString()) : 0,
                            UnitSold = reader["UnitSold"].ToString() != "" ? Convert.ToInt32(reader["UnitSold"].ToString()) : 0,
                            Remarks = reader["Remarks"].ToString(),
                            IsActive = reader["IsActive"].ToString() != "" ? Convert.ToInt32(reader["IsActive"].ToString()) : 0,
                            //Cart = new CartModel
                            //{
                            //    Id = reader["ItemCode"].ToString(),
                            //    Name = reader["Name"].ToString(),
                            //    Pic = reader["Picture1"].ToString(),
                            //    Vendor = reader["VendorId"].ToString(),
                            //},
                        };

                        products.Add(ProductModel);
                    }
                    con.Close();
                }

                return products;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        [HttpGet]
        public List<String> SearchProductName(string query)
        {
            try
            {
                var dbProducts = db.Products.Where(x => x.Name.Contains(query)).Select(x => x.Name).Distinct().ToList();
                return dbProducts;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        [HttpGet]
        public List<ProductModel> SearchProduct(string query)
        {
            try
            {
                var dbProducts = db.Products.Where(x => x.Name.Contains(query)).ToList();
                List<ProductModel> products = new List<ProductModel>();
                dbProducts.ForEach(x => products.Add(new ProductModel(x)));

                return products;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        // GET: api/Product/5
        public ProductModel GetProductWithVariationById(string id)
        {
            var product = db.Products.FirstOrDefault(x => x.ItemCode == id);
            if (product == null)
            {
                return null;
            }
            var variations = db.ProductVariations.Where(x => x.ProductId == product.ItemCode).ToList();
            var model = new ProductModel(product);
            model.Variation = new List<ProductVariationModel>();
            variations.ForEach(x => model.Variation.Add(new ProductVariationModel(x)));
            return model;
        }
        public ProductModel GetProductById(string id)
        {
            var product = db.Products.FirstOrDefault(x => x.ItemCode == id);
            if (product == null)
            {
                return null;
            }
            var model = new ProductModel(product);
            return model;
        }
        public ProductModel GetProductByMainCategory(string id)
        {
            var produtct = db.Products.FirstOrDefault(x => x.MainCategory == id);
            if (produtct == null)
            {
                return null;
            }
            var model = new ProductModel(produtct);
            return model;
        }
        public ProductModel GetProductBySubCategory(string id)
        {
            var produtct = db.Products.FirstOrDefault(x => x.SubCategory == id);
            if (produtct == null)
            {
                return null;
            }
            var model = new ProductModel(produtct);
            return model;
        }
        public ProductModel GetProductByBrand(string id)
        {
            var produtct = db.Products.FirstOrDefault(x => x.Brand == id);
            if (produtct == null)
            {
                return null;
            }
            var model = new ProductModel(produtct);
            return model;
        }
        public ProductModel GetProductByType(string type)
        {
            var category = db.Products.FirstOrDefault(x => x.Type.Split(',').Any(y => y == type));
            if (category == null)
            {
                return null;
            }
            var model = new ProductModel(category);
            return model;
        }
        [HttpGet]
        public List<ProductModel> GetSimilarProducts(string itemcode)
        {
            var model = new List<ProductModel>();
            var p = db.Products.FirstOrDefault(x => x.ItemCode == itemcode);
            if (p != null)
            {
                var products = db.Products.Where(x => (x.Type == p.Type || x.VendorId == p.VendorId || x.ProductType == p.ProductType ||
                                                 x.Brand == p.Brand || x.MainCategory == p.MainCategory || x.SubCategory == p.SubCategory) && x.ItemCode != p.ItemCode)
                                                 .Take(9).ToList();
                products.ForEach(x => model.Add(new ProductModel(x)));
            }
            return model;
        }

        #endregion

        #region Create/Update Product
        // POST: api/Product
        [HttpPost]
        public IHttpActionResult CreateProduct(ProductModel p)
        {
            try
            {
                // var p = JsonConvert.DeserializeObject<ProductModel>(json);
                var product = new Product
                {
                    ItemCode = p.ItemCode,
                    IsActive = 1,
                    Name = p.Name,
                    Brand = p.Brand,
                    MainCategory = p.MainCategory,
                    SubCategory = p.SubCategory,
                    Picture1 = p.Picture1,
                    Picture2 = p.Picture2,
                    Picture3 = p.Picture3,
                    Picture4 = p.Picture4,
                    Picture5 = p.Picture5,
                    Type = p.Type,
                    ProductType = p.ProductType,
                    Description = p.Description,
                    ReturnPolicy = p.ReturnPolicy,
                    VendorId = p.VendorId,
                    Price = p.Price,
                    DiscountedPrice = p.DiscountedPrice
                };
                db.Products.Add(product);
                p.Variation.ForEach(x =>
                {
                    var PVariation = new ProductVariation
                    {
                        ProductId = p.ItemCode,
                        Color = x.Color,
                        ColarSize = x.ColarSize,
                        FitType = x.FitType,
                        Size = x.Size,
                        VPrice = x.Price,
                        Qty = x.Qty
                    };
                    db.ProductVariations.Add(PVariation);
                });

                db.SaveChanges();
                p.ID = product.ID;
                return Created(new Uri(Request.RequestUri + "/" + product.ID), p);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        // PUT: api/Product/5
        [HttpPost]
        public IHttpActionResult UpdateProduct(string json)
        {
            try
            {
                var p = JsonConvert.DeserializeObject<ProductModel>(json);

                var product = db.Products.FirstOrDefault(x => x.ItemCode == p.ItemCode);
                if (product != null)
                {
                    product.ItemCode = product.ItemCode;
                    product.IsActive = 1;
                    product.Name = product.Name;
                    product.Description = product.Description;
                    //product.Price = product.Price;

                    db.SaveChanges();
                }
                else
                {
                    return NotFound();
                }

                return Ok();
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpPost]
        public IHttpActionResult UpdateProductById(string id, string json)
        {
            try
            {
                var p = JsonConvert.DeserializeObject<ProductModel>(json);

                var product = db.Products.FirstOrDefault(x => x.ItemCode == id);
                if (product != null)
                {
                    product.ItemCode = product.ItemCode;
                    product.IsActive = 1;
                    product.Name = product.Name;
                    product.Description = product.Description;
                    //product.Price = product.Price;

                    db.SaveChanges();
                }
                else
                {
                    return NotFound();
                }

                return Ok();
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
        #endregion

        #region Delete Product
        // DELETE: api/Product/5
        [HttpPost]
        public IHttpActionResult DeleteProduct(string json)
        {
            try
            {
                var p = JsonConvert.DeserializeObject<ProductModel>(json);
                var product = db.Products.FirstOrDefault(x => x.ItemCode == p.ItemCode);
                if (product != null)
                {
                    db.Products.Remove(product);
                    db.SaveChanges();
                }
                else
                {
                    return NotFound();
                }

                return Ok();
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpPost]
        public IHttpActionResult DeleteProductbyId(string id)
        {
            try
            {
                var product = db.Products.FirstOrDefault(x => x.ItemCode == id);
                if (product != null)
                {
                    db.Products.Remove(product);
                    db.SaveChanges();
                }
                else
                {
                    return NotFound();
                }

                return Ok();
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
        #endregion

        #endregion

        #region Main Category
        [HttpPost]
        public IHttpActionResult CreateMainCategory(MainCategoryModel cat)
        {
            try
            {
                //var cat = JsonConvert.DeserializeObject<MainCategoryModel>(json);

                var Type = "";
                foreach (var type in cat.Type)
                {
                    Type += type + ",";
                }
                var category = new Category
                {
                    Name = cat.Name,
                    Description = cat.Description,
                    Type = Type.Substring(0, Type.Length - 1)
                };
                db.Categories.Add(category);
                db.SaveChanges();
                return Created(new Uri(Request.RequestUri + "/" + category.Name), cat);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
        // GET: api/Product
        public List<MainCategoryModel> GetMainCategoryList()
        {
            var categories = db.Categories.ToList();
            if (categories == null)
            {
                return new List<MainCategoryModel>();
            }
            var model = new List<MainCategoryModel>();
            categories.ForEach(c => model.Add(new MainCategoryModel(c)));
            return model;
        }

        public MainCategoryModel GetMainCategoryById(string id)
        {
            var category = db.Categories.FirstOrDefault(x => x.Name == id);
            if (category == null)
            {
                return null;
            }
            var model = new MainCategoryModel(category);
            return model;
        }

        public MainCategoryModel GetMainCategoryByType(string type)
        {
            var category = db.Categories.FirstOrDefault(x => x.Type.Split(',').Any(y => y == type));
            if (category == null)
            {
                return null;
            }
            var model = new MainCategoryModel(category);
            return model;
        }

        #endregion

        #region Sub Category
        [HttpPost]
        public IHttpActionResult CreateSubCategory(SubCategoryModel cat)
        {
            try
            {
                //                var cat = JsonConvert.DeserializeObject<SubCategoryModel>(json);
                var Type = "";
                foreach (var type in cat.Type)
                {
                    Type += type + ",";
                }

                var category = new SubCategory
                {
                    Name = cat.Name,
                    Description = cat.Description,
                    Type = Type.Substring(0, Type.Length - 1),
                    CategoryName = cat.MainCategoryName,
                };
                db.SubCategories.Add(category);
                db.SaveChanges();
                return Created(new Uri(Request.RequestUri + "/" + category.Name), cat);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
        // GET: api/Product
        public List<SubCategoryModel> GetSubCategoryList()
        {
            var sc = db.SubCategories.ToList();
            if (sc == null)
            {
                return new List<SubCategoryModel>();
            }
            var model = new List<SubCategoryModel>();
            sc.ForEach(c => model.Add(new SubCategoryModel(c)));
            return model;
        }
        public SubCategoryModel GetSubCategoryById(string id)
        {
            var category = db.SubCategories.FirstOrDefault(x => x.Name == id);
            if (category == null)
            {
                return null;
            }
            var model = new SubCategoryModel(category);
            return model;
        }
        public SubCategoryModel GetSubCategoryByCategoryId(string id)
        {
            var category = db.SubCategories.FirstOrDefault(x => x.CategoryName == id);
            if (category == null)
            {
                return null;
            }
            var model = new SubCategoryModel(category);
            return model;
        }
        public SubCategoryModel GetSubCategoryByType(string type)
        {
            var category = db.SubCategories.FirstOrDefault(x => x.Type.Split(',').Any(y => y == type));
            if (category == null)
            {
                return null;
            }
            var model = new SubCategoryModel(category);
            return model;
        }
        #endregion

        #region Brand
        [HttpPost]
        public IHttpActionResult CreateBrand(BrandModel b)
        {
            try
            {
                var brand = db.Brands.FirstOrDefault(x => x.Name == b.Name);
                if (brand != null)
                {
                    return BadRequest("Brand already exist");
                }

                brand = new Brand
                {
                    Name = b.Name,
                    Description = b.Description,
                    CategoryName = b.MainCategoryName,
                    SubCategoryName = b.SubCategoryName,
                };
                db.Brands.Add(brand);
                db.SaveChanges();
                return Created(new Uri(Request.RequestUri + "/" + brand.Name), b);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
        // GET: api/Product
        public List<BrandModel> GetBrandList()
        {
            var brands = db.Brands.ToList();
            if (brands == null)
            {
                return new List<BrandModel>();
            }
            var model = new List<BrandModel>();
            brands.ForEach(b => model.Add(new BrandModel(b)));
            return model;
        }
        public BrandModel GetBrandById(string id)
        {
            var brand = db.Brands.FirstOrDefault(x => x.Name == id);
            if (brand == null)
            {
                return null;
            }
            var model = new BrandModel(brand);
            return model;
        }
        public BrandModel GetBrandByCategoryId(string id)
        {
            var brand = db.Brands.FirstOrDefault(x => x.CategoryName == id);
            if (brand == null)
            {
                return null;
            }
            var model = new BrandModel(brand);
            return model;
        }
        public BrandModel GetBrandBySubCategoryId(string id)
        {
            var brand = db.Brands.FirstOrDefault(x => x.SubCategoryName == id);
            if (brand == null)
            {
                return null;
            }
            var model = new BrandModel(brand);
            return model;
        }
        public BrandModel GetBrandByCategoryAndSubCategoryId(string id, string subId)
        {
            var brand = db.Brands.FirstOrDefault(x => x.CategoryName == id && x.SubCategoryName == subId);
            if (brand == null)
            {
                return null;
            }
            var model = new BrandModel(brand);
            return model;
        }
        #endregion

        #region Product Type
        [HttpPost]
        public IHttpActionResult CreateProductType(ProductTypeModel t)
        {
            try
            {
                //var t = JsonConvert.DeserializeObject<ProductTypeModel>(json);
                var type = db.ProductTypes.FirstOrDefault(x => x.Name == t.Name);
                if (type != null)
                {
                    return BadRequest("Type already exist");
                }

                type = new ProductType
                {
                    Name = t.Name,
                    Description = t.Description,
                    Picture = t.Picture,
                };
                db.ProductTypes.Add(type);
                db.SaveChanges();
                return Created(new Uri(Request.RequestUri + "/" + type.Name), t);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
        // GET: api/Product
        public List<ProductTypeModel> GetProductTypeList()
        {
            var types = db.ProductTypes.ToList();
            if (types == null)
            {
                return new List<ProductTypeModel>();
            }
            var model = new List<ProductTypeModel>();
            types.ForEach(t => model.Add(new ProductTypeModel(t)));
            return model;
        }
        public ProductTypeModel GetProductTypeById(string id)
        {
            var type = db.ProductTypes.FirstOrDefault(x => x.Name == id);
            if (type == null)
            {
                return null;
            }
            var model = new ProductTypeModel(type);
            return model;
        }
        #endregion

        public List<ProductFilterModel> GetFilterList()
        {
            try
            {
                List<ProductFilterModel> productFilters = new List<ProductFilterModel>();

                using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["OutfixDB"].ConnectionString))
                {
                    SqlCommand storedProcCommand = new SqlCommand("sp_GetAllFilters", con);
                    SqlDataReader reader;

                    storedProcCommand.CommandText = "sp_GetAllFilters";
                    storedProcCommand.CommandType = CommandType.StoredProcedure;

                    // Execute the command.
                    con.Open();
                    reader = storedProcCommand.ExecuteReader();

                    while (reader.Read())
                    {
                        ProductFilterModel ProductFilterModel;
                        var ID = Convert.ToInt32(reader["i"].ToString());
                        var Name = reader["Name"].ToString();
                        var Alias = reader["Alias"].ToString();
                        var Value = reader["Value"].ToString();
                        var FullName = reader["FullName"].ToString();

                        var filter = productFilters.FirstOrDefault(x => x.Name == Name);
                        if (filter == null)
                        {
                            ProductFilterModel = new ProductFilterModel();
                            ProductFilterModel.ID = ID;
                            ProductFilterModel.Name = Name;
                            ProductFilterModel.FullName = FullName;
                            ProductFilterModel.Alias = Alias;
                            ProductFilterModel.Values.Add(Value);

                            productFilters.Add(ProductFilterModel);
                        }
                        else
                        {
                            filter.Values.Add(reader["Value"].ToString());
                        }

                    }
                    con.Close();
                }

                return productFilters;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<ProductFilterModel> getAllFilterNames()
        {
            var names = db.ProductFilters.Where(x => x.IsVariation == true).ToList();
            var model = new List<ProductFilterModel>();
            names.ForEach(x => model.Add(new ProductFilterModel(x)));

            return model;
        }
        [HttpGet]
        public List<ProductVariationModel> GetVariationDropDowns(string ids)
        {
            List<ProductVariationModel> m = new List<ProductVariationModel>();

            var idArr = ids.Split(',');
            var names = db.ProductVariations.Where(x => idArr.Contains(x.Id.ToString())).ToList();
            names.ForEach(x => m.Add(new ProductVariationModel(x)));

            return m;
        }
        [HttpPost]
        public ProductVariationModel GetVariationProductbyId(CartModel model)
        {
            try
            {
                //var Filters = JsonConvert.DeserializeObject<ProductVariationModel>(ids);
                ProductVariationModel Variation = new ProductVariationModel();
                //x => x[field.Name] == field.GetValue()
                //var products = db.sp_GetFilteredProducts(Filters.Category,Filters.SubCategory,Filters.Brand,Filters.Type,Filters.PriceMin,Filters.PriceMax,Filters.Rating);

                using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["OutfixDB"].ConnectionString))
                {
                    SqlCommand storedProcCommand = new SqlCommand("sp_GetVariationProduct", con);
                    SqlDataReader reader;

                    storedProcCommand.CommandText = "sp_GetVariationProduct";
                    storedProcCommand.CommandType = CommandType.StoredProcedure;
                    storedProcCommand.Parameters.AddWithValue("@ItemCode", model.Id != null ? model.Id : "");
                    storedProcCommand.Parameters.AddWithValue("@ColarSize", model.Variation.ColarSize != null ? model.Variation.ColarSize : "");
                    storedProcCommand.Parameters.AddWithValue("@Size", model.Variation.Size != null ? model.Variation.Size : "");
                    storedProcCommand.Parameters.AddWithValue("@Color", model.Variation.Color != null ? model.Variation.Color : "");
                    storedProcCommand.Parameters.AddWithValue("@FitType", model.Variation.FitType != null ? model.Variation.FitType : "");

                    // Execute the command.
                    con.Open();
                    reader = storedProcCommand.ExecuteReader();

                    while (reader.Read())
                    {

                        Variation = new ProductVariationModel
                        {
                            ID = Convert.ToInt32(reader["ID"].ToString()),
                            Color = reader["Color"].ToString(),
                            Size = reader["Size"].ToString(),
                            ColarSize = reader["ColarSize"].ToString(),
                            FitType = reader["FitType"].ToString(),
                            Price = reader["Price"].ToString() != "" ? Convert.ToDecimal(reader["Price"].ToString()) : 0,
                            Qty = reader["Qty"].ToString() != "" ? Convert.ToInt32(reader["Qty"].ToString()) : 0,
                        };
                    }
                    con.Close();
                }

                return Variation;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        [HttpGet]
        public void ExportToExcel()
        {
            using (ExcelEngine excelEngine = new ExcelEngine())
            {
                IApplication application = excelEngine.Excel;
                application.DefaultVersion = ExcelVersion.Excel2016;

                //Create a new workbook
                IWorkbook workbook = application.Workbooks.Create(1);
                IWorksheet sheet = workbook.Worksheets[0];

                var plist = db.Products.ToList();
                int i = 1;
                object[] titles = new object[]
                    {
                        "ItemCode","Name","Description","MainCategory","SubCategory","Brand","Price","BulkPrice","DiscountedPrice","Picture1","Picture2",
                        "Picture3","Picture4","Picture5","IsActive"
                    };
                sheet.InsertRow(i, 1, ExcelInsertOptions.FormatAsBefore);
                sheet.ImportArray(titles, i, 1, false);

                foreach (var p in plist)
                {
                    i += 1;
                    object[] product = new object[]
                    {
                        p.ItemCode,p.Name,p.Description,p.MainCategory,p.SubCategory,p.Brand,p.Price,p.BulkPrice,p.DiscountedPrice,p.Picture1,p.Picture2,
                        p.Picture3,p.Picture4,p.Picture5,p.IsActive
                    };
                    sheet.InsertRow(i, 1, ExcelInsertOptions.FormatAsBefore);
                    sheet.ImportArray(product, i, 1, false);
                }

                //Save the file in the given path
                Stream excelStream = File.Create(Path.GetFullPath(@"D:\Startup\Projects\OutputOutfix.xlsx"));
                workbook.SaveAs(excelStream);
                excelStream.Dispose();
            }
        }

        [HttpGet]
        public void ImportFromExcel(HttpPostedFileBase FileUpload)
        {
            using (ExcelEngine excelEngine = new ExcelEngine())
            {
                //Initialize application
                IApplication app = excelEngine.Excel;

                //Set default application version as Excel 2016
                app.DefaultVersion = ExcelVersion.Excel2016;

                //Open existing Excel workbook from the specified location
                IWorkbook workbook = app.Workbooks.Open(FileUpload.InputStream, ExcelOpenType.Automatic);

                //Access the first worksheet
                IWorksheet worksheet = workbook.Worksheets[0];

                //Access the used range of the Excel file
                IRange usedRange = worksheet.UsedRange;
                int lastRow = usedRange.LastRow;
                int lastColumn = usedRange.LastColumn;
                //Iterate the cells in the used range and print the cell values
                for (int row = 2; row <= lastRow; row++)
                {
                    var Product = new Product
                    {
                        ItemCode = worksheet[row, 1].Value,
                        Name = worksheet[row, 2].Value,
                        Description = worksheet[row, 3].Value,
                        MainCategory = worksheet[row, 4].Value,
                        SubCategory = worksheet[row, 5].Value,
                        Brand = worksheet[row, 6].Value,
                        Price = Convert.ToDecimal(worksheet[row, 7].Value),
                        BulkPrice = Convert.ToDecimal(worksheet[row, 8].Value),
                        DiscountedPrice = Convert.ToDecimal(worksheet[row, 9].Value),
                        Picture1 = worksheet[row, 10].Value,
                        Picture2 = worksheet[row, 10].Value,
                        Picture3 = worksheet[row, 10].Value,
                        Picture4 = worksheet[row, 10].Value,
                        Picture5 = worksheet[row, 10].Value,
                        IsActive = Convert.ToInt32(worksheet[row, 11].Value),
                    };
                    db.Products.Add(Product);
                    //var Name = dr[1].ToString();
                }

                workbook.Close();
                db.SaveChanges();
            }
        }

        [HttpGet]
        public List<SubCategory> GetSubCategoryList(string val)
        {
            var list = db.SubCategories.Where(x => x.CategoryName.ToLower().Equals(val.ToLower())).ToList();
            return list;
        }
    }
}