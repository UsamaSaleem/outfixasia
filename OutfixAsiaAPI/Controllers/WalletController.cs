﻿using Outfix.Models;
using OutfixDB;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web.Http;

namespace OutfixAsiaAPI.Controllers
{
    public class WalletController : ApiController
    {
        OutfixEntities db;

        public WalletController()
        {
            db = new OutfixEntities();
        }
        #region Get Wallet
        public List<WalletModel> GetAllWallet()
        {
            var Wallet = db.Wallets.ToList();
            if (Wallet == null || Wallet.Count <= 0)
            {
                return new List<WalletModel>();
            }
            var model = new List<WalletModel>();
            Wallet.ForEach(r => model.Add(new WalletModel(r)));
            return model;
        }
        public WalletModel GetWalletByUser(string UserId)
        {
            var Wallet = db.Wallets.SingleOrDefault(x => x.UserId.ToString() == UserId);
            if (Wallet == null)
            {
                return null;
            }
            var model = new WalletModel(Wallet);
            return model;
        }

        public WalletModel GetWalletById(string Id)
        {
            var Wallet = db.Wallets.SingleOrDefault(x => x.Id.ToString() == Id);
            var model = new WalletModel(Wallet);
            return model;
        }

        public List<WalletDetailsModel> GetWalletByWalletId(string WalletId)
        {
            var wd = db.WalletDetails.Where(x => x.WalletId.ToString() == WalletId).ToList();
            var model = new List<WalletDetailsModel>();
            wd.ForEach(x => model.Add(new WalletDetailsModel(x)));
            return model;
        }
        public List<WalletDetailsModel> GetWalletByCode(string Code)
        {
            var wd = db.WalletDetails.Where(x => x.CodeUsed.ToString() == Code).ToList();
            var model = new List<WalletDetailsModel>();
            wd.ForEach(x => model.Add(new WalletDetailsModel(x)));
            return model;
        }
        public List<WalletDetailsModel> GetAllWalletDetailsByExpiryDate(DateTime Expiry)
        {
            var WalletDet = db.WalletDetails.Where(x => x.ExpiryDate == Expiry).ToList();
            if (WalletDet == null || WalletDet.Count <= 0)
            {
                return new List<WalletDetailsModel>();
            }
            var model = new List<WalletDetailsModel>();
            WalletDet.ForEach(r => model.Add(new WalletDetailsModel(r)));
            return model;
        }
        public List<WalletDetailsModel> GetAllWalletDetailsByDate(DateTime Date)
        {
            var WalletDet = db.WalletDetails.Where(x => x.Date == Date).ToList();
            if (WalletDet == null || WalletDet.Count <= 0)
            {
                return new List<WalletDetailsModel>();
            }
            var model = new List<WalletDetailsModel>();
            WalletDet.ForEach(r => model.Add(new WalletDetailsModel(r)));
            return model;
        }
        public List<WalletDetailsModel> GetAllWalletDetailsByState(int state)
        {
            var WalletDet = db.WalletDetails.Where(x => x.IsActive == state).ToList();
            if (WalletDet == null || WalletDet.Count <= 0)
            {
                return new List<WalletDetailsModel>();
            }
            var model = new List<WalletDetailsModel>();
            WalletDet.ForEach(r => model.Add(new WalletDetailsModel(r)));
            return model;
        }
        public List<WalletDetailsModel> GetAllWalletDetailsByCreditUsedGE(decimal Credit)
        {
            var WalletDet = db.WalletDetails.Where(x => x.TotalCreditUsed >= Credit).ToList();
            if (WalletDet == null || WalletDet.Count <= 0)
            {
                return new List<WalletDetailsModel>();
            }
            var model = new List<WalletDetailsModel>();
            WalletDet.ForEach(r => model.Add(new WalletDetailsModel(r)));
            return model;
        }
        public List<WalletDetailsModel> GetAllWalletDetailsByCreditUsedLE(decimal Credit)
        {
            var WalletDet = db.WalletDetails.Where(x => x.TotalCreditUsed <= Credit).ToList();
            if (WalletDet == null || WalletDet.Count <= 0)
            {
                return new List<WalletDetailsModel>();
            }
            var model = new List<WalletDetailsModel>();
            WalletDet.ForEach(r => model.Add(new WalletDetailsModel(r)));
            return model;
        }
        public List<WalletDetailsModel> GetAllWalletDetailsByCreditUsedEQ(decimal Credit)
        {
            var WalletDet = db.WalletDetails.Where(x => x.TotalCreditUsed == Credit).ToList();
            if (WalletDet == null || WalletDet.Count <= 0)
            {
                return new List<WalletDetailsModel>();
            }
            var model = new List<WalletDetailsModel>();
            WalletDet.ForEach(r => model.Add(new WalletDetailsModel(r)));
            return model;
        }
        public List<WalletDetailsModel> GetAllWalletDetailsByAddedUsedGE(decimal Credit)
        {
            var WalletDet = db.WalletDetails.Where(x => x.TotalCreditAdded >= Credit).ToList();
            if (WalletDet == null || WalletDet.Count <= 0)
            {
                return new List<WalletDetailsModel>();
            }
            var model = new List<WalletDetailsModel>();
            WalletDet.ForEach(r => model.Add(new WalletDetailsModel(r)));
            return model;
        }
        public List<WalletDetailsModel> GetAllWalletDetailsByAddedUsedLE(decimal Credit)
        {
            var WalletDet = db.WalletDetails.Where(x => x.TotalCreditAdded <= Credit).ToList();
            if (WalletDet == null || WalletDet.Count <= 0)
            {
                return new List<WalletDetailsModel>();
            }
            var model = new List<WalletDetailsModel>();
            WalletDet.ForEach(r => model.Add(new WalletDetailsModel(r)));
            return model;
        }
        public List<WalletDetailsModel> GetAllWalletDetailsByCreditAddedEQ(decimal Credit)
        {
            var WalletDet = db.WalletDetails.Where(x => x.TotalCreditAdded == Credit).ToList();
            if (WalletDet == null || WalletDet.Count <= 0)
            {
                return new List<WalletDetailsModel>();
            }
            var model = new List<WalletDetailsModel>();
            WalletDet.ForEach(r => model.Add(new WalletDetailsModel(r)));
            return model;
        }
        #endregion
        #region Update Wallet
        #endregion

        #region Create Wallet
        [HttpPost]
        public IHttpActionResult CreateWalletForUser(WalletModel model)
        {
            try
            {
                var customer = db.Customers.FirstOrDefault(x => x.ID == model.UserID);

                if (customer != null)
                {
                    var wallet = new Wallet
                    {
                        UserId = model.UserID,
                        Credit = model.Credit,
                    };
                    db.Wallets.Add(wallet);
                    db.SaveChanges();
                    model.ID = wallet.Id;

                    var walletDetail = new WalletDetail
                    {
                        WalletId = wallet.Id,
                        Date = DateTime.Now,
                        CodeUsed = "",
                        IsActive = 1,
                        ExpiryDate = DateTime.Now.AddYears(1),
                        TotalCreditUsed = 0,
                        TotalCreditAdded = 300,
                        Description = "Sign Up Bonus"
                    };
                    db.WalletDetails.Add(walletDetail);

                    Random rnd = new Random();
                    int value = rnd.Next(0, 99999);

                    var referral = new ReferalCode
                    {
                        Code = customer.Username + value.ToString("D5"),
                        Name = customer.FirstName,
                        WalletPoints = 50,
                        Limit = 5,
                        Description = "Referral code of user " + customer.Username,
                        UserId = customer.ID,
                        IsSignUp = true
                    };
                    db.ReferalCodes.Add(referral);
                    db.SaveChanges();

                    return Created(new Uri(Request.RequestUri + "/" + wallet.Id), model);
                }
                return Created(new Uri(Request.RequestUri + "/"), "Customer Not Found");
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        public IHttpActionResult CreateWalletDetailForUser(WalletDetail model)
        {
            try
            {
                // var p = JsonConvert.DeserializeObject<ProductModel>(json);
                var walletDetail = new WalletDetail
                {
                    WalletId = model.WalletId,
                    CodeUsed = model.CodeUsed,
                    IsActive = model.IsActive,
                    Date = model.Date,
                    ExpiryDate = model.ExpiryDate,
                    TotalCreditUsed = model.TotalCreditUsed,
                    TotalCreditAdded = model.TotalCreditAdded,
                    Description = model.Description
                };
                db.WalletDetails.Add(walletDetail);
                db.SaveChanges();
                model.Id = walletDetail.Id;
                return Created(new Uri(Request.RequestUri + "/" + walletDetail.Id), model);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpPost]
        public IHttpActionResult UpdateWalletByUserId(WalletModel w)
        {
            try
            {
                //var w = JsonConvert.DeserializeObject<WalletModel>(json);
                var uid = Convert.ToInt32(w.UserID);
                var wallet = db.Wallets.SingleOrDefault(x => x.UserId == uid);
                if (wallet != null)
                {
                    wallet.Credit = wallet.Credit + w.Credit;
                    //wallet.UserId = wallet.UserId;
                    db.Entry(wallet).State = EntityState.Modified;


                    var wdetails = new WalletDetail
                    {
                        Date = DateTime.Now,

                    };
                    db.SaveChanges();
                }
                else
                {
                    return NotFound();
                }

                return Ok();
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
        [HttpPost]
        public IHttpActionResult UpdateWalletAfterReferralUsed(ReferralUsedModel r)
        {
            try
            {
                var cUsed = db.Customers.FirstOrDefault(x => x.ID == r.RefUsedByCustomer);    //customer details who used referral code
                var cRef = db.ReferalCodes.FirstOrDefault(x => x.Code == r.Referral); //referral details of the referral used

                if (cRef != null)
                {
                    cRef.Limit -= 1;    
                    db.Entry(cRef).State = EntityState.Modified;

                    var cRefDetails = new ReferalCodeDetail()
                    {
                        Id = cRef.Id,
                        Date = DateTime.Now,
                        IsSignup = true,
                        Description = "Sign-Up Code used by " + cUsed.Username,
                        UserId = r.RefUsedByCustomer,
                        ReferalCodeId = cRef.Id
                    };
                    db.ReferalCodeDetails.Add(cRefDetails);

                    var cWallet = db.Wallets.FirstOrDefault(x => x.UserId == cRef.UserId);      //Customer Wallet of which Referral code is used.

                    if (cWallet != null)
                    {
                        cWallet.Credit = cWallet.Credit + cRef.WalletPoints;
                        //wallet.UserId = wallet.UserId;
                        db.Entry(cWallet).State = EntityState.Modified;


                        var wdetails = new WalletDetail
                        {
                            WalletId = cWallet.Id,
                            Date = DateTime.Now,
                            CodeUsed = r.Referral,
                            TotalCreditAdded = cRef.WalletPoints,
                            Description = "Sign-Up Code used by " + cUsed.Username,
                            ExpiryDate = DateTime.Now.AddYears(1),
                            IsActive = 1,
                        };
                        db.WalletDetails.Add(wdetails);

                        db.SaveChanges();
                    }
                    else
                    {
                        return NotFound();
                    }
                }

                return Ok();
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
        #endregion



    }
}
