﻿using Outfix.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Outfix.ViewModels
{
    public class ReferralViewModel
    {
        public ReferralModel Referral { get; set; }
        public List<ReferralDetailsModel> ReferralDetails { get; set; }
    }
}
