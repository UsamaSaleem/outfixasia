﻿using Outfix.Models;
using System.Collections.Generic;

namespace Outfix.ViewModels
{
    public class CartViewModel
    {
        public CustomerModel Customer { get; set; }
        public List<ProductModel> Product { get; set; }
        public OrderModel Order { get; set; }
        public ReferralModel Voucher { get; set; }
        public int TotalItems { get; set; }
        public int TotalAmount { get; set; }
        public decimal Discount { get; set; }
        public bool SaveLocation { get; set; }

        public CartViewModel()
        {
            Product = new List<ProductModel>();
        }
    }
}
