﻿using Outfix.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Outfix.ViewModels
{
    public class ProductDetailViewModel
    {
        public ProductModel Product { get; set; }
        public List<ProductModel> SimilarProducts { get; set; }
        public List<ReviewModel> Reviews { get; set; }

        public ProductDetailViewModel()
        {
            Reviews = new List<ReviewModel>();
            SimilarProducts = new List<ProductModel>();
        }
    }
}
