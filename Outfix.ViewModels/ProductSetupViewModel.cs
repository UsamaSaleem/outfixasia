﻿using Outfix.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Outfix.ViewModels
{
    public class ProductSetupViewModel
    {
        public MainCategoryModel Main { get; set; }
        public SubCategoryModel Sub { get; set; }
        public BrandModel Brand { get; set; }
        public ProductTypeModel ProductType { get; set; }

    }
}
