﻿using Outfix.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Outfix.ViewModels
{
    public class CustomerDetailViewModel
    {
        public CustomerModel Customer { get; set; }
        public List<OrderModel> Orders { get; set; }
        public WalletModel Wallet { get; set; }


        public CustomerDetailViewModel()
        {
            Orders = new List<OrderModel>();
        }
    }
}
