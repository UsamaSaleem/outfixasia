﻿$(function () {

    $("#MainCategoryList").change(function () {
        var value = $(this).find('option:selected').val(); // val or text
        if (value === 'Create') {
            $("#formCreateCategory").prop('hidden', false);
        }
    });

    $("#SubCategoryList").change(function () {
        var value = $(this).find('option:selected').val(); // val or text
        if (value === 'Create') {
            $("#formCreateSubCategory").prop('hidden', false);
        }
    });

    $("#BrandList").change(function () {
        var value = $(this).find('option:selected').val(); // val or text
        if (value === 'Create') {
            $("#formCreateBrand").prop('hidden', false);
        }
    });

    $("#formCreateCategory").on('submit', function (e) {
        e.preventDefault(); // prevent the form's normal submission
        if (ProductSetup.PreSave("#formCreateCategory")) {
            ProductSetup.SaveCategory();
        }

    });

    $("#formCreateSubCategory").on('submit', function (e) {
        e.preventDefault(); // prevent the form's normal submission
        if (ProductSetup.PreSave("#formCreateSubCategory")) {
            ProductSetup.SaveSubCategory();
        }

    });

    $("#formCreateBrand").on('submit', function (e) {
        e.preventDefault(); // prevent the form's normal submission
        if (ProductSetup.PreSave("#formCreateBrand")) {
            ProductSetup.SaveBrand();
        }

    });

});

var ProductSetup = {
    PreSave: (form) => {
        return true;
    },
    SaveCategory: () => {
        var form = $("#formCreateCategory").serialize();
        $.ajax({
            url: "/Product/CreateCategory",
            type: 'post',
            data: form,
            dataType: 'json',
            success: function (data) {
            },
            error: function (jqXHR, exception) {
            }
        });
    },
    SaveSubCategory: () => {
        var form = $("#formCreateSubCategory").serialize();
        $.ajax({
            url: "/Product/CreateSubCategory",
            type: 'post',
            data: form,
            dataType: 'json',
            success: function (data) {
            },
            error: function (jqXHR, exception) {
            }
        });
    },
    SaveBrand: () => {
        var form = $("#formCreateBrand").serialize();
        $.ajax({
            url: "/Product/CreateBrand",
            type: 'post',
            data: form,
            dataType: 'json',
            success: function (data) {
            },
            error: function (jqXHR, exception) {
            }
        });
    },

};