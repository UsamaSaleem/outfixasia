﻿$(function () {

    $("#addProductForm").on('submit', function (e) {
        e.preventDefault(); // prevent the form's normal submission
        Product.PreSave();

    });
});

var Product = {
    PreSave: () => {
        Product.Save();
    },

    Save: () => {

        //var form = new FormData($("#addProductForm"));

        //$.ajax({
        //    url: "/Product/CreateNewProduct",
        //    type: 'post',
        //    data: form,
        //    contentType: false,
        //    processData: false,
        //    success: function (data) {
        //    },
        //    error: function (jqXHR, exception) {
        //    }
        //});
        debugger
        var data = new FormData();

        // You can update the jquery selector to use a css class if you want
        var inputs = document.getElementById("addProductForm").elements;

        for (var i = 0; i < inputs.length; i++) {
            data.append(inputs[i].name, inputs[i].value);
        }

        var files = $("#ImageFile").get(0).files;
        if (files.length > 0) {
            data.append("ProductPicture1", files[0]);
        }
        files = $("#ImageFile2").get(0).files;
        if (files.length > 0) {
            data.append("ProductPicture2", files[0]);
        }
        files = $("#ImageFile3").get(0).files;
        if (files.length > 0) {
            data.append("ProductPicture3", files[0]);
        }
        

        $.ajax({
            url: "/Product/CreateNewProduct",
            type: "POST",
            processData: false,
            contentType: false,
            data: data,
            success: function (response) {
                //code after success

            },
            error: function (er) {
                alert(er);
            }

        });
    },
};