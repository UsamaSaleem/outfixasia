﻿$(function () {
    $("#searchOrder").on("keyup", function () {
        var value = $(this).val().toLowerCase();
        $("#ReviewList tr").filter(function () {
            $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
        });
    });

    $('#btn_select').click(function () {
        var ids = [];
        $('#tblOrders #SelectOrder:checked').each(function () {
            id = $(this).closest('tr').find('#Id').text();
            ids.push(id);
        });
        var select = $('#selectOrderState').find(":selected").text();
        MngOrdr.ChangeOrderStatus(ids, select);
    });
    
});


var MngOrdr = {

    ChangeOrderStatus: (ids, status) => {
        $.ajax({
            url: "/Order/ChangeBulkStatus",
            type: 'post',
            dataType: 'json',
            data: {
                ids, status
            },
            success: function (data) {
            },
            error: function (jqXHR, exception) {
            }
        });
    },
}