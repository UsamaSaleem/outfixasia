﻿$(function () {

    $("#Approve").on("click", function (e) {
        Review.Update(this, "Approved");
    });
    $("#Trash").on("click", function (e) {
        Review.Update(this, "Trash");
    });
    $("#Spam").on("click", function (e) {
        Review.Update(this, "Spam");
    });
    $("#Unapprove").on("click", function (e) {
        Review.Update(this, "Unapprove");
    });
    $("#NotSpam").on("click", function (e) {
        Review.Update(this, "NotSpam");
    });
    $("#Redo").on("click", function (e) {
        Review.Update(this, "Redo");
    });
    $("#Delete").on("click", function (e) {
        Review.Update(this, "Delete");
    });
});

var Review = {
    Update: (row, state) => {
        var data = new Object();
        data.ID = $(row).closest('tr').find('#Id').text();
        data.Name = $(row).closest('tr').find('#Name').text();
        data.Status = state;
        $.ajax({
            url: "/Review/ApproveReview",
            type: "POST",
            dataType: 'json',
            contentType: 'application/json; charset=utf-8',
            data: JSON.stringify(data),
            success: function (response) {
                //code after success
            },
            error: function (er) {
                alert(er);
            }

        });
    },
    ChangeColor: (span) => {
        if (span.style.color !== "red")
            span.style.color = "red";
        else
            span.style.color = "black";
    }
};