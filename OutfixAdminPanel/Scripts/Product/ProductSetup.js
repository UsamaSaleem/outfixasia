﻿$(function () {

    $("#MainCategoryList").change(function () {
        var value = $(this).find('option:selected').val(); // val or text
        ProductSetup.changeSubCategoryList();
    });

    $("#SubCategoryList").change(function () {
        var value = $(this).find('option:selected').val(); // val or text
        ProductSetup.changeBrandList();
    });

    //$("#BrandList").change(function () {
    //    var value = $(this).find('option:selected').val(); // val or text
    //    if (value === 'Create') {
    //        $("#formCreateBrand").prop('hidden', false);
    //    }
    //});

    $("#formCreateCategory").on('submit', function (e) {
        e.preventDefault(); // prevent the form's normal submission
        if (ProductSetup.PreSave("#formCreateCategory")) {
            ProductSetup.SaveCategory();
        }

    });

    $("#formCreateSubCategory").on('submit', function (e) {
        e.preventDefault(); // prevent the form's normal submission
        if (ProductSetup.PreSave("#formCreateSubCategory")) {
            ProductSetup.SaveSubCategory();
        }

    });

    $("#formCreateBrand").on('submit', function (e) {
        e.preventDefault(); // prevent the form's normal submission
        if (ProductSetup.PreSave("#formCreateBrand")) {
            ProductSetup.SaveBrand();
        }
    });
    $("#formCreateType").on('submit', function (e) {
        e.preventDefault(); // prevent the form's normal submission
        if (ProductSetup.PreSave("#formCreateType")) {
            ProductSetup.SaveType();
        }
    });

});

var ProductSetup = {
    PreSave: (form) => {
        return true;
    },
    SaveCategory: () => {
        var form = $("#formCreateCategory").serialize();
        $.ajax({
            url: "/Product/CreateCategory",
            type: 'post',
            data: form,
            dataType: 'json',
            success: function (data) {
            },
            error: function (jqXHR, exception) {
            }
        });
    },
    SaveSubCategory: () => {
        var form = $("#formCreateSubCategory").serialize();
        $.ajax({
            url: "/Product/CreateSubCategory",
            type: 'post',
            data: form,
            dataType: 'json',
            success: function (data) {
            },
            error: function (jqXHR, exception) {
            }
        });
    },
    SaveBrand: () => {
        var form = $("#formCreateBrand").serialize();
        $.ajax({
            url: "/Product/CreateBrand",
            type: 'post',
            data: form,
            dataType: 'json',
            success: function (data) {
            },
            error: function (jqXHR, exception) {
            }
        });
    },
    SaveType: () => {
        var form = $("#formCreateType").serialize();
        $.ajax({
            url: "/Product/CreateProductType",
            type: 'post',
            data: form,
            dataType: 'json',
            success: function (data) {
            },
            error: function (jqXHR, exception) {
            }
        });
    },

    changeSubCategoryList: (val) => {
        $.ajax({
            url: "/Product/GetSubCategoryList",
            type: 'GET',
            data: {val},
            dataType: 'json',
            success: function (data) {
            },
            error: function (jqXHR, exception) {
            }
        });
    },

    changeBrandList: () => {
        $.ajax({
            url: "/Product/GetBrandList",
            type: 'GET',
            data: { val },
            dataType: 'json',
            success: function (data) {
            },
            error: function (jqXHR, exception) {
            }
        });
    },
};