﻿using Newtonsoft.Json;
using Outfix.Commons;
using Outfix.Models;
using Outfix.ViewModels;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Web.Mvc;

namespace OutfixAdminPanel.Controllers
{
    public class OrderController : Controller
    {
        private string _apiPath;

        public string ApiPath
        {
            get => _apiPath;
            set => _apiPath = value;
        }
        public OrderController()
        {
            ApiPath = "http://localhost:57791/api/order/";
        }
        // GET: Order
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult ShowAllOrders()
        {
            try
            {
                List<OrderModel> orders = new List<OrderModel>();
                var url = ApiPath;
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(url);
                    //HTTP GET
                    var responseTask = client.GetAsync("GetOrdersList");
                    responseTask.Wait();

                    var result = responseTask.Result;
                    if (result.IsSuccessStatusCode)
                    {
                        var readTask = result.Content.ReadAsStringAsync();
                        readTask.Wait();

                        var response = readTask.Result;
                        orders = JsonConvert.DeserializeObject<List<OrderModel>>(response);
                    }
                }
                return View(orders);
            }
            catch (Exception ex)
            {
                return Json(new { Message = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }
        public ActionResult ShowOrdersByState(string status)
        {
            try
            {
                List<OrderModel> orders = new List<OrderModel>();
                var url = ApiPath;
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(url);
                    //HTTP GET
                    var responseTask = client.GetAsync("GetOrderList?state=" + status);
                    responseTask.Wait();

                    var result = responseTask.Result;
                    if (result.IsSuccessStatusCode)
                    {
                        var readTask = result.Content.ReadAsStringAsync();
                        readTask.Wait();

                        var response = readTask.Result;
                        orders = JsonConvert.DeserializeObject<List<OrderModel>>(response);
                    }
                }
                return View("ShowAllOrders", orders);
            }
            catch (Exception ex)
            {
                return Json(new { Message = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }
        public ActionResult OrderDetail(int id)
        {
            try
            {
                WP_Common common = new WP_Common();
                
                CartViewModel cvm = new CartViewModel();
                cvm.Order = common.GetOrderDetails(id);
                cvm.Customer = common.getCustomerDetailsById(cvm.Order.CustomerId);
                return View(cvm);
            }
            catch (Exception ex)
            {
                return Json(new { Message = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }
        public ActionResult ChangeBulkStatus(List<string> ids, string status)
        {
            try
            {

                Message msg = new Message();
                msg.ids = ids;
                msg.status = status;

                List<OrderModel> orders = new List<OrderModel>();
                var stringContent = new StringContent(JsonConvert.SerializeObject(msg), System.Text.Encoding.UTF8, "application/json");
                var url = ApiPath;
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(url);
                    //HTTP GET
                    var responseTask = client.PutAsync("ChangeOrderStatusInBulk", stringContent);
                    responseTask.Wait();

                    var result = responseTask.Result;
                    if (result.IsSuccessStatusCode)
                    {
                        var readTask = result.Content.ReadAsStringAsync();
                        readTask.Wait();

                        var response = readTask.Result;

                    }
                }
                return Json("OK", JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { Message = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }
    }
    public class Message
    {
        public List<string> ids { get; set; }
        public string status { get; set; }
        public override string ToString()
        {
            return JsonConvert.SerializeObject(this);
        }
    }
}