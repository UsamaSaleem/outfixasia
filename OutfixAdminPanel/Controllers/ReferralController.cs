﻿using Newtonsoft.Json;
using Outfix.Models;
using Outfix.ViewModels;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Web.Mvc;

namespace OutfixAdminPanel.Controllers
{
    public class ReferralController : Controller
    {
        private string _apiPath;

        public string ApiPath
        {
            get => _apiPath;
            set => _apiPath = value;
        }
        public ReferralController()
        {
            ApiPath = "http://localhost/api/referral/";

        }
        // GET: Referral
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult ShowAllReferrals()
        {
            try
            {
                List<ReferralModel> referrals = new List<ReferralModel>();
                var url = ApiPath;
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(url);
                    //HTTP GET
                    var responseTask = client.GetAsync("GetReferralList");
                    responseTask.Wait();

                    var result = responseTask.Result;
                    if (result.IsSuccessStatusCode)
                    {
                        var readTask = result.Content.ReadAsStringAsync();
                        readTask.Wait();

                        var response = readTask.Result;
                        referrals = JsonConvert.DeserializeObject<List<ReferralModel>>(response);
                    }
                }
                return View(referrals);
            }
            catch (Exception ex)
            {
                return Json(new { Message = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult ReferralDetail(string id)
        {
            try
            {
                ReferralModel referral = new ReferralModel();
                List<ReferralDetailsModel> details = new List<ReferralDetailsModel>();
                var url = ApiPath;
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(url);
                    
                    //HTTP GET
                    var responseTask = client.GetAsync("GetReferralDetailsList?ReferralId=" + Convert.ToInt32(id));
                    responseTask.Wait();

                    var result = responseTask.Result;
                    if (result.IsSuccessStatusCode)
                    {
                        var readTask = result.Content.ReadAsStringAsync();
                        readTask.Wait();

                        var response = readTask.Result;
                        details = JsonConvert.DeserializeObject<List<ReferralDetailsModel>>(response);
                    }

                    responseTask = client.GetAsync("GetReferralById?Id=" + Convert.ToInt32(id));
                    responseTask.Wait();

                    result = responseTask.Result;
                    if (result.IsSuccessStatusCode)
                    {
                        var readTask = result.Content.ReadAsStringAsync();
                        readTask.Wait();

                        var response = readTask.Result;
                        referral = JsonConvert.DeserializeObject<ReferralModel>(response);
                    }
                    
                }

                ReferralViewModel vm = new ReferralViewModel();
                vm.Referral = referral;
                vm.ReferralDetails = details;

                return View(vm);
            }
            catch (Exception ex)
            {
                return Json(new { Message = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult CreateReferral()
        {
            return View();
        }
        [HttpPost]
        public ActionResult CreateReferralForUser(ReferralModel model)
        {
            try
            {
                List<ReferralModel> referrals = new List<ReferralModel>();
                var url = ApiPath;
                var stringContent = new StringContent(model.ToString(), System.Text.Encoding.UTF8, "application/json");
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(url);
                    //HTTP GET
                    var responseTask = client.PostAsync("CreateReferralForUser", stringContent);
                    responseTask.Wait();

                    var result = responseTask.Result;
                    if (result.IsSuccessStatusCode)
                    {
                        var readTask = result.Content.ReadAsStringAsync();
                        readTask.Wait();

                        var response = readTask.Result;
                        // referrals = JsonConvert.DeserializeObject<List<ReferralModel>>(response);
                    }
                }
                return Json("OK", JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { Message = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }
    }
}