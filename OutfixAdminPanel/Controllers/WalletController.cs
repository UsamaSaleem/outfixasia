﻿using Newtonsoft.Json;
using Outfix.Models;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Web.Mvc;

namespace OutfixAdminPanel.Controllers
{
    public class WalletController : Controller
    {
        private string _apiPath;

        public string ApiPath
        {
            get => _apiPath;
            set => _apiPath = value;
        }
        public WalletController()
        {
            ApiPath = "http://localhost/api/wallet/";

        }
        // GET: Wallet
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult ShowAllWallets()
        {
            try
            {
                List<WalletModel> reviews = new List<WalletModel>();
                var url = ApiPath;
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(url);
                    //HTTP GET
                    var responseTask = client.GetAsync("GetAllWallet");
                    responseTask.Wait();

                    var result = responseTask.Result;
                    if (result.IsSuccessStatusCode)
                    {
                        var readTask = result.Content.ReadAsStringAsync();
                        readTask.Wait();

                        var response = readTask.Result;
                        reviews = JsonConvert.DeserializeObject<List<WalletModel>>(response);
                    }
                }
                return View(reviews);
            }
            catch (Exception ex)
            {
                return Json(new { Message = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }
        public ActionResult WalletDetail(string id)
        {
            try
            {
                List<WalletDetailsModel> wd = new List<WalletDetailsModel>();
                var url = ApiPath;
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(url);
                    //HTTP GET
                    var responseTask = client.GetAsync("GetWalletByWalletId?WalletId=" + id);
                    responseTask.Wait();

                    var result = responseTask.Result;
                    if (result.IsSuccessStatusCode)
                    {
                        var readTask = result.Content.ReadAsStringAsync();
                        readTask.Wait();

                        var response = readTask.Result;
                        wd = JsonConvert.DeserializeObject<List<WalletDetailsModel>>(response);
                    }
                }
                return View(wd);
            }
            catch (Exception ex)
            {
                return Json(new { Message = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }
    }
}