﻿using Outfix.Commons;
using Outfix.ViewModels;
using System.Web.Mvc;

namespace OutfixAdminPanel.Controllers
{
    public class HomeController : Controller
    {
        public SecurityCommons security;

        public HomeController()
        {
            security = new SecurityCommons();
        }
        public ActionResult Index()
        {
            if (Session.Count > 0)
            {
                if(Session["User"] != null)
                {
                    return View();
                }
            }

            return View("Login");
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }

        public ActionResult Login(LoginViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }
            var cust = security.checkPortalLogin(model);
            if (cust == null)
            {
                return View();
            }
            else
            {
                Session["User"] = cust;
                return RedirectToAction("Index");
            }
        }
    }
}