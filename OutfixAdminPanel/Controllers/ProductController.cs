﻿using Newtonsoft.Json;
using Outfix.Models;
using Outfix.ViewModels;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Web;
using System.Web.Mvc;

namespace OutfixAdminPanel.Controllers
{
    public class ProductController : Controller
    {
        private string _apiPath;

        public string ApiPath
        {
            get => _apiPath;
            set => _apiPath = value;
        }

        public ProductController()
        {
            ApiPath = "http://localhost:57791/api/product/";
        }
        // GET: Product
        public ActionResult Index()
        {
            ExportProductsToExcel();
            return View();
        }
        public ActionResult ShowAllProducts()
        {
            try
            {
                List<ProductModel> products = new List<ProductModel>();
                var url = ApiPath;
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(url);
                    //HTTP GET
                    var responseTask = client.GetAsync("GetProductList");
                    responseTask.Wait();

                    var result = responseTask.Result;
                    if (result.IsSuccessStatusCode)
                    {
                        var readTask = result.Content.ReadAsStringAsync();
                        readTask.Wait();

                        var response = readTask.Result;
                        products = JsonConvert.DeserializeObject<List<ProductModel>>(response);
                    }
                }
                return View(products);
            }
            catch (Exception ex)
            {
                return Json(new { Message = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }
        public ActionResult AddNewProduct()
        {
            ViewData["MainCategoryList"] = new SelectList(getAllCategories(), "ID", "Description");
            ViewData["SubCategoryList"] = new SelectList(getAllSubCategories(), "ID", "Description");
            ViewData["BrandList"] = new SelectList(getAllBrands(), "ID", "Description");
            ViewData["TypeList"] = new SelectList(getAllProductTypes(), "ID", "Description");
            return View();
        }
        public ActionResult AddVariation()
        {
            var model = getAllProductVariations();
            return View(model);
        }

        [HttpPost]
        public ActionResult CreateNewProduct(ProductModel form)
        {

            if (System.Web.HttpContext.Current.Request.Files.AllKeys.Any())
            {
                var pic = System.Web.HttpContext.Current.Request.Files["ProductPicture1"];
                string path;

                if (pic != null)
                {
                    path = SaveImages(pic);
                    form.Picture1 = path;
                }

                pic = System.Web.HttpContext.Current.Request.Files["ProductPicture2"];
                if (pic != null)
                {
                    path = SaveImages(pic);
                    form.Picture2 = path;
                }

                pic = System.Web.HttpContext.Current.Request.Files["ProductPicture3"];
                if (pic != null)
                {
                    path = SaveImages(pic);
                    form.Picture3 = path;
                }
                pic = System.Web.HttpContext.Current.Request.Files["ProductPicture4"];
                if (pic != null)
                {
                    path = SaveImages(pic);
                    form.Picture4 = path;
                }
                pic = System.Web.HttpContext.Current.Request.Files["ProductPicture5"];
                if (pic != null)
                {
                    path = SaveImages(pic);
                    form.Picture5 = path;
                }
            }

            var url = ApiPath;
            form.Variation = JsonConvert.DeserializeObject<List<ProductVariationModel>>(form.VarStr);

            var stringContent = new StringContent(form.ToString(), System.Text.Encoding.UTF8, "application/json");

            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(url);
                var responseTask = client.PostAsync("CreateProduct", stringContent);
                responseTask.Wait();

                var result = responseTask.Result;
                if (result.IsSuccessStatusCode)
                {
                    var readTask = result.Content.ReadAsStringAsync();
                    readTask.Wait();

                    var response = readTask.Result;
                    // products = JsonConvert.DeserializeObject<List<ProductModel>>(response);
                }
            }
            return Json(new { Success = "OK" });
        }
        [HttpPost]
        public ActionResult CreateCategory(ProductSetupViewModel form)
        {
            var url = ApiPath;
            var stringContent = new StringContent(form.Main.ToString(), System.Text.Encoding.UTF8, "application/json");

            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(url);
                var responseTask = client.PostAsync("CreateMainCategory", stringContent);
                responseTask.Wait();

                var result = responseTask.Result;
                if (result.IsSuccessStatusCode)
                {
                    var readTask = result.Content.ReadAsStringAsync();
                    readTask.Wait();
                    var response = readTask.Result;
                }
            }
            return Json(new { Success = "OK" });
        }
        [HttpPost]
        public ActionResult CreateSubCategory(ProductSetupViewModel form)
        {
            var url = ApiPath;
            var stringContent = new StringContent(form.Sub.ToString(), System.Text.Encoding.UTF8, "application/json");

            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(url);
                var responseTask = client.PostAsync("CreateSubCategory", stringContent);
                responseTask.Wait();

                var result = responseTask.Result;
                if (result.IsSuccessStatusCode)
                {
                    var readTask = result.Content.ReadAsStringAsync();
                    readTask.Wait();
                    var response = readTask.Result;
                }
            }
            return Json(new { Success = "OK" });
        }

        [HttpPost]
        public ActionResult CreateBrand(ProductSetupViewModel form)
        {
            var url = ApiPath;
            var stringContent = new StringContent(form.Brand.ToString(), System.Text.Encoding.UTF8, "application/json");

            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(url);
                var responseTask = client.PostAsync("CreateBrand", stringContent);
                responseTask.Wait();

                var result = responseTask.Result;
                if (result.IsSuccessStatusCode)
                {
                    var readTask = result.Content.ReadAsStringAsync();
                    readTask.Wait();
                    var response = readTask.Result;
                }
            }
            return Json(new { Success = "OK" });
        }

        [HttpPost]
        public ActionResult CreateProductType(ProductSetupViewModel form)
        {
            var url = ApiPath;
            var stringContent = new StringContent(form.ProductType.ToString(), System.Text.Encoding.UTF8, "application/json");

            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(url);
                var responseTask = client.PostAsync("CreateProductType", stringContent);
                responseTask.Wait();

                var result = responseTask.Result;
                if (result.IsSuccessStatusCode)
                {
                    var readTask = result.Content.ReadAsStringAsync();
                    readTask.Wait();
                    var response = readTask.Result;
                }
            }
            return Json(new { Success = "OK" });
        }

        public IList<DropDownModel> getAllCategories()
        {
            try
            {
                List<MainCategoryModel> categories = new List<MainCategoryModel>();
                IList<DropDownModel> list = new List<DropDownModel>();

                var url = ApiPath;
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(url);
                    //HTTP GET
                    var responseTask = client.GetAsync("GetMainCategoryList");
                    responseTask.Wait();

                    var result = responseTask.Result;
                    if (result.IsSuccessStatusCode)
                    {
                        var readTask = result.Content.ReadAsStringAsync();
                        readTask.Wait();

                        var response = readTask.Result;
                        categories = JsonConvert.DeserializeObject<List<MainCategoryModel>>(response);

                        foreach (var data in categories)
                        {
                            DropDownModel item = new DropDownModel();
                            item.ID = data.Name;
                            item.Description = data.Name;
                            list.Add(item);
                        }
                    }
                }
                return list;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public IList<DropDownModel> getAllSubCategories()
        {
            try
            {
                List<SubCategoryModel> categories = new List<SubCategoryModel>();
                IList<DropDownModel> list = new List<DropDownModel>();

                var url = ApiPath;
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(url);
                    //HTTP GET
                    var responseTask = client.GetAsync("GetSubCategoryList");
                    responseTask.Wait();

                    var result = responseTask.Result;
                    if (result.IsSuccessStatusCode)
                    {
                        var readTask = result.Content.ReadAsStringAsync();
                        readTask.Wait();

                        var response = readTask.Result;
                        categories = JsonConvert.DeserializeObject<List<SubCategoryModel>>(response);

                        foreach (var data in categories)
                        {
                            DropDownModel item = new DropDownModel();
                            item.ID = data.Name;
                            item.Description = data.Name;
                            list.Add(item);
                        }
                    }
                }
                return list;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public IList<DropDownModel> getAllBrands()
        {
            try
            {
                List<BrandModel> categories = new List<BrandModel>();
                IList<DropDownModel> list = new List<DropDownModel>();

                var url = ApiPath;
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(url);
                    //HTTP GET
                    var responseTask = client.GetAsync("GetBrandList");
                    responseTask.Wait();

                    var result = responseTask.Result;
                    if (result.IsSuccessStatusCode)
                    {
                        var readTask = result.Content.ReadAsStringAsync();
                        readTask.Wait();

                        var response = readTask.Result;
                        categories = JsonConvert.DeserializeObject<List<BrandModel>>(response);

                        foreach (var data in categories)
                        {
                            DropDownModel item = new DropDownModel();
                            item.ID = data.Name;
                            item.Description = data.Name;
                            list.Add(item);

                        }
                    }
                }
                return list;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public IList<DropDownModel> getAllProductTypes()
        {
            try
            {
                List<ProductTypeModel> types = new List<ProductTypeModel>();
                IList<DropDownModel> list = new List<DropDownModel>();

                var url = ApiPath;
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(url);
                    //HTTP GET
                    var responseTask = client.GetAsync("GetProductTypeList");
                    responseTask.Wait();

                    var result = responseTask.Result;
                    if (result.IsSuccessStatusCode)
                    {
                        var readTask = result.Content.ReadAsStringAsync();
                        readTask.Wait();

                        var response = readTask.Result;
                        types = JsonConvert.DeserializeObject<List<ProductTypeModel>>(response);

                        foreach (var data in types)
                        {
                            DropDownModel item = new DropDownModel();
                            item.ID = data.Name;
                            item.Description = data.Name;
                            list.Add(item);

                        }
                    }
                }
                return list;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public List<ProductFilterModel> getAllProductVariations()
        {
            try
            {
                List<ProductFilterModel> vs = new List<ProductFilterModel>();
                IList<DropDownModel> list = new List<DropDownModel>();

                var url = ApiPath;
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(url);
                    //HTTP GET
                    var responseTask = client.GetAsync("getAllFilterNames");
                    responseTask.Wait();

                    var result = responseTask.Result;
                    if (result.IsSuccessStatusCode)
                    {
                        var readTask = result.Content.ReadAsStringAsync();
                        readTask.Wait();

                        var response = readTask.Result;
                        vs = JsonConvert.DeserializeObject<List<ProductFilterModel>>(response);
                    }
                }
                return vs;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public ActionResult ProductSetup()
        {
            ViewData["MainCategoryList"] = new SelectList(getAllCategories(), "ID", "Description");
            ViewData["SubCategoryList"] = new SelectList(getAllSubCategories(), "ID", "Description");
            ViewData["BrandList"] = new SelectList(getAllBrands(), "ID", "Description");
            ViewData["TypeList"] = new SelectList(getAllProductTypes(), "ID", "Description");

            ViewBag.Type = getAllTypes();
            return View();
        }
        public List<DropDownModel> getAllTypes()
        {
            List<DropDownModel> list = new List<DropDownModel>();

            list.Add(new DropDownModel
            {
                ID = "Men",
                Description = "Men"
            });
            list.Add(new DropDownModel
            {
                ID = "Women",
                Description = "Women"
            });
            list.Add(new DropDownModel
            {
                ID = "Kids",
                Description = "Kids"
            });
            return list;
        }
        public string SaveImages(HttpPostedFile product)
        {
            var FileName = Path.GetFileNameWithoutExtension(product.FileName);
            var FileExtension = Path.GetExtension(product.FileName);
            var UploadPath = ConfigurationManager.AppSettings["ImageFolderPath"].ToString();
            Guid guid = Guid.NewGuid();
            var FilePath = guid.ToString() + "-" + FileName.Trim() + FileExtension;
            FileName = Path.Combine(UploadPath, FilePath);
            //product.FileName = UploadPath + FileName;
            product.SaveAs(FileName);

            return FilePath;
        }

        public void ExportProductsToExcel()
        {
            var url = ApiPath + "ExportToExcel/";

            using (var client = new HttpClient())
            {
                var responseTask = client.GetAsync(url);
                responseTask.Wait();
            }
        }
        public IList<DropDownModel> GetSubCategoryList()
        {
            IList<DropDownModel> list = new List<DropDownModel>();

            var url = ApiPath + "GetSubCategoryList/";

            using (var client = new HttpClient())
            {
                var responseTask = client.GetAsync(url);
                responseTask.Wait();
                var result = responseTask.Result;
                if (result.IsSuccessStatusCode)
                {
                    var readTask = result.Content.ReadAsStringAsync();
                    readTask.Wait();

                    var response = readTask.Result;
                    var sclist = JsonConvert.DeserializeObject<List<SubCategoryModel>>(response);

                    foreach (var data in sclist)
                    {
                        DropDownModel item = new DropDownModel();
                        item.ID = data.Name;
                        item.Description = data.Name;
                        list.Add(item);

                    }
                }
            }
            return list;
        }
    }
}