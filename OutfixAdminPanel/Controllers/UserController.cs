﻿using Newtonsoft.Json;
using Outfix.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Web;
using System.Web.Mvc;

namespace OutfixAdminPanel.Controllers
{
    public class UserController : Controller
    {
        private string _apiPath;

        public string ApiPath
        {
            get => _apiPath;
            set => _apiPath = value;
        }
        public UserController()
        {
            ApiPath = "http://localhost/api/user/";
        }
        // GET: User
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult ShowAllUsers()
        {
            try
            {
                List<UserModel> users = new List<UserModel>();
                var url = ApiPath;
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(url);
                    //HTTP GET
                    var responseTask = client.GetAsync("GetUserList?type=user");
                    responseTask.Wait();

                    var result = responseTask.Result;
                    if (result.IsSuccessStatusCode)
                    {
                        var readTask = result.Content.ReadAsStringAsync();
                        readTask.Wait();

                        var response = readTask.Result;
                        users = JsonConvert.DeserializeObject<List<UserModel>>(response);
                    }
                }
                return View(users);
            }
            catch (Exception ex)
            {
                return Json(new { Message = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }

    }
}