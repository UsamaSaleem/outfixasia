﻿using Newtonsoft.Json;
using Outfix.Models;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Web.Mvc;

namespace OutfixAdminPanel.Controllers
{
    public class ReviewController : Controller
    {
        private string _apiPath;

        public string ApiPath
        {
            get { return _apiPath; }
            set { _apiPath = value; }
        }
        public ReviewController()
        {
            ApiPath = "http://localhost/api/review/";
        }
        // GET: Review
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult ShowAllReviews()
        {
            try
            {
                List<ReviewModel> reviews = new List<ReviewModel>();
                var url = ApiPath;
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(url);
                    //HTTP GET
                    var responseTask = client.GetAsync("GetReviewList");
                    responseTask.Wait();

                    var result = responseTask.Result;
                    if (result.IsSuccessStatusCode)
                    {
                        var readTask = result.Content.ReadAsStringAsync();
                        readTask.Wait();

                        var response = readTask.Result;
                        reviews = JsonConvert.DeserializeObject<List<ReviewModel>>(response);
                    }
                }
                return View(reviews);
            }
            catch (Exception ex)
            {
                return Json(new { Message = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }
        public ActionResult ShowReviewsByState(string status)
        {
            try
            {
                List<ReviewModel> reviews = new List<ReviewModel>();
                var url = ApiPath;
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(url);
                    //HTTP GET
                    var responseTask = client.GetAsync("GetReviewList?state=" + status);
                    responseTask.Wait();

                    var result = responseTask.Result;
                    if (result.IsSuccessStatusCode)
                    {
                        var readTask = result.Content.ReadAsStringAsync();
                        readTask.Wait();

                        var response = readTask.Result;
                        reviews = JsonConvert.DeserializeObject<List<ReviewModel>>(response);
                    }
                }
                return View("ShowAllReviews", reviews);
            }
            catch (Exception ex)
            {
                return Json(new { Message = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }
        public JsonResult ApproveReview(ReviewModel model)
        {
            List<ReviewModel> reviews = new List<ReviewModel>();
            var stringContent = new StringContent(model.ToString(), System.Text.Encoding.UTF8, "application/json");
            var url = ApiPath;
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(url);
                //HTTP GET
                var responseTask = client.PutAsync("UpdateReview", stringContent);
                responseTask.Wait();

                var result = responseTask.Result;
                if (result.IsSuccessStatusCode)
                {
                    var readTask = result.Content.ReadAsStringAsync();
                    readTask.Wait();

                    var response = readTask.Result;
                    
                }
            }
            return Json("OK", JsonRequestBehavior.AllowGet);
        }
    }
}