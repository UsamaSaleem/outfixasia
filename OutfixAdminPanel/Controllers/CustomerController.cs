﻿using Newtonsoft.Json;
using Outfix.Commons;
using Outfix.Models;
using Outfix.ViewModels;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Web.Mvc;

namespace OutfixAdminPanel.Controllers
{
    public class CustomerController : Controller
    {
        private string _apiPath;

        public string ApiPath
        {
            get => _apiPath;
            set => _apiPath = value;
        }
        public CustomerController()
        {
            ApiPath = "http://localhost/api/customer/";
        }
        // GET: Customer
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult ShowAllCustomers()
        {
            try
            {
                List<CustomerModel> customers = new List<CustomerModel>();
                var url = ApiPath;
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(url);
                    //HTTP GET
                    var responseTask = client.GetAsync("GetCustomerList");
                    responseTask.Wait();

                    var result = responseTask.Result;
                    if (result.IsSuccessStatusCode)
                    {
                        var readTask = result.Content.ReadAsStringAsync();
                        readTask.Wait();

                        var response = readTask.Result;
                        customers = JsonConvert.DeserializeObject<List<CustomerModel>>(response);
                    }
                }
                return View(customers);
            }
            catch (Exception ex)
            {
                return Json(new { Message = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult CustomerDetail(string id)
        {
            try
            {
                int cid = Convert.ToInt32(id);
                WP_Common commons = new WP_Common();
                List<OrderModel> orders = new List<OrderModel>();

                var customer = commons.getCustomerDetailsById(cid);
                orders = commons.getCustomerOrders(cid);
                var wallet = commons.getCustomerWallet(cid);

                CustomerDetailViewModel vm = new CustomerDetailViewModel();
                vm.Customer = customer;
                vm.Orders = orders;
                vm.Wallet = wallet;

                return View(vm);
            }
            catch (Exception ex)
            {
                return Json(new { Message = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }
    }
}