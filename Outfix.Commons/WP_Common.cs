﻿using Newtonsoft.Json;
using Outfix.Models;
using Outfix.ViewModels;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Net.Http;

namespace Outfix.Commons
{
    public class WP_Common
    {
        private string _apiPath;

        public string ApiPath
        {
            get => _apiPath;
            set => _apiPath = value;
        }

        public WP_Common()
        {
            var path = ConfigurationManager.AppSettings["PathAPI"].ToString();
            ApiPath = path;
        }
        public List<ProductModel> getAllProducts(SearchFilterModel model)
        {
            try
            {
                List<ProductModel> products = new List<ProductModel>();
                List<ProductModel> finalList = new List<ProductModel>();
                var url = ApiPath + "product/";
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(url);
                    //HTTP GET
                    var responseTask = client.GetAsync("GetProductList?filters=" + model.ToString());
                    responseTask.Wait();

                    var result = responseTask.Result;
                    if (result.IsSuccessStatusCode)
                    {
                        var readTask = result.Content.ReadAsStringAsync();
                        readTask.Wait();

                        var response = readTask.Result;
                        products = JsonConvert.DeserializeObject<List<ProductModel>>(response);


                    }
                }

                return products;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public ProductModel getProductbyId(string id)
        {
            try
            {
                ProductModel product = new ProductModel();
                var url = ApiPath + "product/";

                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(url);
                    //HTTP GET
                    var responseTask = client.GetAsync("GetProductById?id=" + id);
                    responseTask.Wait();

                    var result = responseTask.Result;
                    if (result.IsSuccessStatusCode)
                    {
                        var readTask = result.Content.ReadAsStringAsync();
                        readTask.Wait();

                        var response = readTask.Result;
                        product = JsonConvert.DeserializeObject<ProductModel>(response);
                    }
                }
                if (product != null)
                {

                }

                return product;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public ProductModel getProductWithVariationById(string id)
        {
            try
            {
                ProductModel product = new ProductModel();
                var url = ApiPath + "product/";

                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(url);
                    //HTTP GET
                    var responseTask = client.GetAsync("GetProductWithVariationById?id=" + id);
                    responseTask.Wait();

                    var result = responseTask.Result;
                    if (result.IsSuccessStatusCode)
                    {
                        var readTask = result.Content.ReadAsStringAsync();
                        readTask.Wait();

                        var response = readTask.Result;
                        product = JsonConvert.DeserializeObject<ProductModel>(response);
                    }
                }
                if (product != null)
                {

                }

                return product;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public ProductVariationModel getVariationProductbyId(CartModel model)
        {
            try
            {
                ProductVariationModel product = new ProductVariationModel();
                var url = ApiPath + "product/";

                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(url);
                    //HTTP POST
                    var stringContent = new StringContent(model.ToString(), System.Text.Encoding.UTF8, "application/json");

                    var responseTask = client.PostAsync("GetVariationProductbyId", stringContent);
                    responseTask.Wait();

                    var result = responseTask.Result;
                    if (result.IsSuccessStatusCode)
                    {
                        var readTask = result.Content.ReadAsStringAsync();
                        readTask.Wait();

                        var response = readTask.Result;
                        product = JsonConvert.DeserializeObject<ProductVariationModel>(response);
                    }
                }
                if (product != null)
                {

                }

                return product;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public List<string> getNamesbySearch(string id)
        {
            try
            {
                List<string> names = new List<string>();
                var url = ApiPath + "product/";

                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(url);
                    //HTTP GET
                    var responseTask = client.GetAsync("SearchProductName?query=" + id);
                    responseTask.Wait();

                    var result = responseTask.Result;
                    if (result.IsSuccessStatusCode)
                    {
                        var readTask = result.Content.ReadAsStringAsync();
                        readTask.Wait();

                        var response = readTask.Result;
                        names = JsonConvert.DeserializeObject<List<string>>(response);
                    }
                }

                return names;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public List<ProductModel> getProductbySearch(string id)
        {
            try
            {
                List<ProductModel> products = new List<ProductModel>();
                var url = ApiPath + "product/";
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(url);
                    //HTTP GET
                    var responseTask = client.GetAsync("SearchProduct?query=" + id);
                    responseTask.Wait();

                    var result = responseTask.Result;
                    if (result.IsSuccessStatusCode)
                    {
                        var readTask = result.Content.ReadAsStringAsync();
                        readTask.Wait();

                        var response = readTask.Result;
                        products = JsonConvert.DeserializeObject<List<ProductModel>>(response);
                    }
                }

                return products;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public List<ProductModel> getSimilarProducts(string id)
        {
            try
            {
                List<ProductModel> product = new List<ProductModel>();
                var url = ApiPath + "product/";
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(url);
                    //HTTP GET
                    var responseTask = client.GetAsync("GetSimilarProducts?itemcode=" + id);
                    responseTask.Wait();

                    var result = responseTask.Result;
                    if (result.IsSuccessStatusCode)
                    {
                        var readTask = result.Content.ReadAsStringAsync();
                        readTask.Wait();

                        var response = readTask.Result;
                        product = JsonConvert.DeserializeObject<List<ProductModel>>(response);
                    }
                }
                if (product != null)
                {

                }

                return product;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public List<MainCategoryModel> getAllCategories()
        {
            try
            {
                List<MainCategoryModel> categories = new List<MainCategoryModel>();

                var url = ApiPath + "product/";
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(url);
                    //HTTP GET
                    var responseTask = client.GetAsync("GetMainCategoryList");
                    responseTask.Wait();

                    var result = responseTask.Result;
                    if (result.IsSuccessStatusCode)
                    {
                        var readTask = result.Content.ReadAsStringAsync();
                        readTask.Wait();

                        var response = readTask.Result;
                        categories = JsonConvert.DeserializeObject<List<MainCategoryModel>>(response);
                    }
                }
                return categories;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public IList<SubCategoryModel> getAllSubCategories()
        {
            try
            {
                List<SubCategoryModel> categories = new List<SubCategoryModel>();

                var url = ApiPath + "product/";
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(url);
                    //HTTP GET
                    var responseTask = client.GetAsync("GetSubCategoryList");
                    responseTask.Wait();

                    var result = responseTask.Result;
                    if (result.IsSuccessStatusCode)
                    {
                        var readTask = result.Content.ReadAsStringAsync();
                        readTask.Wait();

                        var response = readTask.Result;
                        categories = JsonConvert.DeserializeObject<List<SubCategoryModel>>(response);

                    }
                }
                return categories;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public IList<BrandModel> getAllBrands()
        {
            try
            {
                List<BrandModel> categories = new List<BrandModel>();

                var url = ApiPath + "product/";
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(url);
                    //HTTP GET
                    var responseTask = client.GetAsync("GetBrandList");
                    responseTask.Wait();

                    var result = responseTask.Result;
                    if (result.IsSuccessStatusCode)
                    {
                        var readTask = result.Content.ReadAsStringAsync();
                        readTask.Wait();

                        var response = readTask.Result;
                        categories = JsonConvert.DeserializeObject<List<BrandModel>>(response);

                    }
                }
                return categories;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public List<DropDownModel> getAllTypes()
        {
            List<DropDownModel> list = new List<DropDownModel>();

            list.Add(new DropDownModel
            {
                ID = "Men",
                Description = "Men"
            });
            list.Add(new DropDownModel
            {
                ID = "Women",
                Description = "Women"
            });
            list.Add(new DropDownModel
            {
                ID = "Kids",
                Description = "Kids"
            });
            return list;
        }

        public List<ProductFilterModel> getAllFilters()
        {
            try
            {
                List<ProductFilterModel> filters = new List<ProductFilterModel>();

                var url = ApiPath + "product/";
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(url);
                    //HTTP GET
                    var responseTask = client.GetAsync("GetFilterList");
                    responseTask.Wait();

                    var result = responseTask.Result;
                    if (result.IsSuccessStatusCode)
                    {
                        var readTask = result.Content.ReadAsStringAsync();
                        readTask.Wait();

                        var response = readTask.Result;
                        filters = JsonConvert.DeserializeObject<List<ProductFilterModel>>(response);

                    }
                }
                return filters;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<ReviewModel> getProductReviews(string id)
        {
            try
            {
                List<ReviewModel> reviews = new List<ReviewModel>();
                var url = ApiPath + "review/";
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(url);
                    //HTTP GET
                    var responseTask = client.GetAsync("GetReviewByProductId?id=" + id);
                    responseTask.Wait();

                    var result = responseTask.Result;
                    if (result.IsSuccessStatusCode)
                    {
                        var readTask = result.Content.ReadAsStringAsync();
                        readTask.Wait();

                        var response = readTask.Result;
                        reviews = JsonConvert.DeserializeObject<List<ReviewModel>>(response);
                    }
                }
                if (reviews != null)
                {

                }

                return reviews;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public ReferralModel getCustomerbyReferralCode(string Code)
        {
            try
            {
                ReferralModel referral = new ReferralModel();
                var url = ApiPath + "referral/";
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(url);
                    //HTTP GET
                    var responseTask = client.GetAsync("GetUserIdByReferralCode?Code=" + Code);
                    responseTask.Wait();

                    var result = responseTask.Result;
                    if (result.IsSuccessStatusCode)
                    {
                        var readTask = result.Content.ReadAsStringAsync();
                        readTask.Wait();

                        var response = readTask.Result;
                        referral = JsonConvert.DeserializeObject<ReferralModel>(response);
                    }
                }
                if (referral != null)
                {

                }

                return referral;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public WalletModel getWalletbyUserId(string UserId)
        {
            try
            {
                WalletModel wallet = new WalletModel();
                var url = ApiPath + "wallet/";
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(url);
                    //HTTP GET
                    var responseTask = client.GetAsync("GetWalletByUser?UserId=" + UserId);
                    responseTask.Wait();

                    var result = responseTask.Result;
                    if (result.IsSuccessStatusCode)
                    {
                        var readTask = result.Content.ReadAsStringAsync();
                        readTask.Wait();

                        var response = readTask.Result;
                        wallet = JsonConvert.DeserializeObject<WalletModel>(response);
                    }
                }
                if (wallet != null)
                {

                }

                return wallet;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public WalletModel CreateWallet(WalletModel wallet)
        {
            try
            {
                var url = ApiPath + "wallet/";
                WalletModel w = new WalletModel();
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(url);
                    //HTTP POST

                    var stringContent = new StringContent(wallet.ToString(), System.Text.Encoding.UTF8, "application/json");

                    var responseTask = client.PostAsync("CreateWalletForUser", stringContent);
                    responseTask.Wait();

                    var result = responseTask.Result;
                    if (result.IsSuccessStatusCode)
                    {
                        var readTask = result.Content.ReadAsStringAsync();
                        readTask.Wait();

                        var response = readTask.Result;
                        w = JsonConvert.DeserializeObject<WalletModel>(response);
                    }
                }

                return w;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public string CreateWalletDetail(WalletDetailsModel wallet)
        {
            try
            {
                var url = ApiPath + "wallet/";
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(url);
                    //HTTP POST

                    var stringContent = new StringContent(wallet.ToString(), System.Text.Encoding.UTF8, "application/json");

                    var responseTask = client.PostAsync("CreateWalletDetailForUser", stringContent);
                    responseTask.Wait();

                    var result = responseTask.Result;
                    if (result.IsSuccessStatusCode)
                    {
                        var readTask = result.Content.ReadAsStringAsync();
                        readTask.Wait();

                        var response = readTask.Result;
                        // products = JsonConvert.DeserializeObject<List<ProductModel>>(response);
                    }
                }

                return "OK";
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }

        public StyleProfileUserAnswersModel CreateSurveyForUser(StyleProfileSurveyModel survey)
        {
            try
            {
                var url = ApiPath + "profile/";
                StyleProfileUserAnswersModel s = new StyleProfileUserAnswersModel();

                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(url);
                    //HTTP POST
                    //public WalletModel getWalletByUserId()
                    //{
                    var stringContent = new StringContent(survey.ToString(), System.Text.Encoding.UTF8, "application/json");

                    var responseTask = client.PostAsync("CreateSurveyForUser", stringContent);
                    responseTask.Wait();

                    var result = responseTask.Result;
                    if (result.IsSuccessStatusCode)
                    {
                        var readTask = result.Content.ReadAsStringAsync();
                        readTask.Wait();

                        var response = readTask.Result;
                        //s = JsonConvert.DeserializeObject<StyleProfileUserAnswersModel>(response);
                    }
                }

                return s;

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public StyleProfileSurveyModel GetSurveyForUser(StyleProfileSurveyModel model)
        {
            try
            {
                var url = ApiPath + "profile/";
                var survey = new StyleProfileSurveyModel();


                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(url);
                    //HTTP GET
                    var responseTask = client.GetAsync("GetAllQuestionAnswersByUser?CustId=" + model.Customer.ID);
                    responseTask.Wait();

                    var result = responseTask.Result;
                    if (result.IsSuccessStatusCode)
                    {
                        var readTask = result.Content.ReadAsStringAsync();
                        readTask.Wait();
                        var response = readTask.Result;
                        survey = JsonConvert.DeserializeObject<StyleProfileSurveyModel>(response);
                    }

                    return survey;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public ReferralModel CreateReferral(ReferralModel referral)
        {
            try
            {
                var url = ApiPath + "referral/";
                ReferralModel r = new ReferralModel();
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(url);
                    //HTTP POST
                    //public WalletModel getWalletByUserId()
                    //{
                    var stringContent = new StringContent(referral.ToString(), System.Text.Encoding.UTF8, "application/json");

                    var responseTask = client.PostAsync("CreateReferralForUser", stringContent);
                    responseTask.Wait();

                    var result = responseTask.Result;
                    if (result.IsSuccessStatusCode)
                    {
                        var readTask = result.Content.ReadAsStringAsync();
                        readTask.Wait();

                        var response = readTask.Result;
                        r = JsonConvert.DeserializeObject<ReferralModel>(response);
                    }
                }

                return r;

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public CustomerModel getCustomerByEmail(string email)
        {
            try
            {
                CustomerModel customer = new CustomerModel();
                var url = ApiPath + "customer/";
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(url);
                    //HTTP GET
                    var responseTask = client.GetAsync("GetCustomerByEmail?email=" + email);
                    responseTask.Wait();
                    var result = responseTask.Result;
                    if (result.IsSuccessStatusCode)
                    {
                        var readTask = result.Content.ReadAsStringAsync();
                        readTask.Wait();

                        var response = readTask.Result;
                        customer = JsonConvert.DeserializeObject<CustomerModel>(response);
                    }
                }
                if (customer != null)
                {

                }

                return customer;

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public List<OrderModel> getCustomerOrders(int id)
        {
            try
            {
                List<OrderModel> orders = new List<OrderModel>();
                var url = ApiPath + "customer/";
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(url);
                    //HTTP GET
                    var responseTask = client.GetAsync("GetCustomerOrders?Id=" + id);

                    responseTask.Wait();

                    var result = responseTask.Result;
                    if (result.IsSuccessStatusCode)
                    {
                        var readTask = result.Content.ReadAsStringAsync();
                        readTask.Wait();

                        var response = readTask.Result;
                        orders = JsonConvert.DeserializeObject<List<OrderModel>>(response);
                    }
                }
                if (orders != null)
                {

                }

                return orders;

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public OrderModel GetOrderDetails(int id)
        {
            try
            {
                OrderModel order = new OrderModel();
                var url = ApiPath + "order/";
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(url);
                    //HTTP GET
                    var responseTask = client.GetAsync("GetOrderById/" + id);
                    responseTask.Wait();

                    var result = responseTask.Result;
                    if (result.IsSuccessStatusCode)
                    {
                        var readTask = result.Content.ReadAsStringAsync();
                        readTask.Wait();

                        var response = readTask.Result;
                        order = JsonConvert.DeserializeObject<OrderModel>(response);
                    }
                }
                if (order != null)
                {

                }

                return order;

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public string CreateReferralDetail(WalletDetailsModel wallet)
        {
            try
            {
                var url = ApiPath + "referral/";
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(url);
                    //HTTP POST

                    var stringContent = new StringContent(wallet.ToString(), System.Text.Encoding.UTF8, "application/json");

                    var responseTask = client.PostAsync("CreateReferralDetailForUser", stringContent);
                    responseTask.Wait();

                    var result = responseTask.Result;
                    if (result.IsSuccessStatusCode)
                    {
                        var readTask = result.Content.ReadAsStringAsync();
                        readTask.Wait();

                        var response = readTask.Result;
                        // products = JsonConvert.DeserializeObject<List<ProductModel>>(response);
                    }
                }

                return "OK";
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }

        public WalletModel UpdateWallet(WalletModel wallet)
        {
            try
            {
                var url = ApiPath + "wallet/";
                WalletModel w = new WalletModel();
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(url);
                    //HTTP POST

                    var stringContent = new StringContent(wallet.ToString(), System.Text.Encoding.UTF8, "application/json");

                    var responseTask = client.PostAsync("UpdateWalletByUserId", stringContent);
                    responseTask.Wait();

                    var result = responseTask.Result;
                    if (result.IsSuccessStatusCode)
                    {
                        var readTask = result.Content.ReadAsStringAsync();
                        readTask.Wait();

                        var response = readTask.Result;
                        w = JsonConvert.DeserializeObject<WalletModel>(response);
                    }
                }

                return w;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public WalletModel UpdateWalletAfterReferral(ReferralUsedModel referral)
        {
            try
            {
                var url = ApiPath + "wallet/";
                WalletModel w = new WalletModel();
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(url);
                    //HTTP POST
                    var stringContent = new StringContent(referral.ToString(), System.Text.Encoding.UTF8, "application/json");

                    var responseTask = client.PostAsync("UpdateWalletAfterReferralUsed", stringContent);
                    responseTask.Wait();

                    var result = responseTask.Result;
                    if (result.IsSuccessStatusCode)
                    {
                        var readTask = result.Content.ReadAsStringAsync();
                        readTask.Wait();

                        var response = readTask.Result;
                        w = JsonConvert.DeserializeObject<WalletModel>(response);
                    }
                }

                return w;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public bool checkReferral(string referral)
        {
            try
            {
                bool response = false;
                var url = ApiPath + "referral/";
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(url);
                    //HTTP GET
                    var responseTask = client.GetAsync("ValidateReferral?rfrl=" + referral);
                    responseTask.Wait();

                    var result = responseTask.Result;
                    if (result.IsSuccessStatusCode)
                    {
                        var readTask = result.Content.ReadAsStringAsync();
                        readTask.Wait();

                        response = Convert.ToBoolean(readTask.Result);
                        //code = JsonConvert.DeserializeObject<string>(response);
                    }
                }

                return response;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public string getCustomerReferCode(int id)
        {
            try
            {
                string code = "";
                var url = ApiPath + "customer/";
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(url);
                    //HTTP GET
                    var responseTask = client.GetAsync("GetReferCode?id=" + id);
                    responseTask.Wait();

                    var result = responseTask.Result;
                    if (result.IsSuccessStatusCode)
                    {
                        var readTask = result.Content.ReadAsStringAsync();
                        readTask.Wait();

                        var response = readTask.Result;
                        code = JsonConvert.DeserializeObject<string>(response);
                    }
                }
                return code;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public ReferralModel ValidateCoupon(string coupon)
        {
            try
            {
                ReferralModel model = new ReferralModel();
                var url = ApiPath + "referral/";
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(url);
                    //HTTP GET
                    var responseTask = client.GetAsync("ValidateCoupon?cpn=" + coupon);
                    responseTask.Wait();

                    var result = responseTask.Result;
                    if (result.IsSuccessStatusCode)
                    {
                        var readTask = result.Content.ReadAsStringAsync();
                        readTask.Wait();
                        var response = readTask.Result;
                        model = JsonConvert.DeserializeObject<ReferralModel>(response);
                    }
                }
                return model;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public List<StyleProfileQuestionsModel> getAllQuestionAnswers()
        {
            try
            {
                List<StyleProfileQuestionsModel> profile = new List<StyleProfileQuestionsModel>();
                //List<ProductModel> finalList = new List<ProductModel>();
                var url = ApiPath + "profile/";
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(url);
                    //HTTP GET
                    var responseTask = client.GetAsync("getAllQuestionAnswers");
                    responseTask.Wait();


                    var result = responseTask.Result;
                    if (result.IsSuccessStatusCode)
                    {
                        var readTask = result.Content.ReadAsStringAsync();
                        readTask.Wait();
                        var response = readTask.Result;
                        profile = JsonConvert.DeserializeObject<List<StyleProfileQuestionsModel>>(response);
                    }

                    return profile;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public void PlaceOrder(CartViewModel model)
        {
            try
            {
                CustomerModel customer = new CustomerModel();
                if (model.SaveLocation)
                {
                    //customer.AddNewLocation(model.Customer);

                    var cm = JsonConvert.SerializeObject(model.Customer);

                    var apiUrl = ApiPath + "customer/";
                    using (var client = new HttpClient())
                    {
                        client.BaseAddress = new Uri(apiUrl);
                        //HTTP GET
                        var stringContent = new StringContent(cm, System.Text.Encoding.UTF8, "application/json");
                        var responseTask = client.PostAsync("AddNewLocation", stringContent);
                        responseTask.Wait();

                        var result = responseTask.Result;
                        if (result.IsSuccessStatusCode)
                        {
                            var readTask = result.Content.ReadAsStringAsync();
                            readTask.Wait();

                        }

                    }
                }

                var m = JsonConvert.SerializeObject(model);

                var url = ApiPath + "order/";
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(url);
                    //HTTP GET
                    var stringContent = new StringContent(m, System.Text.Encoding.UTF8, "application/json");
                    var responseTask = client.PostAsync("PlaceCustomerOrder", stringContent);
                    responseTask.Wait();

                    var result = responseTask.Result;
                    if (result.IsSuccessStatusCode)
                    {
                        var readTask = result.Content.ReadAsStringAsync();
                        readTask.Wait();
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public List<ProductVariationModel> getVariationDropDowns(string ids)
        {
            try
            {
                List<ProductVariationModel> variation = new List<ProductVariationModel>();
                //var m = JsonConvert.SerializeObject(model);

                var url = ApiPath + "product/";
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(url);
                    //HTTP GET
                    var responseTask = client.GetAsync("GetVariationDropDowns?ids=" + ids);
                    responseTask.Wait();


                    var result = responseTask.Result;
                    if (result.IsSuccessStatusCode)
                    {
                        var readTask = result.Content.ReadAsStringAsync();
                        readTask.Wait();
                        var response = readTask.Result;
                        variation = JsonConvert.DeserializeObject<List<ProductVariationModel>>(response);
                    }
                }
                return variation;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public List<AnchorsModel> setHierarchy(SearchFilterModel s)
        {

            List<AnchorsModel> anc = new List<AnchorsModel>();

            if (!string.IsNullOrEmpty(s.Tp))
            {
                anc.Add(new AnchorsModel
                {
                    Name = s.Tp,
                    Value = "/Product/ProductsPage?Tp=" + s.Tp,
                });
            }
            if (!string.IsNullOrEmpty(s.Ct))
            {
                anc.Add(new AnchorsModel
                {
                    Name = s.Ct,
                    Value = "/Product/ProductsPage?Ct=" + s.Ct,
                });
            }
            if (!string.IsNullOrEmpty(s.SCt))
            {
                anc.Add(new AnchorsModel
                {
                    Name = s.SCt,
                    Value = "/Product/ProductsPage?SCt=" + s.SCt,
                });
            }
            if (!string.IsNullOrEmpty(s.Bd))
            {
                anc.Add(new AnchorsModel
                {
                    Name = s.Bd,
                    Value = "/Product/ProductsPage?Bd=" + s.Bd,
                });
            }
            return anc;
        }
        public CustomerModel getCustomerDetailsById(int id)
        {
            var url = ApiPath + "customer/";

            CustomerModel customer = new CustomerModel();
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(url);
                var responseTask = client.GetAsync("GetCustomerById?id=" + id);
                responseTask.Wait();

                var result = responseTask.Result;
                if (result.IsSuccessStatusCode)
                {
                    var readTask = result.Content.ReadAsStringAsync();
                    readTask.Wait();

                    var response = readTask.Result;
                    customer = JsonConvert.DeserializeObject<CustomerModel>(response);
                }
            }
            return customer;
        }
        public WalletModel getCustomerWallet(int id)
        {
            var url = ApiPath + "customer/";

            WalletModel wallet = new WalletModel();
            using (var client = new HttpClient())
            {
                var responseTask = client.GetAsync("GetCustomerWallet?Id=" + Convert.ToInt32(id));
                responseTask.Wait();

                var result = responseTask.Result;
                if (result.IsSuccessStatusCode)
                {
                    var readTask = result.Content.ReadAsStringAsync();
                    readTask.Wait();

                    var response = readTask.Result;
                    wallet = JsonConvert.DeserializeObject<WalletModel>(response);
                }
            }
            return wallet;
        }

        

    }
}

