﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Outfix.Commons
{
    public class OutfixEnums
    {
        public enum ReviewStatus
        {
            All = 0,
            Pending = 1,
            Approved = 2,
            Spam = 3
        }
        public enum City
        {
            Karachi = 1,
            Lahore = 2,
            Islamabad = 3,
            Multan = 4,
            Hyderabad = 5
        }
    }
}
