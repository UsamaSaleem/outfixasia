﻿using Newtonsoft.Json;
using Outfix.Models;
using Outfix.ViewModels;
using System;
using System.Configuration;
using System.Net.Http;
using System.Security.Cryptography;
using System.Text;

namespace Outfix.Commons
{
    public class SecurityCommons
    {
        private string _apiPath;

        public string ApiPath
        {
            get => _apiPath;
            set => _apiPath = value;
        }

        public SecurityCommons()
        {
            var path = ConfigurationManager.AppSettings["PathAPI"].ToString();
            ApiPath = path;
        }
        public byte[] GenerateSalt()
        {
            const int saltLength = 32;

            using (var randomNumberGenerator = new RNGCryptoServiceProvider())
            {
                var randomNumber = new byte[saltLength];
                randomNumberGenerator.GetBytes(randomNumber);

                return randomNumber;
            }
        }
        private byte[] Combine(byte[] first, byte[] second)
        {
            var ret = new byte[first.Length + second.Length];

            Buffer.BlockCopy(first, 0, ret, 0, first.Length);
            Buffer.BlockCopy(second, 0, ret, first.Length, second.Length);

            return ret;
        }
        public byte[] HashPasswordWithSalt(byte[] toBeHashed, byte[] salt)
        {
            using (var sha256 = SHA256.Create())
            {
                var combinedHash = Combine(toBeHashed, salt);
                return sha256.ComputeHash(combinedHash);
            }
        }

        public CustomerModel CreateCustomer(RegisterViewModel model)
        {
            try
            {
                var url = ApiPath + "customer/";
                CustomerModel customer = new CustomerModel();
                var cust = new CustomerModel();

                if (!model.ExternalUser)
                {

                    byte[] salt = GenerateSalt();
                    var hashedPassword = HashPasswordWithSalt(Encoding.UTF8.GetBytes(model.Password), salt);

                    cust = new CustomerModel
                    {
                        Username = model.Username,
                        Email = model.Email,
                        FirstName = model.FirstName,
                        LastName = model.LastName,
                        Password = Convert.ToBase64String(hashedPassword),
                        PasswordKey = Convert.ToBase64String(salt),
                    };
                }
                else
                {
                    cust = new CustomerModel
                    {
                        Username = model.Username,
                        Email = model.Email,
                    };
                }

                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(url);
                    //HTTP POST

                    var stringContent = new StringContent(cust.ToString(), System.Text.Encoding.UTF8, "application/json");

                    var responseTask = client.PostAsync("CreateCustomer", stringContent);
                    responseTask.Wait();

                    var result = responseTask.Result;
                    if (result.IsSuccessStatusCode)
                    {
                        var readTask = result.Content.ReadAsStringAsync();
                        readTask.Wait();

                        var response = readTask.Result;
                        customer = JsonConvert.DeserializeObject<CustomerModel>(response);
                    }
                }

                return customer;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public CustomerModel checkCustomerLogin(LoginViewModel model)
        {
            try
            {
                CustomerModel customer = new CustomerModel();

                var m = JsonConvert.SerializeObject(model);

                var url = ApiPath + "customer/";
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(url);
                    //HTTP GET
                    var stringContent = new StringContent(m, System.Text.Encoding.UTF8, "application/json");
                    var responseTask = client.PostAsync("CheckLogin", stringContent);
                    responseTask.Wait();

                    var result = responseTask.Result;
                    if (result.IsSuccessStatusCode)
                    {
                        var readTask = result.Content.ReadAsStringAsync();
                        readTask.Wait();

                        var response = readTask.Result;
                        if (response == null)
                        {
                            customer = null;
                        }
                        else
                        {
                            customer = JsonConvert.DeserializeObject<CustomerModel>(response);
                        }
                    }
                }
                return customer;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public UserModel CreatePortalUser(RegisterViewModel model)
        {
            try
            {
                var url = ApiPath + "user/";
                UserModel user = new UserModel();
                var usr = new UserModel();

                byte[] salt = GenerateSalt();
                var hashedPassword = HashPasswordWithSalt(Encoding.UTF8.GetBytes(model.Password), salt);

                usr = new UserModel
                {
                    Username = model.Username,
                    Email = model.Email,
                    Name = model.FirstName,
                    Status = model.LastName,
                    Password = Convert.ToBase64String(hashedPassword),
                    PasswordKey = Convert.ToBase64String(salt),
                };

                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(url);
                    //HTTP POST

                    var stringContent = new StringContent(usr.ToString(), System.Text.Encoding.UTF8, "application/json");

                    var responseTask = client.PostAsync("CreateCustomer", stringContent);
                    responseTask.Wait();

                    var result = responseTask.Result;
                    if (result.IsSuccessStatusCode)
                    {
                        var readTask = result.Content.ReadAsStringAsync();
                        readTask.Wait();

                        var response = readTask.Result;
                        user = JsonConvert.DeserializeObject<UserModel>(response);
                    }
                }

                return user;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public UserModel checkPortalLogin(LoginViewModel model)
        {
            try
            {
                UserModel user = new UserModel();

                var m = JsonConvert.SerializeObject(model);

                var url = ApiPath + "user/";
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(url);
                    //HTTP GET
                    var stringContent = new StringContent(m, System.Text.Encoding.UTF8, "application/json");
                    var responseTask = client.PostAsync("CheckLogin", stringContent);
                    responseTask.Wait();

                    var result = responseTask.Result;
                    if (result.IsSuccessStatusCode)
                    {
                        var readTask = result.Content.ReadAsStringAsync();
                        readTask.Wait();

                        var response = readTask.Result;
                        if (response == null)
                        {
                            user = null;
                        }
                        else
                        {
                            user = JsonConvert.DeserializeObject<UserModel>(response);
                        }
                    }
                }
                return user;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
