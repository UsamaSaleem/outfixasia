//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace OutfixDB
{
    using System;
    using System.Collections.Generic;
    
    public partial class Log
    {
        public int SerialNo { get; set; }
        public string ModuleName { get; set; }
        public string Type { get; set; }
        public string Description { get; set; }
        public string UserId { get; set; }
        public Nullable<System.DateTime> TimeStamp { get; set; }
        public string Flag { get; set; }
        public string ShopId { get; set; }
    }
}
